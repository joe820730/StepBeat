# WeDo emulator with Allegro5

### Introduce:
A cross-platform rhythm game.  
Developed as a simulator of IGS' arcade/PC online game.


### Installation

For Windows user, you can download from: [MEGA](https://mega.nz/#F!bc9W0QzQ!UZGA-Wz5dYn5mDXiGKu9lg).

For Unix-like system, you need compile from source.

Dependency:  
[Allegro 5](https://liballeg.org/)

Current version: **v0.12.0**

### Preview
![](preview.png)

Current game background image source: [Link](https://pixabay.com/zh/image-1462755/)