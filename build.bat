@ECHO OFF
set TARGET="bin\Release\StepBeat.exe"
set RAR="%PROGRAMFILES%\WinRAR\WinRAR.exe"
set MINGWPATH="D:\GCC\Compiler\MinGW\bin"
set ALLEGROPATH="D:\GCC\Libraries\allegro"

IF EXIST "%RAR%" (
  goto CopyFile
  ) else (
  goto end
)

:CopyFile
REM Check install directory is exist
IF EXIST install (
rmdir install /s /q )
mkdir install
REM Check file is exist
IF EXIST %TARGET% (
xcopy %TARGET% install )
xcopy res install\res /E /I
xcopy Songs\Songs\alice install\Songs\alice /E /I
xcopy Songs\Songs\hallelujah install\Songs\hallelujah /E /I
xcopy Songs\Songs\gatorix install\Songs\gatorix /E /I
goto Pack

:Pack
FOR /F "usebackq" %%i IN (`where allegro*.dll /r %ALLEGROPATH% ^| findstr /v "debug" ^| findstr /v "monolith"`) Do (
REM echo %%i
xcopy %%i install
)
FOR /F "usebackq" %%i IN (`where lib*.dll /r %MINGWPATH% ^| findstr /v "fortran"`) DO (
xcopy %%i install
)
cd install
%RAR% a -m3 -r -s -sfx StepBeatInst
%RAR% c StepBeatInst.exe -z"..\installer_comment.txt"
%RAR% s -iiconres/icon.ico StepBeatInst.exe
goto End

:End