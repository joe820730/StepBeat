#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <functional>
#include <string>
#include <chrono>
#include <ctime>

#include <Allegro_include.hpp>
#include <globalDef.hpp>
#include <MainMenu.hpp>
#include <ConfigMenu.hpp>
#include <KeymapConfig.hpp>
#include <MusicList.hpp>
#include <ResultScreen.hpp>
#include <GameEvents.hpp>
#include "BackgroundWorker.h"
#include "StateHandler.hpp"
#include "debug.hpp"

#include <StepBeat.h>

#ifdef ALLEGRO_WINDOWS
#define PROGRAMICON 1
#include <windows.h>
#include <allegro5/allegro_windows.h>
#endif // ALLEGRO_WINDOWS

#define GAMECFG_PATH "config.ini"
#define TITLE_STR_LEN 32

using namespace std;

bool g_isRun = true;
bool g_isRestart = false;
bool g_needRestartDisp = true;
bool g_isPaused = false;
double gScaleRate = 1.0;

ALLEGRO_DISPLAY* g_display = nullptr;/* pointer to display */

GameResources* wRes = nullptr; // Resources structure.

/* Global objects */
MainMenu* gMAINMENU = nullptr;
ConfigMenu* gCONFIGMENU = nullptr;
KeymapConfig* gKEYMAPCFGMENU = nullptr;
MusicList* gMUSICLIST = nullptr;
ResultScreen* gRESULTSCREEN = nullptr;
ThemeManager* gTHEMEMGR = nullptr;
GamePlayCtrl* gGAMEPLAYCTRL = nullptr;
GameEvents* gGAMEEVENTS = nullptr;
StateHandler* gSTATEHANDLER = nullptr;
BackgroundWorker* gBGNDWORKER = nullptr;
extern void AllegroDestroy(void);

extern void GameLoop(ConfigProvider* cfg_provider);
void DispTester(ConfigProvider* cfg_provider);

int main()
{
    srand((unsigned int)time(0));
    char windowTitle[TITLE_STR_LEN] = {0};
    snprintf(windowTitle, TITLE_STR_LEN, "%s v%s", GAME_TITLE, GAME_VER_STR);

    /* Program initialize */
    if(!AllegroInit())
    {
        printf("System init fail!! Program will exit.\n");
        return -1;
    }
    /* Load game configure file */
    ConfigProvider *cfg_provider = new ConfigProvider;
    cfg_provider->LoadFile(GAMECFG_PATH);

    /* Resources structure. */
    wRes = new GameResources;

    #ifdef ALLEGRO_WINDOWS
    HWND winHandle;
    HICON titleICON = LoadIcon(GetModuleHandle(nullptr), "PROGRAMICON");
    #endif // ALLEGRO_WINDOWS

    /* Resource initialize. */

    al_set_new_display_option(ALLEGRO_SAMPLE_BUFFERS, 1, ALLEGRO_SUGGEST);
    //al_set_new_display_option(ALLEGRO_SAMPLES, 2, ALLEGRO_SUGGEST);
    al_set_new_bitmap_flags(ALLEGRO_CONVERT_BITMAP|ALLEGRO_MIN_LINEAR|ALLEGRO_MAG_LINEAR);
    /* Initial screen. */
    int dispFlag = ALLEGRO_WINDOWED;
    g_isRestart = false;
    g_isRun = true;

    gGAMEEVENTS = new GameEvents;
    gGAMEPLAYCTRL = new GamePlayCtrl;
    gTHEMEMGR = new ThemeManager;
    gKEYMAPCFGMENU = new KeymapConfig;
    gCONFIGMENU = new ConfigMenu;
    gMUSICLIST = new MusicList;
    gMAINMENU = new MainMenu;
    gRESULTSCREEN = new ResultScreen;
    gSTATEHANDLER = new StateHandler;
    gBGNDWORKER = new BackgroundWorker;
    gTHEMEMGR->DefaultTheme();

    if(!g_isRun)
        goto FAIL_EXIT;
    do
    {
        g_isRestart = false;
        g_isRun = true;
        W_GAMECONFIG curcfg = cfg_provider->GetConfig();
        gScaleRate = (double)curcfg.screenHeight / (double)wAvailableOption.resolution[0].screenHeight;

        if(!wRes->LoadFont(curcfg.screenHeight))
        {
            printf("Error occurred when loading font!\n");
            al_show_native_message_box(nullptr, "ERROR!", "Error occurred when loading font!", "Resources missing or unknown error, please reinstall!", "Exit", 0);
            g_isRun = false;
        }
        if(!gBGNDWORKER->Init()) g_isRun = false;

        if(g_needRestartDisp)
        {
            g_needRestartDisp = false;
            if(g_display != nullptr)
            {
                al_destroy_display(g_display); /* destroy the display */
                wRes->DestroyRes();
                g_display = nullptr;
            }
            al_set_new_display_flags(dispFlag);
            al_set_new_display_option(ALLEGRO_VSYNC, curcfg.vsyncOption, ALLEGRO_SUGGEST);
            LOG("Vsync opt = %s", (curcfg.vsyncOption == 0?"default":curcfg.vsyncOption == 1?"ON":"OFF"));
            al_set_new_window_title(windowTitle);
            g_display = al_create_display(curcfg.screenWidth, curcfg.screenHeight);
            if(g_display == nullptr)
            {
                printf("Can't create display! Line: %d\n", __LINE__);
                g_isRun = false;
            }
            #ifdef ALLEGRO_WINDOWS
            if(titleICON)
            {
                winHandle = al_get_win_window_handle(g_display);
                SetClassLongPtr(winHandle, GCLP_HICON, (LONG_PTR)titleICON);
                SetClassLongPtr(winHandle, GCLP_HICONSM, (LONG_PTR)titleICON);
            }
            #endif // ALLEGRO_WINDOWS
            if(!wRes->LoadRes())
            {
                printf("Resource missing, please check all files are exist.\n");
                al_show_native_message_box(nullptr, "ERROR!", "Resources missing!", "Please reinstall!", "Exit", 0);
                goto FAIL_EXIT;
            }
            if(!gMUSICLIST->ScanSong()) g_isRun = false;
            else gMUSICLIST->isSongScanning = false;
            wRes->Resize(curcfg.screenHeight); // TODO: Try to not resize twice.
            DispTester(cfg_provider);
        }
        wRes->Resize(curcfg.screenHeight); // TODO: Try to not resize twice.

        if(!gMAINMENU->Setup(cfg_provider)) g_isRun = false;
        if(!gKEYMAPCFGMENU->Setup(cfg_provider)) g_isRun = false;
        if(!gCONFIGMENU->Setup(cfg_provider)) g_isRun = false;
        if(!gRESULTSCREEN->Setup(cfg_provider)) g_isRun = false;
        if(!gGAMEPLAYCTRL->Setup(cfg_provider)) g_isRun = false;
        if(!gMUSICLIST->Setup(cfg_provider)) g_isRun = false;
        gKEYMAPCFGMENU->GetFontHeight();
        gCONFIGMENU->GetFontHeight();
        gMUSICLIST->GetFontHeight();
        gMAINMENU->GetFontHeight();
        if(g_isRun)
        {
            ALLEGRO_MONITOR_INFO monitorInfo;
            al_get_monitor_info(0, &monitorInfo);
            int monitorW = monitorInfo.x2 - monitorInfo.x1;
            int monitorH = monitorInfo.y2 - monitorInfo.y1;
            int windowposX = (monitorW - curcfg.screenWidth)/2;
            int windowposY = (monitorH - curcfg.screenHeight)/2;
            if(windowposX < 0) windowposX = 0;
            if(windowposY < 0) windowposY = 0;
            al_set_window_position(g_display, windowposX, windowposY);
            al_resize_display(g_display, curcfg.screenWidth, curcfg.screenHeight);
            gBGNDWORKER->Start();
            /* Main loop */
            GameLoop(cfg_provider);
        }
        wRes->DestroyFont();
    }
    while(g_isRestart);
FAIL_EXIT:
    gGAMEPLAYCTRL->CleanUp();
    cfg_provider->SaveFile();
    gBGNDWORKER->Stop();
    /* Resource and objects cleanup */
    delete gRESULTSCREEN;
    delete gMUSICLIST;
    delete gCONFIGMENU;
    delete gKEYMAPCFGMENU;
    delete gMAINMENU;
    delete gGAMEEVENTS;
    delete cfg_provider;
    if(g_display != nullptr)
        al_destroy_display(g_display); /* destroy the display */
    g_display = nullptr;
    wRes->DestroyRes();
    delete wRes;
    AllegroDestroy();
    return 0;
}

void DispTester(ConfigProvider* cfg_provider)
{
    W_GAMECONFIG curcfg = cfg_provider->GetConfig();
    double duringTime = 1.5;
    double testStart = al_get_time();
    double testTime = 0;
    ALLEGRO_DISPLAY_MODE dispMode;
    if(!al_get_display_mode(0, &dispMode))
        return;

    int refreshRate = dispMode.refresh_rate;
    printf("Refresh rate = %d\n", refreshRate);
    double logoX = ((double)curcfg.screenWidth - wRes->bitmaps[WR_LOGO].GetScaledWidth()) / 2.0;
    double logoY = ((double)curcfg.screenHeight - wRes->bitmaps[WR_LOGO].GetScaledHeight()) / 2.0;
    int frameCount = 0;
    while(testTime < duringTime)
    {
        testTime = al_get_time() - testStart;
        double tint = testTime/duringTime;
        wRes->bitmaps[WR_LOGO].DrawTinted(logoX, logoY, al_map_rgba_f(tint, tint, tint, tint));
        frameCount++;
        al_flip_display();
        al_clear_to_color(al_map_rgb(0,0,0));
    }
    double averageFPS = frameCount/duringTime;
    printf("averageFPS = %lf\n", averageFPS);

    if(fabs(averageFPS - refreshRate) > 10)
    {
        cfg_provider->ResetVSync(VSYNC_OFF);
    }
    else
    {
        cfg_provider->ResetVSync(VSYNC_ON);
    }
}
