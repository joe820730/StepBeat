@ECHO OFF
set TARGET="bin\Release\StepBeat_x86.exe"
set RAR="%PROGRAMFILES%\WinRAR\WinRAR.exe"
set MINGWPATH="D:\GCC\Compiler\MinGW_x86\bin"
set ALLEGROPATH="D:\GCC\Libraries\allegro_x86"

IF EXIST "%RAR%" (
  goto CopyFile
  ) else (
  goto end
)

:CopyFile
REM Check install directory is exist
IF EXIST install_x86 (
rmdir install_x86 /s /q )
mkdir install_x86
REM Check file is exist
IF EXIST %TARGET% (
xcopy %TARGET% install_x86 )
xcopy res install_x86\res /E /I
xcopy Songs\Songs\alice install_x86\Songs\alice /E /I
xcopy Songs\Songs\hallelujah install_x86\Songs\hallelujah /E /I
xcopy Songs\Songs\gatorix install_x86\Songs\gatorix /E /I
goto Pack

:Pack
FOR /F "usebackq" %%i IN (`where allegro*.dll /r %ALLEGROPATH% ^| findstr /v "debug" ^| findstr /v "monolith"`) Do (
echo %%i
xcopy %%i install_x86
)
FOR /F "usebackq" %%i IN (`where lib*.dll /r %MINGWPATH% ^| findstr /v "fortran"`) DO (
echo %%i
xcopy %%i install_x86
)
cd install_x86
%RAR% a -m3 -r -s -sfx StepBeatInst_x86
%RAR% c StepBeatInst_x86.exe -z"..\installer_comment.txt"
%RAR% s -iiconres/icon.ico StepBeatInst_x86.exe
goto End

:End