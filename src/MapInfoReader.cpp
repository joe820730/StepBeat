#include <iostream>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <unistd.h>
#include <sys/stat.h>

#include "md5.hpp"
#include "globalDef.hpp"
#include "MapInfo.hpp"
#include "debug.hpp"

using namespace std;

void PrintSongInfo(W_SONGLIST* songInfo)
{
    printf("[INFO]\n");
    printf("TITLE = %s\n", songInfo->songTitle.c_str());
    printf("ARTIST = %s\n", songInfo->songAtrist.c_str());
    printf("WAV = %s\n", songInfo->wavPath.c_str());
    printf("BPM = %lf\n", songInfo->songBPM);
    printf("OFFSET = %d\n", songInfo->mapOffset);
    printf("[LEVEL]\n");
    for(int i = 0; i < MAP_TOTAL; i++)
    {
        if(songInfo->haveDiff[i])
        {
            if(!songInfo->mapAuthor[i].empty())
                printf("Map Author of [%s] = %s\n", DiffNameStr[i].c_str(), songInfo->mapAuthor[i].c_str());
            printf("%s = %d\n", DiffNameStr[i].c_str(), songInfo->mapLevel[i]);
        }
    }
}

bool ReadMapAuthorLevel(const char* filepath, std::string& author, int& level)
{
    bool ret = false;
    if(filepath == nullptr)
        return false;
    ALLEGRO_FILE* mapFile = al_fopen(filepath, "rb");
    if(mapFile == nullptr)
        return false;
    std::string tmpLine, tmpKey, tmpStr;
    tmpLine.reserve(64);
    while(!al_feof(mapFile))
    {
        alGetLine(mapFile, tmpLine);
        if(!tmpLine.empty())
        {
            if(tmpLine.at(0) == '#')
            {
                KeyValSplit(tmpLine, tmpKey, tmpStr);
                cout << tmpKey << endl;
                if(tmpKey == "AUTHOR")
                {
                    cout << tmpStr << endl;
                    author = tmpStr;
                    ret = true;
                }
                if(tmpKey == "LEVEL")
                {
                    cout << tmpStr << endl;
                    level = stoi(tmpStr);
                    ret = true;
                }
            }
            if(tmpLine.at(0) == '<')
                break;
        }
    }
    al_fclose(mapFile);
    return ret;
}

int MapInfoReader(const std::string& wdiPath, W_SONGLIST** songInfo)
{
    struct stat mapfile_stat;
    int ret = INFO_SUCCESS;
    bool is_db_exist = false;
    ALLEGRO_CONFIG* mapInfoFile = al_load_config_file(wdiPath.c_str());
    if(mapInfoFile == nullptr)
    {
        return INFO_NOFILE;
    }
    const char* tmpvalue = nullptr;
    (*songInfo) = new W_SONGLIST();
    (*songInfo)->nextSong = nullptr;
    (*songInfo)->wdiPath = wdiPath;
    std::size_t posOfDot = (*songInfo)->wdiPath.find_last_of(".");
    std::size_t posOfSlash = (*songInfo)->wdiPath.find_last_of("\\/");
    for(int32_t i = 0; i < MAP_TOTAL; i++)
    {
        (*songInfo)->mapAuthorustr[i] = nullptr;
    }
    (*songInfo)->filefmt = 1;

    tmpvalue = al_get_config_value(mapInfoFile, "INFO", "FILEFORMAT");
    if(tmpvalue != nullptr)
    {
        (*songInfo)->filefmt = atoi(tmpvalue);
    }
    (*songInfo)->dbPath = (*songInfo)->wdiPath.substr(0, posOfDot + 1) + "wdb";
    is_db_exist = read_score_db((*songInfo)->dbPath, (*songInfo)->scoredb);
    switch((*songInfo)->filefmt)
    {
        case 1:   // Old mapinfo format
        {
            std::string testpath = (*songInfo)->wdiPath.substr(0, posOfDot + 1) + "wdn";
            if(access(testpath.c_str(), F_OK) != 0)
            {
                testpath = (*songInfo)->wdiPath.substr(0, posOfDot + 1) + "wdm";
                if(access(testpath.c_str(), F_OK) != 0)
                {
                    for(int i = 0; i < MAP_TOTAL; i++)
                    {
                        (*songInfo)->haveDiff[i] = false;
                    }
                }
            }
            for(int32_t i = 0; i < MAP_TOTAL; i++)
            {
                (*songInfo)->mapPath[i] = testpath;
            }
            tmpvalue = al_get_config_value(mapInfoFile, "INFO", "MAP_AUTHOR");
            if(tmpvalue != nullptr)
            {
                for(int32_t i = 0; i < MAP_TOTAL; i++)
                {
                    (*songInfo)->mapAuthor[i] = tmpvalue;
                    (*songInfo)->mapAuthorustr[i] = al_ustr_new(tmpvalue);
                }
            }
            bool isNoDiff = true;
            tmpvalue = al_get_config_value(mapInfoFile, "LEVEL", "BASIC");
            if(tmpvalue != nullptr)
            {
                isNoDiff = false;
                (*songInfo)->haveDiff[MAP_BASIC] = true;
                (*songInfo)->mapLevel[MAP_BASIC] = atoi(tmpvalue);
            }
            tmpvalue = al_get_config_value(mapInfoFile, "LEVEL", "NORMAL");
            if(tmpvalue != nullptr)
            {
                isNoDiff = false;
                (*songInfo)->haveDiff[MAP_NORMAL] = true;
                (*songInfo)->mapLevel[MAP_NORMAL] = atoi(tmpvalue);
            }
            tmpvalue = al_get_config_value(mapInfoFile, "LEVEL", "HARD");
            if(tmpvalue != nullptr)
            {
                isNoDiff = false;
                (*songInfo)->haveDiff[MAP_HARD] = true;
                (*songInfo)->mapLevel[MAP_HARD] = atoi(tmpvalue);
            }
            tmpvalue = al_get_config_value(mapInfoFile, "LEVEL", "SPECIAL");
            if(tmpvalue != nullptr)
            {
                isNoDiff = false;
                (*songInfo)->haveDiff[MAP_SPECIAL] = true;
                (*songInfo)->mapLevel[MAP_SPECIAL] = atoi(tmpvalue);
            }
            tmpvalue = al_get_config_value(mapInfoFile, "LEVEL", "EXPERT");
            if(tmpvalue != nullptr)
            {
                isNoDiff = false;
                (*songInfo)->haveDiff[MAP_EXPERT] = true;
                (*songInfo)->mapLevel[MAP_EXPERT] = atoi(tmpvalue);
            }
            if(isNoDiff)
            {
                ret = INFO_NODIFF;
            }
            break;
        }
        case 2:   // New mapinfo format
        {
            bool isNoDiff = true;
            tmpvalue = al_get_config_value(mapInfoFile, "MAPFILE", "BASIC");
            if(tmpvalue != nullptr)
            {
                isNoDiff = false;
                (*songInfo)->mapPath[MAP_BASIC] = (*songInfo)->wdiPath.substr(0, posOfSlash + 1) + tmpvalue;
                std::string author;
                (*songInfo)->haveDiff[MAP_BASIC] = ReadMapAuthorLevel((*songInfo)->mapPath[MAP_BASIC].c_str(), author, (*songInfo)->mapLevel[MAP_BASIC]);
                if(!(*songInfo)->haveDiff[MAP_BASIC])
                {
                    al_show_native_message_box(nullptr, "Read map file error!", "Filename: ", (*songInfo)->mapPath[MAP_BASIC].c_str(), nullptr, ALLEGRO_MESSAGEBOX_ERROR);
                }
                (*songInfo)->mapAuthorustr[MAP_BASIC] = al_ustr_new(author.c_str());
            }
            tmpvalue = al_get_config_value(mapInfoFile, "MAPFILE", "NORMAL");
            if(tmpvalue != nullptr)
            {
                isNoDiff = false;
                (*songInfo)->mapPath[MAP_NORMAL] = (*songInfo)->wdiPath.substr(0, posOfSlash + 1) + tmpvalue;
                std::string author;
                (*songInfo)->haveDiff[MAP_NORMAL] = ReadMapAuthorLevel((*songInfo)->mapPath[MAP_NORMAL].c_str(), author, (*songInfo)->mapLevel[MAP_NORMAL]);
                if(!(*songInfo)->haveDiff[MAP_NORMAL])
                {
                    al_show_native_message_box(nullptr, "Read map file error!", "Filename: ", (*songInfo)->mapPath[MAP_NORMAL].c_str(), nullptr, ALLEGRO_MESSAGEBOX_ERROR);
                }
                (*songInfo)->mapAuthorustr[MAP_NORMAL] = al_ustr_new(author.c_str());
            }
            tmpvalue = al_get_config_value(mapInfoFile, "MAPFILE", "HARD");
            if(tmpvalue != nullptr)
            {
                isNoDiff = false;
                (*songInfo)->mapPath[MAP_HARD] = (*songInfo)->wdiPath.substr(0, posOfSlash + 1) + tmpvalue;
                std::string author;
                (*songInfo)->haveDiff[MAP_HARD] = ReadMapAuthorLevel((*songInfo)->mapPath[MAP_HARD].c_str(), author, (*songInfo)->mapLevel[MAP_HARD]);
                if(!(*songInfo)->haveDiff[MAP_HARD])
                {
                    al_show_native_message_box(nullptr, "Read map file error!", "Filename: ", (*songInfo)->mapPath[MAP_HARD].c_str(), nullptr, ALLEGRO_MESSAGEBOX_ERROR);
                }
                (*songInfo)->mapAuthorustr[MAP_HARD] = al_ustr_new(author.c_str());
            }
            tmpvalue = al_get_config_value(mapInfoFile, "MAPFILE", "SPECIAL");
            if(tmpvalue != nullptr)
            {
                isNoDiff = false;
                (*songInfo)->mapPath[MAP_SPECIAL] = (*songInfo)->wdiPath.substr(0, posOfSlash + 1) + tmpvalue;
                std::string author;
                (*songInfo)->haveDiff[MAP_SPECIAL] = ReadMapAuthorLevel((*songInfo)->mapPath[MAP_SPECIAL].c_str(), author, (*songInfo)->mapLevel[MAP_SPECIAL]);
                if(!(*songInfo)->haveDiff[MAP_SPECIAL])
                {
                    al_show_native_message_box(nullptr, "Read map file error!", "Filename: ", (*songInfo)->mapPath[MAP_SPECIAL].c_str(), nullptr, ALLEGRO_MESSAGEBOX_ERROR);
                }
                (*songInfo)->mapAuthorustr[MAP_SPECIAL] = al_ustr_new(author.c_str());
            }
            tmpvalue = al_get_config_value(mapInfoFile, "MAPFILE", "EXPERT");
            if(tmpvalue != nullptr)
            {
                isNoDiff = false;
                (*songInfo)->mapPath[MAP_EXPERT] = (*songInfo)->wdiPath.substr(0, posOfSlash + 1) + tmpvalue;
                std::string author;
                (*songInfo)->haveDiff[MAP_EXPERT] = ReadMapAuthorLevel((*songInfo)->mapPath[MAP_EXPERT].c_str(), author, (*songInfo)->mapLevel[MAP_EXPERT]);
                if(!(*songInfo)->haveDiff[MAP_EXPERT])
                {
                    al_show_native_message_box(nullptr, "Read map file error!", "Filename: ", (*songInfo)->mapPath[MAP_EXPERT].c_str(), nullptr, ALLEGRO_MESSAGEBOX_ERROR);
                }
                (*songInfo)->mapAuthorustr[MAP_EXPERT] = al_ustr_new(author.c_str());
            }
            if(isNoDiff)
            {
                ret = INFO_NODIFF;
            }
            break;
        }
        default:
        {
            ret = INFO_FORMATERROR;
            break;
        }
    }
    tmpvalue = al_get_config_value(mapInfoFile, "INFO", "TITLE");
    if(tmpvalue != nullptr)
    {
        (*songInfo)->songTitle = tmpvalue;
        (*songInfo)->songTitleustr = al_ustr_new(tmpvalue);
    }
    tmpvalue = al_get_config_value(mapInfoFile, "INFO", "ARTIST");
    if(tmpvalue != nullptr)
    {
        (*songInfo)->songAtrist = tmpvalue;
        (*songInfo)->songAtristustr = al_ustr_new(tmpvalue);
    }
    tmpvalue = al_get_config_value(mapInfoFile, "INFO", "COVER");
    if(tmpvalue != nullptr)
    {
        std::size_t posOfSlash = (*songInfo)->wdiPath.find_last_of("\\/");
        (*songInfo)->coverPath = (*songInfo)->wdiPath.substr(0, posOfSlash + 1) + tmpvalue;
        (*songInfo)->cover = al_load_bitmap((*songInfo)->coverPath.c_str());
    }
    tmpvalue = al_get_config_value(mapInfoFile, "INFO", "WAV");
    if(tmpvalue != nullptr)
    {
        std::size_t posOfSlash = (*songInfo)->wdiPath.find_last_of("\\/");
        (*songInfo)->wavPath = (*songInfo)->wdiPath.substr(0, posOfSlash + 1) + tmpvalue;
    }
    tmpvalue = al_get_config_value(mapInfoFile, "INFO", "BPM");
    if(tmpvalue != nullptr)
    {
        (*songInfo)->songBPM = atof(tmpvalue);
    }
    else
    {
        ret = INFO_NOBPM;
    }
    tmpvalue = al_get_config_value(mapInfoFile, "INFO", "OFFSET");
    if(tmpvalue != nullptr)
    {
        (*songInfo)->mapOffset = atof(tmpvalue);
    }
    else
    {
        (*songInfo)->mapOffset = 0;
    }
    // Score db process
    if(!is_db_exist)
    {
        for(int i = 0; i < MAP_TOTAL; i++)
        {
            (*songInfo)->scoredb[i].diff = i;
            FILE* mapfile = fopen((*songInfo)->mapPath[i].c_str(), "rb");
            md5digist(mapfile, (*songInfo)->scoredb[i].md5sum);
            fclose(mapfile);
            stat((*songInfo)->mapPath[i].c_str(), &mapfile_stat);
            (*songInfo)->scoredb[i].last_modified_time = mapfile_stat.st_mtime;
            for(int j = 0; j < HISCORE_MAX; j++)
            {
                (*songInfo)->scoredb[i].playername[j] = "StepBeat";
                (*songInfo)->scoredb[i].hiscore[j] = 1000;
                (*songInfo)->scoredb[i].accuracy[j] = 0;
                (*songInfo)->scoredb[i].playername_bonus[j] = "StepBeat";
                (*songInfo)->scoredb[i].hiscore_bonus[j] = 1000;
                (*songInfo)->scoredb[i].accuracy_bonus[j] = 0;
            }
        }
        if(write_score_db((*songInfo)->dbPath, (*songInfo)->scoredb))
        {
            printf("OK!\n");
        }
    }
    else
    {
        for(int i = 0; i < MAP_TOTAL; i++)
        {
            stat((*songInfo)->mapPath[i].c_str(), &mapfile_stat);    // If file modified
            if(mapfile_stat.st_mtime != (*songInfo)->scoredb[i].last_modified_time)
            {
                uint8_t tmp_md5sum[16] = {0};
                bool ismd5match = true;
                FILE* mapfile = fopen((*songInfo)->mapPath[i].c_str(), "rb");
                md5digist(mapfile, tmp_md5sum);
                fclose(mapfile);
                for(int j = 0; j < 16; j++)     // check md5sum again
                {
                    if(tmp_md5sum[j] != (*songInfo)->scoredb[i].md5sum[j])   // if md5sum mismatch
                    {
                        memcpy((*songInfo)->scoredb[i].md5sum, tmp_md5sum, sizeof(uint8_t)*16);
                        ismd5match = false;
                        break;
                    }
                }
                if(!ismd5match)
                {
                    if(al_show_native_message_box(nullptr,
                                                  "Map file modified, keep your high score record?",
                                                  (*songInfo)->mapPath[i].c_str(),
                                                  "Press YES to keep high score, Press NO will RESET your high score!",
                                                  nullptr, ALLEGRO_MESSAGEBOX_YES_NO) == 2)  // 1 = YES, 2 = NO
                    {
                        for(int k = 0; k < HISCORE_MAX; k++)
                        {
                            (*songInfo)->scoredb[i].playername[k] = "StepBeat";
                            (*songInfo)->scoredb[i].hiscore[k] = 1000;     // clear score
                            (*songInfo)->scoredb[i].accuracy[k] = 0.0;     // clear score
                            (*songInfo)->scoredb[i].playername_bonus[k] = "StepBeat";
                            (*songInfo)->scoredb[i].hiscore_bonus[k] = 1000;     // clear score
                            (*songInfo)->scoredb[i].accuracy_bonus[k] = 0.0;     // clear score
                        }
                    }
                }
            }
        }
    }
    #ifdef DEBUG
    //PrintSongInfo(*songInfo);
    #endif // DEBUG
    if(ret != INFO_SUCCESS)
    {
        printf("Error!\n");
        delete (*songInfo);
    }
    al_destroy_config(mapInfoFile);
    return ret;
}
