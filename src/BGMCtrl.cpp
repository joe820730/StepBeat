#include <iostream>
#include "BGMCtrl.hpp"

using namespace std;

BGMCtrl::BGMCtrl()
{
    this->music = nullptr;
    this->musicMixer = nullptr;
    this->musicPosition = 0;
    this->isBGMPaused = false;
    this->musicSampleRate = 0;
}

bool BGMCtrl::LoadBGM(const char* bgmPath, int volume)
{
    this->music = al_load_sample(bgmPath);
    if(this->music == nullptr)
    {
        printf("Song file missing, please check map file or song file.\n");
        return false;
    }
    this->musicMixer = al_create_sample_instance(this->music);
    if(musicMixer == nullptr)
    {
        printf("Can't create sample instance!!\n");
        return false;
    }
    if(!al_attach_sample_instance_to_mixer(this->musicMixer, al_get_default_mixer()))
    {
        printf("Can't attach sample instance to mixer!!\n");
        return false;
    }
    musicSampleRate = al_get_sample_instance_frequency(musicMixer);
    if(!al_set_sample_instance_gain(this->musicMixer, (double)volume/100.0))
    {
        printf("Can't set sample instance gain!!\n");
        return false;
    }
    return true;
}

bool BGMCtrl::PlayBGM()
{
    if(this->musicMixer != nullptr && this->music != nullptr)
    {
        if(!al_set_sample_instance_position(this->musicMixer, this->musicPosition))
        {
            printf("Can't set sample instance position!!\n");
            return false;
        }
        else
        {
            if(!al_set_sample_instance_playing(this->musicMixer, true))
            {
                printf("BGM file maybe broken or unknown error.\n");
                return false;
            }
            else
            {
                this->isBGMPaused = false;
                return true;
            }
        }
    }
    else
        return false;
}

bool BGMCtrl::PauseBGM()
{
    if(this->musicMixer != nullptr && this->music != nullptr)
    {
        this->musicPosition = al_get_sample_instance_position(this->musicMixer);
        if(!al_set_sample_instance_playing(this->musicMixer, false))
        {
            printf("BGM file maybe broken or unknown error.\n");
            return false;
        }
        else
        {
            this->isBGMPaused = true;
            return true;
        }
    }
    else
        return false;
}

double BGMCtrl::GetMusicPos()
{
    if(this->music == nullptr || this->musicMixer == nullptr)
        return 0;
    if(isBGMPaused)
        return (double)this->musicPosition / (double)musicSampleRate * 1000.0;
    else
        return (double)al_get_sample_instance_position(musicMixer) / (double)musicSampleRate * 1000.0;
}

void BGMCtrl::SetMusicPos(int posSeconds)
{
    musicPosition = posSeconds*musicSampleRate;
}

bool BGMCtrl::GetBGMStat()
{
    if(this->musicMixer != nullptr)
        return al_get_sample_instance_playing(this->musicMixer);
    else
        return false;
}

void BGMCtrl::Destroy()
{
    if(this->musicMixer != nullptr)
    {
        al_stop_sample_instance(this->musicMixer);
        al_destroy_sample_instance(this->musicMixer);
    }
    if(this->music != nullptr)
        al_destroy_sample(this->music);

    this->music = nullptr;
    this->musicMixer = nullptr;
    this->musicPosition = 0;
}

BGMCtrl::~BGMCtrl()
{
    if(this->musicMixer != nullptr)
    {
        al_stop_sample_instance(this->musicMixer);
        al_destroy_sample_instance(this->musicMixer);
    }
    if(this->music != nullptr)
        al_destroy_sample(this->music);
}
