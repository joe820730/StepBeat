#include "IWidget.hpp"
#include <iostream>

IWidget::IWidget():
    isClickable(true),
    isEntered(false),
    isClicked(false),
    isVisible(false),
    parent(nullptr),
    customdrawfunc(nullptr)
{
    location = {.x = 0, .y = 0};
    widgetsize = {.width = 0, .height = 0};
}

IWidget::~IWidget(){};

void IWidget::MouseListener(ALLEGRO_MOUSE_EVENT ev)
{
    if(ev.x > this->location.x &&
       ev.x < this->location.x + this->widgetsize.width &&
       ev.y > this->location.y &&
       ev.y < this->location.y + this->widgetsize.height)
    {
        isEntered = true;
        if(ev.button == 0)
        {
            isClicked = true;
        }
        else
        {
            isClicked = false;
        }
    }
    else
    {
        isEntered = false;
        isClicked = false;
    }
}

void IWidget::SetLocation(double x, double y)
{
    this->location.x = x;
    this->location.y = y;
}

void IWidget::SetSize(double width, double height)
{
    this->widgetsize.width = width;
    this->widgetsize.height = height;
}

void IWidget::SetVisible(bool visible)
{
    isVisible = visible;
}

void IWidget::SetClickable(bool clickable)
{
    isClickable = clickable;
}

void IWidget::SetParent(IWidget* parent)
{
    if(parent == nullptr)
        return;
    this->parent = parent;
}

void IWidget::AssignChild(IWidget* child)
{
    if(child == nullptr)
        return;
    childs.push_back(child);
    child->SetParent(this);
}

void IWidget::SetCustomDraw(WidgetDrawFunc draw_func)
{
    if(draw_func == nullptr)
        return;
    customdrawfunc = draw_func;
}

void IWidget::CheckClick(int32_t x, int32_t y)
{
    if(x >= location.x && x <= (location.x + widgetsize.width) &&
       y >= location.y && y <= (location.y + widgetsize.height))
        isEntered = true;
    else
        isEntered = false;
}

void IWidget::CheckEnter(int32_t x, int32_t y)
{
    if(x >= location.x && x <= (location.x + widgetsize.width) &&
       y >= location.y && y <= (location.y + widgetsize.height))
        isClicked = true;
    else
        isClicked = false;
}

Button::Button():
    base(nullptr),
    highlight(nullptr),
    clicked(nullptr)
{

}

int Button::SetBitmap(BitmapRes* base, BitmapRes* highlight, BitmapRes* clicked)
{
    this->base = base;
    this->highlight = highlight;
    this->clicked = clicked;
    int w[3] = {0};
    int h[3] = {0};
    if(this->base != nullptr)
    {
        w[0] = this->base->GetWidth();
        h[0] = this->base->GetHeight();
    }
    if(this->highlight != nullptr)
    {
        w[1] = this->highlight->GetWidth();
        h[1] = this->highlight->GetHeight();
    }
    if(this->clicked != nullptr)
    {
        w[2] = this->clicked->GetWidth();
        h[2] = this->clicked->GetHeight();
    }
    int maxw = 0;
    int maxh = 0;
    for(int i = 0; i < 3; i++)
    {
        if(w[i] > maxw)
            maxw = w[i];
        if(h[i] > maxh)
        {
            maxh = h[i];
        }
    }
    this->widgetsize.width = maxw;
    this->widgetsize.height = maxh;
    return 0;
}

void Button::Draw(double deltatime)
{
    if(isClicked)
    {
        if(clicked != nullptr)
        {
            clicked->Draw(location.x, location.y);
            return;
        }
    }
    if(isEntered)
    {
        if(highlight != nullptr)
        {
            highlight->Draw(location.x, location.y);
            return;
        }
    }
    else
    {
        if(base != nullptr)
        {
            base->Draw(location.x, location.y);
            return;
        }
    }
    return;
}

void Page::Draw(double deltatime)
{
    return;
}
