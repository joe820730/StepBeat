#include <iostream>
#include <cstdio>
#include <cstdlib>

#include "KeymapConfig.hpp"
#include "ConfigProvider.hpp"
#include "StateHandler.hpp"

using namespace std;

KeymapConfig::KeymapConfig()
{
    xLoc = KEYCFGMENUX;
    yLoc = KEYCFGMENUY;
    menuIdxMAX = KEYMAP_TOTAL;
    menuText = new string[menuIdxMAX];
    menuText[KEYMAP_UP] = "Up";
    menuText[KEYMAP_DOWN] = "Down";
    menuText[KEYMAP_LEFT] = "Left";
    menuText[KEYMAP_RIGHT] = "Right";
    menuText[KEYMAP_SHOWTIME] = "Show time";
    menuText[KEYMAP_SAVE] = "Save and return";
    keymapCfgState = KEYCFG_SELECT;
    memset(&cpwKeyCfg, 0, sizeof(W_KEYCFG));
    gameCfg = nullptr;
    menuIdx = 0;
    keyGroupIdx = 0;
}

KeymapConfig::~KeymapConfig()
{
    delete [] menuText;
}

void KeymapConfig::GetFontHeight()
{
    fontHeight = al_get_font_line_height(wRes->gameFonts[TITLE_FONT]);
}

bool KeymapConfig::Setup(ConfigProvider* cfg_provider)
{
    if(cfg_provider != nullptr)
    {
        this->gameCfg = cfg_provider;
        SetMenuLoc(gameCfg->GetConfig().screenHeight, gameCfg->GetConfig().screenWidth);
        return true;
    }
    else
    {
        printf("System Error!\n");
        return false;
    }
}

void KeymapConfig::CopyConfig()
{
    if(gameCfg != nullptr)
        cpwKeyCfg = gameCfg->GetKeymap();
}

void KeymapConfig::MenuCtrl(int keyCode)
{
    if(this->keymapCfgState == KEYCFG_SELECT)
    {
        switch(keyCode)
        {
            case ALLEGRO_KEY_UP:
            {
                IdxMinus(menuIdx, menuIdxMAX);
                break;
            }
            case ALLEGRO_KEY_DOWN:
            {
                IdxAdd(menuIdx, menuIdxMAX);
                break;
            }
            case ALLEGRO_KEY_LEFT:
            {
                keyGroupIdx = 0;
                break;
            }
            case ALLEGRO_KEY_RIGHT:
            {
                keyGroupIdx = 1;
                break;
            }
            case ALLEGRO_KEY_PAD_ENTER:
            case ALLEGRO_KEY_ENTER:
            {
                if(menuIdx == KEYMAP_SAVE)
                {
                    if(!gameCfg->SaveKeymap(cpwKeyCfg))
                    {
                        g_isRun = false;
                    }
                    gSTATEHANDLER->SetState(W_SETTING);
                }
                else
                    keymapCfgState = KEYCFG_GRABBING;
                break;
            }
            case ALLEGRO_KEY_ESCAPE:
            {
                gSTATEHANDLER->SetState(W_SETTING);
                break;
            }
        }
    }
    else
    {
        if(keyCode == ALLEGRO_KEY_ESCAPE)
        {
            keymapCfgState = KEYCFG_SELECT;
            gSTATEHANDLER->SetState(W_SETTING);
            return;
        }
        switch(menuIdx)
        {
            case KEYMAP_UP:
            {
                if(keyGroupIdx == 0)
                    cpwKeyCfg.wKeyUp_1 = keyCode;
                else
                    cpwKeyCfg.wKeyUp_2 = keyCode;
                break;
            }
            case KEYMAP_DOWN:
            {
                if(keyGroupIdx == 0)
                    cpwKeyCfg.wKeyDown_1 = keyCode;
                else
                    cpwKeyCfg.wKeyDown_2 = keyCode;
                break;
            }
            case KEYMAP_LEFT:
            {
                if(keyGroupIdx == 0)
                    cpwKeyCfg.wKeyLeft_1 = keyCode;
                else
                    cpwKeyCfg.wKeyLeft_2 = keyCode;
                break;
            }
            case KEYMAP_RIGHT:
            {
                if(keyGroupIdx == 0)
                    cpwKeyCfg.wKeyRight_1 = keyCode;
                else
                    cpwKeyCfg.wKeyRight_2 = keyCode;
                break;
            }
            case KEYMAP_SHOWTIME:
            {
                if(keyGroupIdx == 0)
                    cpwKeyCfg.wKeyShow_1 = keyCode;
                else
                    cpwKeyCfg.wKeyShow_2 = keyCode;
                break;
            }
        }
        keymapCfgState = KEYCFG_SELECT;
    }
}

void KeymapConfig::Draw(double deltaTime)
{
    ALLEGRO_COLOR textColor[2];
    wRes->nineSlices[WR9_INFO_BLOCK].DrawScaled(screenWidth*0.012, screenHeight*0.12, screenWidth*0.976, screenHeight*0.83);
    wRes->bitmaps[WR_TITLE_BAR].DrawScaled(0,0, screenWidth, 0);
    al_draw_text(wRes->gameFonts[TITLE_FONT],  al_map_rgb(255,255,255), screenWidth*0.03, screenHeight*0.01, 0, "Keymap configure");
    for(int i = 0; i < menuIdxMAX; i++)
    {
        if(menuIdx == i)
        {
            if(i != KEYMAP_SAVE)
                al_draw_filled_circle(menuLocateX-screenWidth*0.02,  menuLocateY+fontHeight*i+screenWidth*0.02, screenWidth*0.01, al_map_rgb(0, 213, 235));
            if(keyGroupIdx == 0)
            {
                textColor[0] = al_map_rgb(0, 213, 235);
                textColor[1] = al_map_rgb(255, 255, 255);
            }
            else
            {
                textColor[0] = al_map_rgb(255, 255, 255);
                textColor[1] = al_map_rgb(0, 213, 235);
            }
        }
        else
        {
            textColor[0] = al_map_rgb(255, 255, 255);
            textColor[1] = al_map_rgb(255, 255, 255);
        }
        if(i == KEYMAP_SAVE)
        {
            if(menuIdx == KEYMAP_SAVE)
                al_draw_text( wRes->gameFonts[TITLE_FONT], al_map_rgb(0, 213, 235), screenWidth*0.5, menuLocateY+fontHeight*i+screenHeight*0.05, ALLEGRO_ALIGN_CENTER, menuText[i].c_str());
            else
                al_draw_text( wRes->gameFonts[TITLE_FONT], al_map_rgb(255,255,255), screenWidth*0.5, menuLocateY+fontHeight*i+screenHeight*0.05, ALLEGRO_ALIGN_CENTER, menuText[i].c_str());
        }
        else
            al_draw_text( wRes->gameFonts[TITLE_FONT], al_map_rgb(255,255,255), menuLocateX, menuLocateY+fontHeight*i, ALLEGRO_ALIGN_LEFT, menuText[i].c_str());

        switch(i)
        {
            case KEYMAP_UP:
            {
                al_draw_text( wRes->gameFonts[TITLE_FONT], textColor[0], screenWidth*0.6, menuLocateY+fontHeight*i, ALLEGRO_ALIGN_CENTER, al_keycode_to_name(cpwKeyCfg.wKeyUp_1));
                al_draw_text( wRes->gameFonts[TITLE_FONT], textColor[1], screenWidth*0.8, menuLocateY+fontHeight*i, ALLEGRO_ALIGN_CENTER, al_keycode_to_name(cpwKeyCfg.wKeyUp_2));
                break;
            }
            case KEYMAP_DOWN:
            {
                al_draw_text( wRes->gameFonts[TITLE_FONT], textColor[0], screenWidth*0.6, menuLocateY+fontHeight*i, ALLEGRO_ALIGN_CENTER, al_keycode_to_name(cpwKeyCfg.wKeyDown_1));
                al_draw_text( wRes->gameFonts[TITLE_FONT], textColor[1], screenWidth*0.8, menuLocateY+fontHeight*i, ALLEGRO_ALIGN_CENTER, al_keycode_to_name(cpwKeyCfg.wKeyDown_2));
                break;
            }
            case KEYMAP_LEFT:
            {
                al_draw_text( wRes->gameFonts[TITLE_FONT], textColor[0], screenWidth*0.6, menuLocateY+fontHeight*i, ALLEGRO_ALIGN_CENTER, al_keycode_to_name(cpwKeyCfg.wKeyLeft_1));
                al_draw_text( wRes->gameFonts[TITLE_FONT], textColor[1], screenWidth*0.8, menuLocateY+fontHeight*i, ALLEGRO_ALIGN_CENTER, al_keycode_to_name(cpwKeyCfg.wKeyLeft_2));
                break;
            }
            case KEYMAP_RIGHT:
            {
                al_draw_text( wRes->gameFonts[TITLE_FONT], textColor[0], screenWidth*0.6, menuLocateY+fontHeight*i, ALLEGRO_ALIGN_CENTER, al_keycode_to_name(cpwKeyCfg.wKeyRight_1));
                al_draw_text( wRes->gameFonts[TITLE_FONT], textColor[1], screenWidth*0.8, menuLocateY+fontHeight*i, ALLEGRO_ALIGN_CENTER, al_keycode_to_name(cpwKeyCfg.wKeyRight_2));
                break;
            }
            case KEYMAP_SHOWTIME:
            {
                al_draw_text( wRes->gameFonts[TITLE_FONT], textColor[0], screenWidth*0.6, menuLocateY+fontHeight*i, ALLEGRO_ALIGN_CENTER, al_keycode_to_name(cpwKeyCfg.wKeyShow_1));
                al_draw_text( wRes->gameFonts[TITLE_FONT], textColor[1], screenWidth*0.8, menuLocateY+fontHeight*i, ALLEGRO_ALIGN_CENTER, al_keycode_to_name(cpwKeyCfg.wKeyShow_2));
                break;
            }
        }
    }
    if(keymapCfgState == KEYCFG_GRABBING)
    {
        al_draw_textf( wRes->gameFonts[TITLE_FONT], al_map_rgb(0, 255, 0), screenWidth*0.5, screenHeight*0.5, ALLEGRO_ALIGN_CENTER, "Please press a key.");
    }
}
