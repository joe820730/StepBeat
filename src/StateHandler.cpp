#include "StateHandler.hpp"
#include "MainMenu.hpp"
#include "MusicList.hpp"
#include "GamePlayCtrl.hpp"
#include "ConfigMenu.hpp"
#include "ConfigProvider.hpp"
#include "KeymapConfig.hpp"
#include "ResultScreen.hpp"
#include "GameEvents.hpp"
#include "debug.hpp"

void StateHandler::SetState(W_STATE state)
{
    switch(state)
    {
        case W_MENU:
            this->cur_state = state;
            break;
        case W_SELECT_MUSIC:
            if(this->cur_state == W_PLAYING)
            {
                gGAMEPLAYCTRL->StopPlaying();
            }
            this->cur_state = state;
            break;
        case W_SETTING:
            if(this->cur_state == W_MENU)
            {
                gCONFIGMENU->CopyConfig();
            }
            this->cur_state = state;
            break;
        case W_KEYMAPCFG:
            if(this->cur_state == W_SETTING)
            {
                gKEYMAPCFGMENU->CopyConfig();
            }
            this->cur_state = state;
            break;
        case W_LOADMAP:
            this->cur_state = state;
            break;
        case W_PLAYING:
            this->cur_state = state;
            break;
        case W_RESULT:
            if(this->cur_state == W_PLAYING)
            {
                gGAMEPLAYCTRL->StopPlaying();
            }
            this->cur_state = state;
            break;
        default:
            break;
    }
}
