#ifndef MENUINTERFACE_HPP_INCLUDED
#define MENUINTERFACE_HPP_INCLUDED

#include <cmath>

#include "Allegro_include.hpp"
#include "globalDef.hpp"
#include "GameResources.hpp"
#include "ConfigProvider.hpp"
using namespace std;

inline void ReloadProgram()
{
    g_isRun = false;
    g_isRestart = true;
}

class MenuInterface
{
public:
    virtual ~MenuInterface(){};
    virtual void SetMenuLoc(int scrHeight, int scrWidth);
    virtual void GetFontHeight(){};
    virtual void MenuCtrl(int mouseX, int mouseY){};
    virtual void MenuCtrl(int keyCode){};
    virtual void Draw(double deltaTime){};
protected:
    string* menuText;
    int screenHeight;
    int screenWidth;
    double menuLocateX;
    double menuLocateY;
    double xLoc;
    double yLoc;
    int32_t menuIdx;
    int32_t menuIdxMAX;
    int fontHeight;
};

extern void IdxAdd(int32_t& targetIdx, int32_t targetIdxMAX, int addNum = 1);
extern void IdxMinus(int32_t& targetIdx, int32_t targetIdxMAX, int minusNum = 1);

#endif
