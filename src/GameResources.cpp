#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <unordered_map>

#include "globalDef.hpp"
#include <allegro5/allegro_native_dialog.h>
#include "GameResources.hpp"
#define RES_PATH_PREFIX "./res/"
using namespace std;

const char* kwResPath[WR_TOTAL]
{
    "logo.png",
    "background.jpg",
    "nocover.png",
    "judgearea.png",
    "trackbg.png",
    "showtimebar.png",
    "hittext.png",
    "hitnumslash.png",
    "hitnumx.png",
    "holdtext.png",
    "combo.png",
    "title_bar.png",
    "score_bar.png",
    "song_info_bar.png",
    "popup_bg.png",
};

const int kwResSize[WR_TOTAL]
{
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0
};

const char* kwGroupedResPath[WRG_TOTAL]
{
    "reversed_bg.png",
    "show.png",
    "auto.png",
    "hitnum.png",
    "combonum.png",
    "scorenum.png",
    "showtimetype.png",
    "showtimetext.png",
    "diff_bg.png",
    "showtimebar20.png",
    "pressingeffect.png",
    "judgetext.png",
    "readygo.png"
};

const int kwGroupedResInfo[WRG_TOTAL][3]
{   //{size, row, col}
    {0,3,1},
    {0,2,1},
    {0,2,1},
    {0,10,1},
    {0,10,1},
    {0,10,1},
    {0,SHOWTIME_TOTAL,1},
    {0,SHOWTIME_TOTAL,1},
    {0,MAP_TOTAL+1,1},
    {0,5,4},
    {0,HOLDEFFECTFRAME,1},
    {0,1,RESULT_MISS+1},
    {0,1,2}
};

const char* kw9slices[WR9_TOTAL]
{
    "item_bg.png",
    "item_selected.png",
    "info_block.png",
    "button.png",
    "button_select.png"
};

const int k9slice_point[WR9_TOTAL][4]
{
    {9, 50, 9, 50},
    {9, 50, 9, 50},
    {32, 40, 32, 40},
    {20, 40, 0, 45},
    {20, 40, 0, 45},
};

const char* kArrowResPath[NA_TOTAL][3]
{
    {"up.png","up_bg.png","up_hitted.png"},
    {"down.png","down_bg.png","down_hitted.png"},
    {"left.png","left_bg.png","left_hitted.png"},
    {"right.png","right_bg.png","right_hitted.png"}
};

const int kwArrowBmpInfo[WAB_TOTAL][3]
{
    {0,3,1},
    {0,3,1},
    {0,HITTEDFRAMEMAX,1}
};

const char* kSoundResPath[WS_TOTAL] =
{
    "sound/TAMB_ok.wav",
    "sound/TAMB5_ok.wav",
    "sound/09CL_BOX.wav"
};

bool LoadSingleRes(ALLEGRO_BITMAP*& target, const char* resPath)
{
    target = al_load_bitmap(resPath);
    if(target == nullptr)
    {
        al_show_native_message_box(nullptr, "ERROR!", "Resources missing! File:", resPath, "Exit", 0);
        printf("%s is missing! Please check it.\n", resPath);
        return false;
    }
    else
        return true;
}
bool LoadSingleRes(ALLEGRO_SAMPLE*& target, const char* resPath)
{
    target = al_load_sample(resPath);
    if(target == nullptr)
    {
        al_show_native_message_box(nullptr, "ERROR!", "Resources missing! File:", resPath, "Exit", 0);
        printf("%s is missing! Please check it.\n", resPath);
        return false;
    }
    else
        return true;
}

GameResources::GameResources()
{
    isResLoaded = false;
    for(int i = 0; i < WS_TOTAL; i++)
        wSounds[i] = nullptr;
    for(int i = 0; i < FONT_TOTAL; i++)
        gameFonts[i] = nullptr;
}

bool GameResources::LoadFont(int screenHeight)
{
    int fontSize[4] = {22, 18, 16, 12};
    switch(screenHeight)
    {
        case 720:
            fontSize[0] = 26;
            fontSize[1] = 22;
            fontSize[2] = 20;
            fontSize[3] = 16;
            break;
        case 768:
            fontSize[0] = 28;
            fontSize[1] = 24;
            fontSize[2] = 22;
            fontSize[3] = 18;
            break;
        case 900:
            fontSize[0] = 32;
            fontSize[1] = 28;
            fontSize[2] = 26;
            fontSize[3] = 22;
            break;
        case 1024:
            fontSize[0] = 36;
            fontSize[1] = 32;
            fontSize[2] = 28;
            fontSize[3] = 24;
            break;
        case 1080:
            fontSize[0] = 38;
            fontSize[1] = 34;
            fontSize[2] = 32;
            fontSize[3] = 28;
            break;
        default:
            break;
    }
    int ret = 0;
    for(int i = 0; i < FONT_TOTAL; i++)
    {
        gameFonts[i] = al_load_font("./res/Fonts/NotoSansMonoCJKtc-Regular.otf", fontSize[i], 0 );
        if(gameFonts[i] == nullptr)
        {
            printf("Font missing.\n");
            ret++;
        }
    }
    if(ret > 0)
        return false;
    else
        return true;
}

void GameResources::DestroyFont()
{
    for(int i = 0; i < FONT_TOTAL; i++)
        al_destroy_font(gameFonts[i]);
}

bool GameResources::LoadRes()
{
    int ret = 0;
    std::string pathPrefix, pathStr;
    pathPrefix = RES_PATH_PREFIX;
    pathPrefix += "bitmap/";
    pathStr.reserve(64);
    for(int i = 0; i < WR_TOTAL; i++)
    {
        pathStr = pathPrefix + kwResPath[i];
        ret += bitmaps[i].LoadRes(pathStr.c_str());
        pathStr.clear();
    }
    for(int i = 0; i < WRG_TOTAL; i++)
    {
        pathStr = pathPrefix + kwGroupedResPath[i];
        ret += bmpGroups[i].LoadRes(pathStr.c_str(), kwGroupedResInfo[i][1], kwGroupedResInfo[i][2]);
        pathStr.clear();
    }
    for(int i = 0; i < WR9_TOTAL; i++)
    {
        pathStr = pathPrefix + kw9slices[i];
        ret += nineSlices[i].LoadRes(pathStr.c_str());
        nineSlices[i].SetSlicePoint(k9slice_point[i][0], k9slice_point[i][1], k9slice_point[i][2], k9slice_point[i][3]);
        pathStr.clear();
    }
    for(int i = 0; i < NA_TOTAL; i++)
    {
        for(int j = 0; j < WAB_TOTAL; j++)
        {
            pathStr = pathPrefix + kArrowResPath[i][j];
            arrows[i][j].LoadRes(pathStr.c_str(), kwArrowBmpInfo[j][1], kwArrowBmpInfo[j][2]);
            pathStr.clear();
        }
    }

    pathPrefix = RES_PATH_PREFIX;
    for(int i = 0; i < WS_TOTAL; i++)
    {
        pathStr = pathPrefix + kSoundResPath[i];
        if(!LoadSingleRes(wSounds[i], pathStr.c_str())) ret++;
        pathStr.clear();
    }
    if(ret > 0)
        return false;
    else
        return true;
}

void GameResources::Resize(int screenH)
{
    for(int i = 0; i < WR_TOTAL; i++)
    {
        bitmaps[i].ResizeRes(screenH, 0);
    }
    for(int i = 0; i < WRG_TOTAL; i++)
    {
        bmpGroups[i].ResizeRes(screenH, kwGroupedResInfo[i][0]);
    }
    for(int i = 0; i < NA_TOTAL; i++)
    {
        for(int j = 0; j < WAB_TOTAL; j++)
        {
            arrows[i][j].ResizeRes(screenH, kwArrowBmpInfo[j][0]);
        }
    }
}

void GameResources::DestroyRes()
{
    for(int i = 0; i < WR_TOTAL; i++)
        bitmaps[i].DestroyRes();
    for(int i = 0; i < WR9_TOTAL; i++)
        nineSlices[i].DestroyRes();
    for(int i = 0; i < WRG_TOTAL; i++)
        bmpGroups[i].DestroyRes();
    for(int i = 0; i < NA_TOTAL; i++)
    {
        for(int j = 0; j < WAB_TOTAL; j++)
            arrows[i][j].DestroyRes();
    }
    for(int i = 0; i < WS_TOTAL; i++)
        al_destroy_sample(wSounds[i]);
}
