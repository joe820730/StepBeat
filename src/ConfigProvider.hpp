#ifndef SYSCONFIG_HPP_INCLUDED
#define SYSCONFIG_HPP_INCLUDED

#define WCFG_KEYMAP_SEC "KEYMAP"
#define WCFG_KEYUP1 "KEY_UP_1"
#define WCFG_KEYUP2 "KEY_UP_2"
#define WCFG_KEYDOWN1 "KEY_DOWN_1"
#define WCFG_KEYDOWN2 "KEY_DOWN_2"
#define WCFG_KEYLEFT1 "KEY_LEFT_1"
#define WCFG_KEYLEFT2 "KEY_LEFT_2"
#define WCFG_KEYRIGHT1 "KEY_RIGHT_1"
#define WCFG_KEYRIGHT2 "KEY_RIGHT_2"
#define WCFG_KEYSHOW1 "KEY_SHOW_1"
#define WCFG_KEYSHOW2 "KEY_SHOW_2"

#define WCFG_CONFIG_SEC "CONFIG"
#define WCFG_CONFIG_FPS "FPS"
#define WCFG_CONFIG_VSYNC "VSYNC"
#define WCFG_CONFIG_OFFSET "OFFSET"
#define WCFG_CONFIG_DIM "DIM"
#define WCFG_CONFIG_SCR_W "SCREEN_W"
#define WCFG_CONFIG_SCR_H "SCREEN_H"

#define WCFG_VOL_SEC "VOLUME"
#define WCFG_VOL_MUSIC "MUSIC"
#define WCFG_VOL_EFFECT "EFFECT"

#define WCFG_SHOW_SEC "SHOWTIME"
#define WCFG_SHOW_SPEEDUP "SPEEDUP"
#define WCFG_SHOW_SPEEDDOWN "SPEEDDOWN"
#define WCFG_SHOW_ROTATE "ROTATE"
#define WCFG_SHOW_HIDDEN "HIDDEN"
#define WCFG_SHOW_REVERSE "REVERSE"

#include <string>

#include "globalDef.hpp"
#include "Allegro_include.hpp"
#include "NoteInfo.hpp"
#define STRBUFLEN 16
#define SHOWTIME_LEVELS 6

using namespace std;

typedef enum
{
    VSYNC_DEFAULT,
    VSYNC_ON,
    VSYNC_OFF
}VSYNC_OPTION;

typedef struct game_keycfg
{
    int wKeyUp_1;
    int wKeyUp_2;
    int wKeyDown_1;
    int wKeyDown_2;
    int wKeyLeft_1;
    int wKeyLeft_2;
    int wKeyRight_1;
    int wKeyRight_2;
    int wKeyShow_1;
    int wKeyShow_2;
}W_KEYCFG;

typedef struct game_corecfg
{
    int offsetMs;
    int fps;
    int32_t bgDim;
    int screenWidth;
    int screenHeight;
    int32_t musicVol;
    int32_t effectVol;
    int32_t vsyncOption;
}W_GAMECONFIG;

class ConfigProvider
{
public:
    explicit ConfigProvider();
    ~ConfigProvider();
    bool LoadFile(const string& cfgFileName);
    bool SaveFile();
    bool SaveConfig(W_GAMECONFIG& cfg);
    bool SaveKeymap(W_KEYCFG& keymap);
    bool SaveShowtime(int32_t showtimecfg[]);
    W_GAMECONFIG GetConfig();
    W_KEYCFG GetKeymap();
    int32_t* GetShowtime();
    void ResetVSync(int32_t vsyncOpt);
private:
    bool ReadFile();
    W_GAMECONFIG wGameCfg;
    W_KEYCFG wKeyConfig;
    int32_t showtimeConfig[SHOWTIME_TOTAL];
    string cfgFileName;
    ALLEGRO_CONFIG* gameConfig;
    void GenDefaultCfg();
};

typedef struct
{
    int screenWidth;
    int screenHeight;
}W_AVAILABLE_RESOLUTION;

typedef struct
{
    int fps[6];
    W_AVAILABLE_RESOLUTION resolution[9];
}W_AVAILABLE_OPTION;

static const W_AVAILABLE_OPTION wAvailableOption =
{
    {0,60,90,120,240,1},
    {
        {800,600},
        {1024,600},
        {1024,768},
        {1280,720},
        {1280,1024},
        {1366,768},
        {1440,900},
        {1600,900},
        {1920,1080}
    }
};

extern NOTE_ARROW KeyCodeToArrow(int keyCode, const W_KEYCFG* wKeyCfg);

#endif // SYSCONFIG_HPP_INCLUDED
