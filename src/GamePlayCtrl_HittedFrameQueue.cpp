#include "GamePlayCtrl.hpp"
#include "GameResources.hpp"
#include <cstdio>
#include <cstdlib>
#include <iostream>

HittedEventQueue::HittedEventQueue()
{
    Reset();
}

void HittedEventQueue::RecalcLoc()
{
    locOffset = fabs(wRes->arrows[0][WAB_BG].GetHeight() -
                      wRes->arrows[0][WAB_HITTED].GetHeight()) * gScaleRate / 2.0;
}

void HittedEventQueue::Reset()
{
    nowHead = 0;
    nowTail = 0;
    for(int i = 0; i < HITTEDFRAMEMAX; i++)
    {
        nowFrameQueue[i].arrowType = NA_TOTAL;
        nowFrameQueue[i].frame = -1;
    }
    hitFrameTime = 0;
}

void HittedEventQueue::AttachEvent(int arrow)
{
    this->nowFrameQueue[nowHead].frame = 0;
    this->nowFrameQueue[nowHead].arrowType = arrow;
    if(nowHead < HITTEDFRAMEMAX-1)
        nowHead++;
    else
        nowHead = 0;
}

void HittedEventQueue::DrawEffect(double xLoc, double yLoc, double deltaTime)
{
    int nowPtr = nowTail;
    bool isNext = false;
    hitFrameTime += deltaTime;
    if(hitFrameTime > 16)
    {
        isNext = true;
        hitFrameTime = 0;
    }
    else
    {
        isNext = false;
    }
    while(nowPtr != nowHead)
    {
        // Queue processing.
        if(isNext)
        {
            if(nowFrameQueue[nowPtr].frame >= 0 && nowFrameQueue[nowPtr].frame < HITTEDFRAMEMAX-2)
            {
                nowFrameQueue[nowPtr].frame++;
            }
            else
            {
                nowFrameQueue[nowPtr].arrowType = NA_TOTAL;
                nowFrameQueue[nowPtr].frame = -1;
                nowTail = nowPtr;
            }
        }
        // Draw effect here.
        if(nowFrameQueue[nowPtr].frame >= 0)
        {
            wRes->arrows[nowFrameQueue[nowPtr].arrowType][WAB_HITTED].Draw(xLoc-locOffset, yLoc-locOffset, nowFrameQueue[nowPtr].frame);
        }
        // Next Queue.
        if(nowPtr < HITTEDFRAMEMAX-1)
            nowPtr++;
        else
            nowPtr = 0;
    }
}
