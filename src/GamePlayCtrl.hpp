#ifndef JUDGEINFO_HPP_INCLUDED
#define JUDGEINFO_HPP_INCLUDED

#include <thread>

#include "globalDef.hpp"
#include "NoteInfo.hpp"
#include "MusicList.hpp"
#include "ConfigProvider.hpp"
#include "GameResources.hpp"
#include "BGMCtrl.hpp"
#include "MapReader.hpp"

#define BASE_SPEED 0.0024

/* Judgment offset, maybe add to configure file in future. */
#define JUDGE_PERFECT 33
#define JUDGE_GREAT 67
#define JUDGE_COOL 100
#define JUDGE_BAD 166.6
#define AUTOPLAY_PERFECT 16.6

#define PLAYINGLEVELX   0.01
#define PLAYINGLEVELY   0.91
#define PLAYINGINFOX    0.08
#define PLAYINGINFOY    0.89
#define PLAYINGTIMEX    0.01
#define PLAYINGTIMEY    0.01

#define JUDGEAREAX      0.7
#define JUDGEAREAY      0.66
#define SHOWTIMETEXTX   0.83    // Showtime score multiplier text
#define SHOWTIMETEXTY   0.672   // Showtime score multiplier text
#define JUDGETEXTY      0.53    // Current note judgment text
#define HITTEXTY        0.645    // Hitting note text y-position
#define HITNUMY         0.7    // Hitting note count y-position
#define HOLDTEXTY       0.68    // Hold note text y-position

#define HITTEXT_CNTX    0.64    // Hitting count text y-position
#define HITTEXT_CNTY    0.48    // Hitting count text y-position
#define HITNUM_CNTX     0.655   // Hitting count x-position
#define HITNUM_CNTY     0.54    // Hitting count y-position


inline double CalcNotePosition(double globalSpeed, double noteSpeed, double nowTime, double noteTime, double noteBpm, double note_judge_area)
{
    return BASE_SPEED * gScaleRate * globalSpeed * noteSpeed * (nowTime - noteTime) * noteBpm + note_judge_area;
}

typedef struct hittedframeinfo
{
    int arrowType;
    int frame;
}HITTEDFRAMEINFO;

class HittedEventQueue
{
public:
    HittedEventQueue();
    void AttachEvent(int arrow);
    void DrawEffect(double xLoc, double yLoc, double deltaTime);
    void Reset();
    void RecalcLoc();
private:
    HITTEDFRAMEINFO nowFrameQueue[HITTEDFRAMEMAX];
    int nowHead, nowTail;
    double hitFrameTime;
    double locOffset;
};

class GamePlayCtrl
{
public:
    GamePlayCtrl();
    ~GamePlayCtrl(){};
    bool Setup(ConfigProvider* cfg_provider);
    void Reset();
    void KeyDownEvent(int keyCode);
    void KeyUpEvent(int keyCode);
    void MapHandle(double deltaTime);
    void CleanUp();
    void LoadMap(W_SONGLIST* selectedSong, W_MAPDIFF selectedDiff);
    void StopPlaying();
    /* Game states and control */
    bool isAutoPlay;
    bool enableShowTime;
    bool isPaused;
    void GameStateCtrl(int keyCode);
    /* Score */
    int scoreRecord[RESULT_TOTAL];
    double accuracy;
    /* Drawing function */
    void Draw(double deltaTime);
    /* Speed control */
    void SpeedUp();
    void SpeedDown();
    /* Effect sound control */
    bool StopPressSound();
private:
    W_GAMECONFIG gameCfg;
    W_KEYCFG keyCfg;
    void ResizeObj(int screenW, int screenH);
    void MoveNote(double nowTime);
    void AutoPlay(double nowTime);
    W_SONGLIST* cur_song;
    W_MAPDIFF cur_song_diff;
    bool isMapPlaying;
    bool isMusicPlaying;
    W_TIMERINFO timers;
    double maphandle_cost_time;
    double timerStart;
    double timerNow;
    double pausedTime;
    double escapeTimerStamp;
    BGMCtrl bgmCtrl;
    bool PressSoundPreProc();
    bool PressSoundSetVol();
    /* Note */
    NOTES_BARLIST* notes_bar;
    NOTES_BARLIST* last_bar;
    NOTES_LINKLIST* judgeNote_long[NA_TOTAL]; //Due to long notes have different judge method, I record it in another variable.
    NOTES_LINKLIST* judgeNote_keep; //Due to long notes have different judge method, I record it in another variable.
    MapReader* mapReader;
    void DrawSingleNote(NOTES_LINKLIST* nowPtr);
    void DrawLongNote(NOTES_LINKLIST* nowPtr);
    void DrawLongBg(NOTES_LINKLIST* nowPtr, double _length, bool isReverse = false);
    /* KeyHandle */
    bool isPressing[NA_TOTAL];
    bool isHitting;
    /* Judgment */
    void NoteJudge(double judgeTime, NOTES_LINKLIST* nowPtr);
    void HittingNoteJudge(NOTES_LINKLIST* hitNotePtr);
    void PressingNoteJudge(NOTES_LINKLIST* holdNotePtr);
    void FindJudgeNote(double nowTime);
    /* Judgment */
    int nowCombo;
    uint8_t latestJudge; //For showing judge text
    int hitCount;
    int hitCountTarget;
    int maxNotes;
    double maxAccuracy;
    void AddJudge(uint8_t judgeType, NOTES_LINKLIST* curNote);
    /* Drawing info */
    void ShowStatistic();
    void DrawTrackBackground();
    void DrawNote();
    void DrawHittedEffect(double deltaTime);
    void DrawKeepHitCount();
    void DrawJudgment(double deltaTime);
    double noteSize;
    double noteSpeed;
    double note_judge_area;
    double note_y_pos;
    double judgeFrameTime;
    double comboTextScale;
    double holdFrameTime;
    int holdFrameIdx;
    HittedEventQueue hittedQueue;
    /* Autoplay control */
    void AutoPlayReset();
    /* ShowTime control */
    void ShowTimeStart(double nowTime);
    void SetShowTimeGatherMax(W_MAPDIFF selectedDiff);
    void ShowTimeCtrl(double deltaTime);
    const int32_t *showtimeConfig;
    W_SHOWTIME_TYPE showType;
    bool isShowTime;
    uint32_t showtime_StartId;
    int showtimegather;
    int showtimegatherMax;
    double showtime_targetSpeed;
    int hiddenDistance;
    int rotateDistance;
    double showtime_scoremultiplier;
    /* Press sound instance */
    ALLEGRO_SAMPLE_INSTANCE* pressSound;
};

extern GamePlayCtrl* gGAMEPLAYCTRL;
#endif // JUDGEINFO_HPP_INCLUDED
