#ifndef GAMEEVENTS_HPP_INCLUDED
#define GAMEEVENTS_HPP_INCLUDED

#include "Allegro_include.hpp"

#define W_EVENT_BASE_ID ALLEGRO_GET_EVENT_TYPE('W', 'e', 'D', 'o')

#define W_EVENTS_MAP(XX)\
    XX(SCANSONG_BEGIN)\
    XX(SCANSONG_END)\
    XX(LOADMAP_BEGIN)\
    XX(LOADMAP_END)\
    XX(RESET_FPS)\
    XX(EXIT)

enum W_EVENT_ID
{
    W_EVENT_ID_MIN = W_EVENT_BASE_ID,
#define XX(evid) W_EVENT_ID_##evid,
    W_EVENTS_MAP(XX)
#undef XX
    W_EVENT_ID_MAX
};

typedef struct
{
    ALLEGRO_EVENT_SOURCE ev_src;
    ALLEGRO_EVENT ev;
}W_EVENTS;

class GameEvents
{
public:
    GameEvents();
    ~GameEvents();
    ALLEGRO_EVENT_SOURCE* GetEventSource(int event_id);
    bool EmitEvent(int event_id, intptr_t data1 = 0, intptr_t data2 = 0, intptr_t data3 = 0, intptr_t data4 = 0);
private:
    enum W_EVENT_LIST
    {
    #define XX(evid) W_EVENT_##evid,
        W_EVENTS_MAP(XX)
    #undef XX
        W_EVENTS_MAX
    };
    #define W_EVENTS_TOTAL W_EVENTS_MAX+1
    W_EVENTS eventlist[W_EVENTS_TOTAL];
    int EventIDToIdx(int event_id);
};

extern GameEvents* gGAMEEVENTS;

#endif // GAMEEVENTS_HPP_INCLUDED
