#include <iostream>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include "MapReader.hpp"
#include "GamePlayCtrl.hpp"
#include "debug.hpp"

using namespace std;

/* Note:
 * If a song set BPM = 150,
 * MEASURE = 3/4, so per bar have 3 beats,
 * If mapper write 12 notes in a bar,
 * Each note time is 60 / ( 150 / (3/4) * (12/3) ) = 60/800=0.075
 * Else if MEASURE = 4/4, and there have 12 notes in a bar,
 * each note time is 60 / ( 150 / (4/4) * (12/4) ) = 0.13333.
 * Another example is my previous map in WeDo (Ice - Entrance).
 * It's BPM=180, and each bar has 12 notes, but each bar is fixed to 16 notes in WeDo editor.
 * So ( 180 / (4/4) * (12/4) ) = 540 = ( 135 / (4/4) * (16/4) ).
 * https://en.wikipedia.org/wiki/Metre_(music)
 */

MapReader::MapReader()
{
    nowBPM = 0.0;
    noteTime = 0.0;
    Refresh();
}

bool MapReader::ReadMap(NOTES_BARLIST*& bars, W_SONGLIST *songInfo, W_MAPDIFF targetDiff, uint32_t& totalNote, NOTES_BARLIST*& last_bar)
{
    std::size_t posOfDot = songInfo->mapPath[targetDiff].find_last_of(".");
    std::string suffix = songInfo->mapPath[targetDiff].substr(posOfDot + 1, std::string::npos);
    if(suffix == "wdn")
    {
        return ReadMap_New(bars, songInfo, targetDiff, totalNote, last_bar);
    }
    else if(suffix == "wdm")
    {
        return ReadMap_Old(bars, songInfo, targetDiff, totalNote, last_bar);
    }
    else
    {
        return false;
    }
}

void ErrorMsg(char msg[], int atline)
{
    snprintf(msg, ERRORMSGLEN, "Map error at line %d :", atline);
}

bool FixedArrow(NOTE_ARROW& nArrow, char c)
{
    switch(c)
    {
        case 'W':
        case 'T':
        case 'I':
        {
            nArrow = NA_UP;
            return true;
        }
        case 'S':
        case 'G':
        case 'K':
        {
            nArrow = NA_DOWN;
            return true;
        }
        case 'A':
        case 'F':
        case 'J':
        {
            nArrow = NA_LEFT;
            return true;
        }
        case 'D':
        case 'H':
        case 'L':
        {
            nArrow = NA_RIGHT;
            return true;
        }
        case '1':
        case '2':
        case '4':
        default:
        {
            nArrow = GenArrow();
            return false;
        }
    }
}

void MapReader::Refresh()
{
    isDiffMatch = false;
    isMapEmpty = true;
    isMapEnd = false;
    nowSpeed = 1.0;
    nowBPB = 4; //Beats per bar
    nowBeatLen = 4; // Default is quarter note because it usual.
    nowBarLen = 16; //Record now bar length
    nowInterval = (double)nowBarLen / (double)nowBPB;
    noteTime_start = 0;
    noteTime_end = 0;
    tmpLong = nullptr;
    isHitWhenPress = false;
    isHitWhenRelease = false;
    nType = NTYPE_TOTAL;
    nType_prev = NTYPE_TOTAL;
    nArrow = NA_TOTAL;
    noteId = 1;
    nowLine = 0; // For showing map error.
    isFixedArrow = false;
    nowPtrPtr = nullptr;
    errormsg[ERRORMSGLEN] = {0};
}

bool MapReader::MapLineParser(NOTES_BARLIST*& bars, W_SONGLIST *songInfo, W_MAPDIFF targetDiff, std::string& tmpLine)
{
    std::string tmpStr; // For analyzing file.
    std::string tmpKey; // For analyzing value.
    tmpStr.reserve(32);
    tmpKey.reserve(16);
    bool isReadNote = false;
    switch(tmpLine.at(0))
    {
        case '<': // Difficult start
        {
            isReadNote = false;
            LOG("Check map block.\n");
            size_t endpos = 0;
            for(size_t i = 1, tmplen = tmpLine.length(); i < tmplen; i++)
            {
                if(tmpLine.at(i) == '>')
                {
                    endpos = i - 1;
                    break;
                }
            }
            if(endpos == 0)
            {
                ErrorMsg(errormsg, nowLine);
                al_show_native_message_box(nullptr, "ERROR!", errormsg, "Missing '>' character, file format error! Please fix it.", "OK", ALLEGRO_MESSAGEBOX_ERROR);
                return false;
            }
            if(tmpLine.substr(1, endpos) != DiffNameStr[targetDiff])
                isDiffMatch = false;
            else
                isDiffMatch = true;
            break;
        }
        case '#': //BPM or NBM change
        {
            if(isDiffMatch)
            {
                isReadNote = false;
                KeyValSplit(tmpLine, tmpKey, tmpStr);
                if(tmpKey == "BPM")
                {
                    std::string _tmpBPM;
                    _tmpBPM.reserve(8);
                    for(size_t i = 0, tmpstrlen = tmpStr.length(); i < tmpstrlen; i++)
                    {
                        if(tmpStr.at(i) >= '0' && tmpStr.at(i) <= '9')
                            _tmpBPM += tmpStr.at(i);
                        else if(tmpStr.at(i) == '.')
                            _tmpBPM += tmpStr.at(i);
                    }
                    nowBPM = stof(_tmpBPM);
                    LOG("BPM = %lf\n", nowBPM);
                    _tmpBPM.erase();
                }
                else if(tmpKey == "MEASURE")
                {
                    std::string _tmpMeasure;
                    _tmpMeasure.reserve(8);
                    for(size_t i = 0, tmpstrlen = tmpStr.length(); i < tmpstrlen; i++)
                    {
                        switch(tmpStr.at(i))
                        {
                            case '/':
                            {
                                nowBPB = stoi(_tmpMeasure);
                                _tmpMeasure.erase();
                                break;
                            }
                            default:
                            {
                                if(tmpStr.at(i) >= '0' && tmpStr.at(i) <= '9')
                                    _tmpMeasure += tmpStr.at(i);
                                break;
                            }
                        }
                    }
                    if(_tmpMeasure.length() != 0)
                        nowBeatLen = stoi(_tmpMeasure);
                    LOG("MEASURE = %d/%d\n", nowBPB, nowBeatLen);
                }
                else if(tmpKey == "SPEED")
                {
                    std::string _tmpSpeed;
                    _tmpSpeed.reserve(8);
                    for(size_t i = 0, tmpstrlen = tmpStr.length(); i < tmpstrlen; i++)
                    {
                        if(tmpStr.at(i) >= '0' && tmpStr.at(i) <= '9')
                            _tmpSpeed += tmpStr.at(i);
                        else if(tmpStr.at(i) == '.' || tmpStr.at(i) == '-')
                            _tmpSpeed += tmpStr.at(i);
                    }
                    nowSpeed = stof(_tmpSpeed);
                    LOG("BPM = %lf\n", nowSpeed);
                    _tmpSpeed.erase();
                }
                else if(tmpKey == "END")
                    isMapEnd = true;
                else if(tmpKey == "EDITSTART")
                {
                    songInfo->beginAt = noteTime - 1000.0;
                    if(songInfo->beginAt < 0)
                        songInfo->beginAt = 0;
                    songInfo->isEditing = true;
                    NOTES_BARLIST** tempPtrPtr = &bars;
                    while(tempPtrPtr != nowbarPtrPtr)
                    {
                        (*tempPtrPtr)->active = false;
                        (*tempPtrPtr)->visible = false;
                        tempPtrPtr = &((*tempPtrPtr)->nextBar);
                    }
                }
                else if((tmpKey == "AUTHOR") || (tmpKey == "LEVEL"))
                {
                    isReadNote = false;
                }
                else
                {
                    ErrorMsg(errormsg, nowLine);
                    al_show_native_message_box(nullptr, "ERROR!", errormsg, "Unknown command! Please check map file.", "OK", ALLEGRO_MESSAGEBOX_ERROR);
                    return false;
                }
            }
            break;
        }
        case ';':
        {
            isReadNote = false;
            break;
        }
        default:
        {
            if(isDiffMatch)
            {
                LOG_ALL("Block founded.");
                isReadNote = true;
                isMapEmpty = false;
                break;
            }
            break;
        }
    }
    return isReadNote;
}

bool MapReader::ReadMap_Old(NOTES_BARLIST*& bars, W_SONGLIST *songInfo, W_MAPDIFF targetDiff, uint32_t& totalNote, NOTES_BARLIST*& last_bar)
{
    songInfo->beginAt = 0;
    songInfo->isEditing = false;
    nowBPM = songInfo->songBPM;
    noteTime = songInfo->mapOffset;
    Refresh();
    nowbarPtrPtr = &bars;
    ALLEGRO_FILE* mapFile;
    NOTES_LINKLIST *tmpKhit = nullptr;
    NOTES_BARLIST *prevbarPtr = nullptr;
    NOTES_LINKLIST *prevPtr = nullptr;
    NOTES_LINKLIST *prevPtr2 = nullptr;
    NOTE_ARROW prevLongArrow = NA_TOTAL;
    mapFile = al_fopen(songInfo->mapPath[targetDiff].c_str(), "rb");
    if(!mapFile)
    {
        printf("Error!\n");
        return false;
    }
    else
    {
        bool isReadNote = false;
        std::string tmpLine; // For analyzing file.
        tmpLine.reserve(64);
        while(!al_feof(mapFile) && !isMapEnd)
        {
            alGetLine(mapFile, tmpLine);
            nowLine++;
            if(!tmpLine.empty())
            {
                // Line analysis
                isReadNote = MapLineParser(bars, songInfo, targetDiff, tmpLine);

                // If the map block is we need, analysis it.
                if(isReadNote)
                {
                    nowBarLen = tmpLine.length();
                    nowInterval = (double)nowBarLen / (double)nowBPB / (BEATLENUSUAL / (double)nowBeatLen);
                    if(prevbarPtr != nullptr)
                    {
                        prevbarPtr->barPosition_end = CalcNotePosition(1, nowSpeed, EMPTY_TIME, noteTime, nowBPM, 0);
                        prevbarPtr->barTime_end = noteTime;
                        prevbarPtr->last_note = prevPtr2;
                    }
                    InsertBar(nowbarPtrPtr, noteTime, nowBPM, nowSpeed);
                    (*nowbarPtrPtr)->prevBar = prevbarPtr;
                    prevPtr2 = nullptr;
                    prevbarPtr = (*nowbarPtrPtr);
                    nowPtrPtr = &((*nowbarPtrPtr)->notes);
                    for(size_t i = 0; i < nowBarLen; i++)
                    {
                        switch(tmpLine.at(i))
                        {
                            case '0': // Empty note
                            {
                                break;
                            }
                            case '1': // Single hit note
                            case 'W':
                            case 'S':
                            case 'A':
                            case 'D':
                            {
                                nType = NTYPE_HIT;
                                if(tmpLong != nullptr)
                                    isHitWhenPress = true;
                                isFixedArrow = FixedArrow(nArrow, tmpLine.at(i));
                                if(!isFixedArrow)
                                    nArrow = GenArrow();
                                InsertNote(nowPtrPtr, noteTime, noteTime, NTYPE_HIT, nArrow, nowBPM, nowSpeed, isFixedArrow, noteId);
                                if(tmpLong != nullptr)
                                {
                                    while((*nowPtrPtr)->noteArrow == tmpLong->noteArrow)
                                        (*nowPtrPtr)->noteArrow = GenArrow();
                                    tmpLong->isSingleInLong = true;
                                    (*nowPtrPtr)->isSingleInLong = true;
                                }
                                noteId++;
                                (*nowPtrPtr)->prevNote = prevPtr2;
                                prevPtr = (*nowPtrPtr);
                                prevPtr2 = (*nowPtrPtr);
                                nowPtrPtr = &((*nowPtrPtr)->nextNote);
                                break;
                            }
                            case '2': // Hold note
                            case 'T':
                            case 'G':
                            case 'F':
                            case 'H':
                            {
                                if(tmpLong == nullptr)
                                {
                                    isFixedArrow = FixedArrow(nArrow, tmpLine.at(i));
                                    if(!isFixedArrow)
                                    {
                                        while(nArrow == prevLongArrow)
                                            nArrow = GenArrow();
                                    }
                                    InsertNote(nowPtrPtr, noteTime, noteTime, NTYPE_HOLD, nArrow, nowBPM, nowSpeed, isFixedArrow, noteId);
                                    tmpLong = (*nowPtrPtr);
                                    noteId+=2;
                                    (*nowPtrPtr)->prevNote = prevPtr2;
                                    prevPtr = (*nowPtrPtr);
                                    prevPtr2 = (*nowPtrPtr);
                                    nowPtrPtr = &((*nowPtrPtr)->nextNote);
                                }
                                break;
                            }
                            case '6':
                            {
                                if(tmpLong != nullptr)
                                {
                                    tmpLong->isSingleInLong = true;
                                    InsertNote(nowPtrPtr, noteTime, noteTime, NTYPE_HIT, nArrow, nowBPM, nowSpeed, false, noteId);
                                    (*nowPtrPtr)->isSingleInLong = true;
                                    while((*nowPtrPtr)->noteArrow == tmpLong->noteArrow)
                                        (*nowPtrPtr)->noteArrow = GenArrow();
                                    noteId++;
                                    (*nowPtrPtr)->prevNote = prevPtr2;
                                    prevPtr = (*nowPtrPtr);
                                    prevPtr2 = (*nowPtrPtr);
                                    nowPtrPtr = &((*nowPtrPtr)->nextNote);
                                }
                            }
                            case '3': // Hold note end
                            {
                                if(tmpLong != nullptr)
                                {
                                    tmpLong->noteTime_end = noteTime;
                                    tmpLong->notePosition_end = CalcNotePosition(1, tmpLong->noteSpeed, EMPTY_TIME, tmpLong->noteTime_end, tmpLong->noteBpm, 0);
                                    prevLongArrow = tmpLong->noteArrow;
                                    tmpLong = nullptr;
                                }
                                break;
                            }
                            case '4': // Keep hitting note
                            case 'I':
                            case 'K':
                            case 'J':
                            case 'L':
                            {
                                if(tmpKhit == nullptr)
                                {
                                    isFixedArrow = FixedArrow(nArrow, tmpLine.at(i));
                                    if(!isFixedArrow)
                                        nArrow = GenArrow();
                                    InsertNote(nowPtrPtr, noteTime, noteTime, NTYPE_KHIT, nArrow, nowBPM, nowSpeed, isFixedArrow, noteId, 0, 0);
                                    tmpKhit = (*nowPtrPtr);
                                    noteId++;
                                    (*nowPtrPtr)->prevNote = prevPtr2;
                                    prevPtr = (*nowPtrPtr);
                                    prevPtr2 = (*nowPtrPtr);
                                    nowPtrPtr = &((*nowPtrPtr)->nextNote);
                                }
                                break;
                            }
                            case '5': // Keep hitting note end
                            {
                                if(tmpKhit != nullptr)
                                {
                                    tmpKhit->noteTime_end = noteTime;
                                    tmpKhit->notePosition_end = CalcNotePosition(1, tmpKhit->noteSpeed, EMPTY_TIME, tmpKhit->noteTime_end, tmpKhit->noteBpm, 0);

                                    int khitDrawCount = 0, hitCountTarget = 0;
                                    khitDrawCount = (tmpKhit->noteTime_end - tmpKhit->noteTime)/(MIN_TO_SEC/(nowBPM*4.0)*1000);
                                    if(targetDiff == MAP_BASIC)
                                        hitCountTarget = (khitDrawCount+1)/4.0;
                                    else if(targetDiff == MAP_NORMAL)
                                        hitCountTarget = (khitDrawCount+1)/2.0;
                                    else
                                        hitCountTarget = khitDrawCount+1;
                                    if(hitCountTarget == 0)
                                        hitCountTarget = 1;
                                    tmpKhit->khitDrawCount = khitDrawCount;
                                    tmpKhit->hitCountTarget = hitCountTarget;
                                    tmpKhit = nullptr;
                                }
                                break;
                            }
                            default:
                            {
                                ErrorMsg(errormsg, nowLine);
                                char errmsg_invalidchar[64] = {0};
                                snprintf(errmsg_invalidchar, ERRORMSGLEN, "Invalid character '%c', please remove it!", tmpLine.at(i));
                                al_show_native_message_box(nullptr, "ERROR!", errormsg, errmsg_invalidchar, "OK", ALLEGRO_MESSAGEBOX_ERROR);
                                return false;
                            }
                        }
                        noteTime += (MIN_TO_SEC / (nowBPM*nowInterval)) * 1000.0;
                        nType_prev = nType;
                    }
                    nowbarPtrPtr = &((*nowbarPtrPtr)->nextBar);
                    #ifdef DEBUG_MORE
                    printf("\n");
                    #endif
                }
            }
        }
    }
    #ifdef DEBUG_MORE
    printf("\n");
    #endif
    if(!isMapEmpty)
    {
        totalNote = prevPtr->noteId;
        if(prevPtr->noteType == NTYPE_HOLD)
        {
            totalNote++;
        }
        prevbarPtr->barTime_end = noteTime;
        last_bar = prevbarPtr;
        prevbarPtr->last_note = prevPtr2;
        return true;
    }
    else
    {
        ErrorMsg(errormsg, nowLine);
        al_show_native_message_box(nullptr, "ERROR!", errormsg, "Selected map doesn't exist!", "OK", ALLEGRO_MESSAGEBOX_ERROR);
        songInfo->haveDiff[targetDiff] = false;
        return false;
    }
}
