#include <iostream>
#include <cstdio>
#include <cstdlib>
#include "BitmapRes.hpp"
#include "ConfigProvider.hpp"

GroupedBitmap::GroupedBitmap()
{
    grouped = nullptr;
    bitmaparray = nullptr;
    arraytotal = 0;
}

void GroupedBitmap::DestroyRes()
{
    for(int i = 0; i < arraytotal; i++)
    {
        bitmaparray[i].DestroyRes();
    }
    delete [] bitmaparray;
    al_destroy_bitmap(grouped);
}

int GroupedBitmap::LoadRes(const char* resPath, int colcount, int rowcount)
{
    grouped = al_load_bitmap(resPath);
    if(grouped == nullptr)
    {
        al_show_native_message_box(nullptr, "ERROR!", "Resources missing! File:", resPath, "OK", 0);
        printf("%s is missing! Please check it.\n", resPath);
        return 1;
    }
    else
    {
        sourcew = al_get_bitmap_width(grouped);
        sourceh = al_get_bitmap_height(grouped);
        isLoaded = true;
        int ret = 0;
        if(rowcount >= 1 || colcount >= 1)
        {
            arraytotal = rowcount * colcount;
            bitmaparray = new BitmapRes[arraytotal];
            int bitmapidx = 0;
            double eachw = sourcew / colcount;
            double eachh = sourceh / rowcount;
            for(int i = 0; i < rowcount; i++)
            {
                for(int j = 0; j < colcount; j++)
                {
                    if(bitmaparray[bitmapidx].LoadRes(grouped, eachw*j, eachh*i, eachw, eachh) > 0)
                        ret++;
                    bitmapidx++;
                }
            }
            if(ret == 0)
                return 0;
            else
                return 1;
        }
        return 0;
    }
}

double GroupedBitmap::GetWidth()
{
    return bitmaparray[0].GetWidth();
}

double GroupedBitmap::GetHeight()
{
    return bitmaparray[0].GetHeight();
}

double GroupedBitmap::GetScaledWidth()
{
    return bitmaparray[0].GetScaledWidth();
}

double GroupedBitmap::GetScaledHeight()
{
    return bitmaparray[0].GetScaledHeight();
}

void GroupedBitmap::ResizeRes(int screenH, double scale)
{
    for(int i = 0; i < arraytotal; i++)
        bitmaparray[i].ResizeRes(screenH, scale);
}

void GroupedBitmap::Draw(double xloc, double yloc, int idx)
{
    if(idx >= 0 && idx < arraytotal)
        bitmaparray[idx].Draw(xloc, yloc);
}

void GroupedBitmap::DrawScaled(double xloc, double yloc, double xsize, double ysize, int idx)
{
    if(idx >= 0 && idx < arraytotal)
        bitmaparray[idx].DrawScaled(xloc, yloc, xsize, ysize);
}

void GroupedBitmap::DrawRotate(double xloc, double yloc, double angle, int idx)
{
    if(idx >= 0 && idx < arraytotal)
        bitmaparray[idx].DrawRotate(xloc, yloc, angle);
}

void GroupedBitmap::DrawTinted(double xloc, double yloc, ALLEGRO_COLOR tint, int idx)
{
    if(idx >= 0 && idx < arraytotal)
        bitmaparray[idx].DrawTinted(xloc, yloc, tint);
}

GroupedBitmapNineSlice::GroupedBitmapNineSlice()
{
    bitmaparray = nullptr;
    isLoaded = false;
    grouped = nullptr;
    arraytotal = 0;
    sourcew = 0;
    sourceh = 0;
}

int GroupedBitmapNineSlice::LoadRes(const char* resPath, int colcount, int rowcount)
{
    grouped = al_load_bitmap(resPath);
    if(grouped == nullptr)
    {
        al_show_native_message_box(nullptr, "ERROR!", "Resources missing! File:", resPath, "OK", 0);
        printf("%s is missing! Please check it.\n", resPath);
        return 1;
    }
    else
    {
        sourcew = al_get_bitmap_width(grouped);
        sourceh = al_get_bitmap_height(grouped);
        isLoaded = true;
        int ret = 0;
        if(rowcount >= 1 || colcount >= 1)
        {
            arraytotal = rowcount * colcount;
            bitmaparray = new NineSliceBitmap[arraytotal];
            int bitmapidx = 0;
            double eachw = sourcew / colcount;
            double eachh = sourceh / rowcount;
            for(int i = 0; i < rowcount; i++)
            {
                for(int j = 0; j < colcount; j++)
                {
                    if(bitmaparray[bitmapidx].LoadRes(grouped, eachw*j, eachh*i, eachw, eachh) > 0)
                        ret++;
                    bitmapidx++;
                }
            }
            if(ret == 0)
                return 0;
            else
                return 1;
        }
        return 0;
    }
}

void GroupedBitmapNineSlice::SetSlicePoint(int x1, int x2, int y1, int y2)
{
    for(int i = 0; i < arraytotal; i++)
    {
        bitmaparray[i].SetSlicePoint(x1, x2, y1, y2);
    }
}

double GroupedBitmapNineSlice::GetWidth()
{
    return bitmaparray[0].GetWidth();
}

double GroupedBitmapNineSlice::GetHeight()
{
    return bitmaparray[0].GetHeight();
}

double GroupedBitmapNineSlice::GetScaledWidth()
{
    return bitmaparray[0].GetScaledWidth();
}

double GroupedBitmapNineSlice::GetScaledHeight()
{
    return bitmaparray[0].GetScaledHeight();
}

void GroupedBitmapNineSlice::Draw(double xloc, double yloc, int idx)
{
    if(idx >= 0 && idx < arraytotal)
        bitmaparray[idx].Draw(xloc, yloc);
}

void GroupedBitmapNineSlice::DrawScaled(double xloc, double yloc, double xsize, double ysize, int idx)
{
    if(idx >= 0 && idx < arraytotal)
        bitmaparray[idx].DrawScaled(xloc, yloc, xsize, ysize);
}

void GroupedBitmapNineSlice::DrawRotate(double xloc, double yloc, double angle, int idx)
{
    if(idx >= 0 && idx < arraytotal)
        bitmaparray[idx].DrawRotate(xloc, yloc, angle);
}

void GroupedBitmapNineSlice::DrawTinted(double xloc, double yloc, ALLEGRO_COLOR tint, int idx)
{
    if(idx >= 0 && idx < arraytotal)
        bitmaparray[idx].DrawTinted(xloc, yloc, tint);
}

void GroupedBitmapNineSlice::DestroyRes()
{
    for(int i = 0; i < arraytotal; i++)
    {
        bitmaparray[i].DestroyRes();
    }
    delete [] bitmaparray;
    al_destroy_bitmap(grouped);
}

