#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <chrono>

#include "Allegro_include.hpp"
#include "globalDef.hpp"
#include "MainMenu.hpp"
#include "ConfigProvider.hpp"
#include "StateHandler.hpp"
#include "GameEvents.hpp"

using namespace std;

MainMenu::MainMenu()
{
    xLoc = MAINMENUX;
    yLoc = MAINMENUY;
    menuIdxMAX = W_MENU_TOTAL;
    menuText = new string[menuIdxMAX];
    menuText[W_MENU_SELECT_MUSIC] = "Select Music";
    menuText[W_MENU_CONFIG] = "Configure";
    menuText[W_MENU_RELOAD] = "Reload";
    menuText[W_MENU_EXIT] = "Exit";
    menuIdx = 0;
}

MainMenu::~MainMenu()
{
    delete [] menuText;
}

bool MainMenu::Setup(ConfigProvider* cfg_provider)
{
    if(cfg_provider == nullptr)
        return false;
    SetMenuLoc(cfg_provider->GetConfig().screenHeight, cfg_provider->GetConfig().screenWidth);
    return true;
}

void MainMenu::GetFontHeight()
{
    fontHeight = al_get_font_line_height(wRes->gameFonts[TITLE_FONT]);
}

void MainMenu::MenuCtrl(int mouseX, int mouseY)
{
    if(mouseX >= screenWidth*0.3 && mouseX <= screenWidth*0.7)
    {
        menuIdx = (mouseY-menuLocateY)/fontHeight;
    }
    else
        menuIdx = -1;
}

void MainMenu::MenuCtrl(int keyCode)
{
    switch(keyCode)
    {
        case ALLEGRO_KEY_UP:
        {
            IdxMinus(menuIdx, menuIdxMAX);
            break;
        }
        case ALLEGRO_KEY_DOWN:
        {
            IdxAdd(menuIdx, menuIdxMAX);
            break;
        }
        case ALLEGRO_KEY_PAD_ENTER:
        case ALLEGRO_KEY_ENTER:
        {
            MenuEnter();
            break;
        }
    }
}

void MainMenu::MenuEnter()
{
    switch(menuIdx)
    {
        case W_MENU_SELECT_MUSIC:
        {
            gSTATEHANDLER->SetState(W_SELECT_MUSIC);
            break;
        }
        case W_MENU_CONFIG:
        {
            gSTATEHANDLER->SetState(W_SETTING);
            break;
        }
        case W_MENU_RELOAD:
        {
            ReloadProgram();
            break;
        }
        case W_MENU_EXIT:
        {
            g_isRun = false;
            gGAMEEVENTS->EmitEvent(W_EVENT_ID_EXIT);
            break;
        }
        default:
        {
            break;
        }
    }
}

void MainMenu::Draw(double deltaTime)
{
    wRes->bitmaps[WR_LOGO].Draw(screenWidth*0.5-(wRes->bitmaps[WR_LOGO].GetScaledWidth()/2), screenHeight*0.2);
    double item_height = screenHeight * 0.08;
    double text_offset = screenHeight * 0.01;
    double item_width = screenWidth * 0.24;
    double xpos = menuLocateX - (item_width/2.0);
    for(uint8_t i = 0; i < menuIdxMAX; i++)
    {
        double ypos = menuLocateY+item_height*i;
        if(this->menuIdx == i)
        {
            wRes->nineSlices[WR9_BUTTON_SELECT].DrawScaled(xpos, ypos, item_width, item_height);
        }
        else
        {
            wRes->nineSlices[WR9_BUTTON].DrawScaled(xpos, ypos, item_width, item_height);
        }
        al_draw_text( wRes->gameFonts[TITLE_FONT], al_map_rgb(255,255,255),
                     menuLocateX, ypos+text_offset, ALLEGRO_ALIGN_CENTER, this->menuText[i].c_str());
    }
}
