#include <iostream>

#include "BGACtrl.hpp"
#include "GameResources.hpp"

BGACtrl::BGACtrl()
{
    this->bga = nullptr;
    this->bgaFrame = nullptr;
    this->screenWidth = 0;
    this->screenHeight = 0;
    this->isBGAPaused = false;
    this->isBGAPlayed = false;
    this->isBGALoaded = false;
}

BGACtrl::~BGACtrl()
{
    if(this->bga != nullptr)
    {
        al_close_video(this->bga);
        this->bga = nullptr;
    }
}

bool BGACtrl::LoadBGA(const char* bgaPath, int screenWidth, int screenHeight)
{
    if(bgaPath != nullptr)
    {
        this->bga = al_open_video(bgaPath);
        this->screenWidth = screenWidth;
        this->screenHeight = screenHeight;
        if(bga != nullptr)
        {
            isBGALoaded = true;
            return true;
        }
        else
            return false;
    }
    else
        return false;
}

void BGACtrl::PlayBGA()
{
    if(!isBGAPlayed && bga != nullptr)
    {
        al_start_video(bga, nullptr);
        isBGAPlayed = true;
    }
}

void BGACtrl::Pause()
{
    if(bga != nullptr)
    {
        al_set_video_playing(bga, false);
        isBGAPaused = true;
    }
}

void BGACtrl::Resume()
{
    if(bga != nullptr)
    {
        al_set_video_playing(bga, true);
        isBGAPaused = false;
    }
}

void BGACtrl::DrawBGA()
{
    if(bga != nullptr)
    {
        if(!isBGAPaused)
        {
            bgaFrame = al_get_video_frame(bga);
        }
        DrawBitmap(bgaFrame, 0, 0, screenWidth, screenHeight);
    }
}

void BGACtrl::Destroy()
{
    if(this->bga != nullptr)
    {
        this->Pause();
        al_close_video(this->bga);
        this->bga = nullptr;
        this->isBGAPaused = false;
        this->isBGAPlayed = false;
        this->isBGALoaded = false;
    }
}
