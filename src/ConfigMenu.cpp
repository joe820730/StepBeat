#include <iostream>

#include "Allegro_include.hpp"
#include "ConfigProvider.hpp"
#include "ConfigMenu.hpp"
#include "MenuInterface.hpp"
#include "GamePlayCtrl.hpp"
#include "StateHandler.hpp"
#include "GameEvents.hpp"

using namespace std;

ConfigMenu::ConfigMenu()
{
    xLoc = CONFIGMENUX;
    yLoc = CONFIGMENUY;
    fpsOptIdx = 0;
    resolutionOptIdx = 0;
    showtimeIdx = 0;
    menuIdxMAX = CFG_TOTAL;
    menuText = new string[menuIdxMAX];
    menuText[CFG_OFFSET]     = "Global offset";
    menuText[CFG_RESOLUTION] = "Resolution";
    menuText[CFG_FPS]        = "FPS";
    menuText[CFG_BGDIM]      = "Background Dim";
    menuText[CFG_MUSICVOL]   = "Music Volume";
    menuText[CFG_EFFECTVOL]  = "Effect Volume";
    menuText[CFG_KEYMAP]     = "Keymap Setting";
    menuText[CFG_SAVE]       = "Save and return";
    menuIdx = 0;
    gameCfg = nullptr;
    memset(&cpGameCfg, 0, sizeof(W_GAMECONFIG));
    for(int i = 0; i < SHOWTIME_TOTAL; i++)
        cpshowtimeConfig[i] = 1;
    isShowTimeCfgVisible = false;
}

ConfigMenu::~ConfigMenu()
{
    delete [] menuText;
}

void ConfigMenu::GetFontHeight()
{
    fontHeight = al_get_font_line_height(wRes->gameFonts[TITLE_FONT]);
}

bool ConfigMenu::Setup(ConfigProvider* cfg_provider)
{
    if(cfg_provider != nullptr)
    {
        this->gameCfg = cfg_provider;
        SetMenuLoc(gameCfg->GetConfig().screenHeight, gameCfg->GetConfig().screenWidth);
        return true;
    }
    else
    {
        printf("System Error!\n");
        return false;
    }
}

void ConfigMenu::CopyConfig()
{
    if(gameCfg == NULL)
        return;
    cpGameCfg = gameCfg->GetConfig();
    for(unsigned int i = 0; i < sizeof(wAvailableOption.fps)/sizeof(int); i++)
    {
        if(cpGameCfg.fps == wAvailableOption.fps[i])
            fpsOptIdx = i;
    }
    for(unsigned int i = 0; i < sizeof(wAvailableOption.resolution)/sizeof(W_AVAILABLE_RESOLUTION); i++)
    {
        if(cpGameCfg.screenHeight == wAvailableOption.resolution[i].screenHeight &&
           cpGameCfg.screenWidth == wAvailableOption.resolution[i].screenWidth)
            resolutionOptIdx = i;
    }
    int32_t* curshowtime = gameCfg->GetShowtime();
    for(int i = 0; i < SHOWTIME_TOTAL; i++)
        cpshowtimeConfig[i] = curshowtime[i];
}

void ConfigMenu::Draw(double deltaTime)
{
    wRes->nineSlices[WR9_INFO_BLOCK].DrawScaled(screenWidth*0.012, screenHeight*0.12, screenWidth*0.976, screenHeight*0.83);
    wRes->bitmaps[WR_TITLE_BAR].DrawScaled(0,0, screenWidth, 0);
    al_draw_text(wRes->gameFonts[TITLE_FONT],  al_map_rgb(255,255,255), screenWidth*0.03, screenHeight*0.01, 0, "Settings");
    for(uint8_t i = 0; i < menuIdxMAX; i++)
    {
        if(menuIdx == i)
        {
            al_draw_filled_circle(menuLocateX-screenWidth*0.02,  menuLocateY+fontHeight*i+screenWidth*0.02, screenWidth*0.01, al_map_rgb(0, 213, 235));
            al_draw_text( wRes->gameFonts[TITLE_FONT], al_map_rgb(0, 213, 235), menuLocateX, menuLocateY+fontHeight*i, ALLEGRO_ALIGN_LEFT, menuText[i].c_str());
        }
        else
        {
            al_draw_text( wRes->gameFonts[TITLE_FONT], al_map_rgb(255,255,255), menuLocateX, menuLocateY+fontHeight*i, ALLEGRO_ALIGN_LEFT, menuText[i].c_str());
        }

        switch(i)
        {
            case CFG_OFFSET:
            {
                al_draw_textf( wRes->gameFonts[TITLE_FONT], al_map_rgb(255,255,255), screenWidth*(1-xLoc), menuLocateY+fontHeight*i, ALLEGRO_ALIGN_RIGHT, "%d", cpGameCfg.offsetMs);
                break;
            }
            case CFG_RESOLUTION:
            {
                al_draw_textf( wRes->gameFonts[TITLE_FONT], al_map_rgb(255,255,255), screenWidth*(1-xLoc), menuLocateY+fontHeight*i, ALLEGRO_ALIGN_RIGHT, "%dx%d", cpGameCfg.screenWidth, cpGameCfg.screenHeight);
                break;
            }
            case CFG_FPS:
            {
                if((cpGameCfg.fps >= 60) && (cpGameCfg.fps <= 240))
                    al_draw_textf( wRes->gameFonts[TITLE_FONT], al_map_rgb(255,255,255), screenWidth*(1-xLoc), menuLocateY+fontHeight*i, ALLEGRO_ALIGN_RIGHT, "%d", cpGameCfg.fps);
                else if(cpGameCfg.fps == 1)
                    al_draw_text( wRes->gameFonts[TITLE_FONT], al_map_rgb(255,255,255), screenWidth*(1-xLoc), menuLocateY+fontHeight*i, ALLEGRO_ALIGN_RIGHT, "Unlimit(When playing)");
                else if(cpGameCfg.fps == 0)
                    al_draw_text( wRes->gameFonts[TITLE_FONT], al_map_rgb(255,255,255), screenWidth*(1-xLoc), menuLocateY+fontHeight*i, ALLEGRO_ALIGN_RIGHT, "VSync");
                break;
            }
            case CFG_BGDIM:
            {
                al_draw_textf( wRes->gameFonts[TITLE_FONT], al_map_rgb(255,255,255), screenWidth*(1-xLoc), menuLocateY+fontHeight*i, ALLEGRO_ALIGN_RIGHT, "%d", cpGameCfg.bgDim);
                break;
            }
            case CFG_MUSICVOL:
            {
                al_draw_textf( wRes->gameFonts[TITLE_FONT], al_map_rgb(255,255,255), screenWidth*(1-xLoc), menuLocateY+fontHeight*i, ALLEGRO_ALIGN_RIGHT, "%d", cpGameCfg.musicVol);
                break;
            }
            case CFG_EFFECTVOL:
            {
                al_draw_textf( wRes->gameFonts[TITLE_FONT], al_map_rgb(255,255,255), screenWidth*(1-xLoc), menuLocateY+fontHeight*i, ALLEGRO_ALIGN_RIGHT, "%d", cpGameCfg.effectVol);
                break;
            }
            default:
                break;
        }
    }
    al_draw_text( wRes->gameFonts[TITLE_FONT], al_map_rgb(255,255,255), screenWidth*0.5, screenHeight * 0.7, ALLEGRO_ALIGN_CENTER, "Press ESC to cancel and return to menu.");
}

void ConfigMenu::MenuCtrl(int mouseX, int mouseY)
{
    if(mouseX >= menuLocateX && mouseX <= screenWidth - menuLocateX)
    {
        menuIdx = (mouseY-menuLocateY)/fontHeight;
    }
    else
        menuIdx = -1;
}

void ConfigMenu::MenuCtrl(int keyCode)
{
    switch(keyCode)
    {
        case ALLEGRO_KEY_UP:
        {
            IdxMinus(menuIdx, menuIdxMAX);
            break;
        }
        case ALLEGRO_KEY_DOWN:
        {
            IdxAdd(menuIdx, menuIdxMAX);
            break;
        }
        case ALLEGRO_KEY_LEFT:
        case ALLEGRO_KEY_RIGHT:
        {
            ConfigSet(keyCode);
            break;
        }
        case ALLEGRO_KEY_PGDN:
        {
            IdxAdd(menuIdx, menuIdxMAX, 3);
            break;
        }
        case ALLEGRO_KEY_PGUP:
        {
            IdxMinus(menuIdx, menuIdxMAX, 3);
            break;
        }
        case ALLEGRO_KEY_PAD_ENTER:
        case ALLEGRO_KEY_ENTER:
        {
            if(menuIdx == CFG_KEYMAP)
            {
                gSTATEHANDLER->SetState(W_KEYMAPCFG);
            }
            else if(menuIdx == CFG_SAVE)
            {
                // TODO: To make "save config" a common case, which can emitted by event.
                gSTATEHANDLER->SetState(W_MENU);
                CheckIsNeedRestart();
            }
            break;
        }
        case ALLEGRO_KEY_ESCAPE:
        {
            gSTATEHANDLER->SetState(W_MENU);
            break;
        }
        default:
            break;
    }
}

// TODO: Remove this function or improve it.
bool ConfigMenu::CheckIsNeedRestart()
{
    bool isNeedRestart = false;
    if(gameCfg != nullptr)
    {
        W_GAMECONFIG curcfg = gameCfg->GetConfig();
        (this->cpGameCfg.fps == 0)?(this->cpGameCfg.vsyncOption = VSYNC_ON):(this->cpGameCfg.vsyncOption = VSYNC_OFF);
        gameCfg->SaveConfig(cpGameCfg);
        if(curcfg.screenHeight != cpGameCfg.screenHeight ||
           curcfg.screenWidth != cpGameCfg.screenWidth   ||
           curcfg.vsyncOption != cpGameCfg.vsyncOption)
        {
            g_isRestart = true;
            g_isRun = false;
        }
        if(curcfg.vsyncOption != cpGameCfg.vsyncOption)
        {
           g_needRestartDisp = true;
        }
        if((cpGameCfg.vsyncOption == VSYNC_OFF) && (cpGameCfg.fps != 0) && (curcfg.fps != cpGameCfg.fps))
        {
            gGAMEEVENTS->EmitEvent(W_EVENT_ID_RESET_FPS);
        }
    }
    return isNeedRestart;
}

void ConfigMenu::ConfigSet(int keyCode)
{
    switch(menuIdx)
    {
        case CFG_OFFSET:
        {
            break;
        }
        case CFG_FPS:
        {
            switch(keyCode)
            {
                case ALLEGRO_KEY_LEFT:
                {
                    IdxMinus(fpsOptIdx, sizeof(wAvailableOption.fps)/sizeof(int));
                    break;
                }
                case ALLEGRO_KEY_RIGHT:
                {
                    IdxAdd(fpsOptIdx, sizeof(wAvailableOption.fps)/sizeof(int));
                    break;
                }
            }
            cpGameCfg.fps = wAvailableOption.fps[fpsOptIdx];
            break;
        }
        case CFG_BGDIM:
        {
            switch(keyCode)
            {
                case ALLEGRO_KEY_LEFT:
                {
                    IdxMinus(cpGameCfg.bgDim, 101, 5);
                    break;
                }
                case ALLEGRO_KEY_RIGHT:
                {
                    IdxAdd(cpGameCfg.bgDim, 101, 5);
                    break;
                }
            }
            break;
        }
        case CFG_RESOLUTION:
        {
            switch(keyCode)
            {
                case ALLEGRO_KEY_LEFT:
                {
                    IdxMinus(resolutionOptIdx, sizeof(wAvailableOption.resolution)/sizeof(W_AVAILABLE_RESOLUTION));
                    break;
                }
                case ALLEGRO_KEY_RIGHT:
                {
                    IdxAdd(resolutionOptIdx, sizeof(wAvailableOption.resolution)/sizeof(W_AVAILABLE_RESOLUTION));
                    break;
                }
            }
            cpGameCfg.screenHeight = wAvailableOption.resolution[resolutionOptIdx].screenHeight;
            cpGameCfg.screenWidth = wAvailableOption.resolution[resolutionOptIdx].screenWidth;
            break;
        }
        case CFG_MUSICVOL:
        {
            switch(keyCode)
            {
                case ALLEGRO_KEY_LEFT:
                {
                    IdxMinus(cpGameCfg.musicVol, 101, 5);
                    break;
                }
                case ALLEGRO_KEY_RIGHT:
                {
                    IdxAdd(cpGameCfg.musicVol, 101, 5);
                    break;
                }
            }
            break;
        }
        case CFG_EFFECTVOL:
        {
            switch(keyCode)
            {
                case ALLEGRO_KEY_LEFT:
                {
                    IdxMinus(cpGameCfg.effectVol, 101, 5);
                    break;
                }
                case ALLEGRO_KEY_RIGHT:
                {
                    IdxAdd(cpGameCfg.effectVol, 101, 5);
                    break;
                }
            }
            break;
        }
        default:
            break;
    }
}
