#ifndef STATEHANDLER_H
#define STATEHANDLER_H
#include "globalDef.hpp"

class StateHandler
{
    public:
        StateHandler():cur_state(W_MENU){};
        virtual ~StateHandler(){};
        W_STATE GetState() { return cur_state; }
        void SetState(W_STATE state);
    protected:

    private:
        W_STATE cur_state;
};

extern StateHandler* gSTATEHANDLER;

#endif // STATEHANDLER_H
