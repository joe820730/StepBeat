#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>

#include "Theme.hpp"

void ThemeManager::DefaultTheme()
{
    theme.gaugebaroffsetX = 18;
    theme.gaugebaroffsetY = 45;
    theme.judgeAreaOffsetX = 57.0;
    theme.judgeAreaOffsetY = 36.0;
    theme.comboTextX = 0.23;
    theme.comboTextY = 0.55;
}

ThemeManager::ThemeManager()
{
    DefaultTheme();
}

ThemeManager::~ThemeManager()
{
}
