#ifndef MAPINFO_HPP_INCLUDED
#define MAPINFO_HPP_INCLUDED
#include <string>
#include "NoteInfo.hpp"
#include "Allegro_include.hpp"
#include "globalDef.hpp"
#include "ScoreDB.hpp"

extern std::istream& safeGetline(std::istream& _is, std::string& t);
extern void alGetLine(ALLEGRO_FILE* fp, std::string& strbuf);
extern void KeyValSplit(const std::string& tmpLine, std::string& tmpKey, std::string& tmpStr);

static std::string DiffNameStr[MAP_TOTAL]
{
    "BASIC",
    "NORMAL",
    "HARD",
    "SPECIAL",
    "EXPERT"
};

typedef struct _songList
{
    uint16_t listIdx;
    uint8_t filefmt;
    std::string wdiPath;
    std::string mapPath[MAP_TOTAL];
    std::string dbPath;
    std::string wavPath;
    std::string coverPath;
    std::string songTitle;
    std::string songAtrist;
    std::string mapAuthor[MAP_TOTAL];
    ALLEGRO_USTR* songTitleustr;
    ALLEGRO_USTR* songAtristustr;
    ALLEGRO_USTR* mapAuthorustr[MAP_TOTAL];
    double songBPM;
    int mapOffset;
    bool haveDiff[MAP_TOTAL];
    int32_t mapLevel[MAP_TOTAL];
    bool isEditing;
    double beginAt;
    ALLEGRO_BITMAP* cover;
    W_SCOREDB scoredb[MAP_TOTAL];
    _songList* nextSong;
    _songList* prevSong;
}W_SONGLIST;

enum
{
    INFO_SUCCESS,
    INFO_NOFILE,
    INFO_NOBPM,
    INFO_NOWAV,
    INFO_NODIFF,
    INFO_FORMATERROR
};

enum
{
    SCAN_SUCCESS,
    SCAN_NODIR,
    SCAN_PATH_ERROR,
    SCAN_UNKNOWN
};

extern int MapInfoReader(const std::string& wdiPath, W_SONGLIST** songInfo);
extern void SongListClean(W_SONGLIST *head);
extern int ReadDir(const char* songdir_path, W_SONGLIST** songList);

#endif // MUSICINFO_HPP_INCLUDED
