#ifndef GAMEPLAYRENDER_HPP_INCLUDED
#define GAMEPLAYRENDER_HPP_INCLUDED

class GamePlayRender
{
public:
    /* Drawing function */
    void ShowStatistic();
    void DrawTrackBackground();
    void DrawCombo();
    void DrawNote();
    void DrawHittedEffect(double deltaTime);
    void DrawKeepHitCount();
    void DrawJudgment(double deltaTime);
private:
    void DrawSingleNote(NOTES_LINKLIST* nowPtr);
    void DrawLongNote(NOTES_LINKLIST* nowPtr);
    void DrawLongBg(NOTES_LINKLIST* nowPtr, double _length, bool isReverse = false);
    double noteSize;
    double noteSpeed;
    double note_judge_area;
    double note_y_pos;
    double judgeFrameTime;
    double holdFrameTime;
    int holdFrameIdx;
    HittedEventQueue hittedQueue;
};

#endif // GAMEPLAYRENDER_HPP_INCLUDED
