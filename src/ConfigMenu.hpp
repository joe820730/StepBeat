#ifndef ConfigMenu_HPP_INCLUDED
#define ConfigMenu_HPP_INCLUDED

#include "Allegro_include.hpp"
#include "ConfigProvider.hpp"
#include "KeymapConfig.hpp"
#include "MenuInterface.hpp"

#define CONFIGMENUX     0.1
#define CONFIGMENUY     0.2

class ConfigMenu : public MenuInterface
{
public:
    ConfigMenu();
    ~ConfigMenu();
    bool Setup(ConfigProvider* cfg_provider);
    void CopyConfig();
    void GetFontHeight();
    void Draw(double deltaTime) override;
    void MenuCtrl(int mouseX, int mouseY) override;
    void MenuCtrl(int keyCode) override;
    void DrawShowTimeConfig();
    void SwitchShowTimeConfig();
    bool CheckShowTimeCfgVisible();
    void ConfigSet(int keyCode);
    void ShowTimeConfig(int keyCode);
private:
    enum
    {
        CFG_OFFSET,
        CFG_RESOLUTION,
        CFG_FPS,
        CFG_BGDIM,
        CFG_MUSICVOL,
        CFG_EFFECTVOL,
        CFG_KEYMAP,
        CFG_SAVE,
        CFG_TOTAL
    };
    bool CheckIsNeedRestart();
    int32_t fpsOptIdx;
    int32_t resolutionOptIdx;
    ConfigProvider* gameCfg;
    int32_t showtimeIdx;
    int32_t cpshowtimeConfig[SHOWTIME_TOTAL];
    bool isShowTimeCfgVisible;
    W_GAMECONFIG cpGameCfg; //Copied for check setting is been modified or not.
};

extern ConfigMenu* gCONFIGMENU;
#endif
