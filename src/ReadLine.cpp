#include <cstdio>
#include <cstdlib>
#include <string>
#include <iostream>
#include <fstream>
#include "Allegro_include.hpp"
/* Reference: https://stackoverflow.com/questions/6089231/getting-std-ifstream-to-handle-lf-cr-and-crlf */
std::istream& safeGetline(std::istream& _is, std::string& t)
{
    t.clear();

    // The characters in the stream are read one-by-one using a std::streambuf.
    // That is faster than reading them one-by-one using the std::istream.
    // Code that uses streambuf this way must be guarded by a sentry object.
    // The sentry object performs various tasks,
    // such as thread synchronization and updating the stream state.

    std::istream::sentry se(_is, true);
    std::streambuf* sb = _is.rdbuf();

    for(;;) {
        int c = sb->sbumpc();
        switch (c) {
        case '\n':
            return _is;
        case '\r':
            if(sb->sgetc() == '\n')
                sb->sbumpc();
            return _is;
        case EOF:
            // Also handle the case when the last line has no line ending
            if(t.empty())
                _is.setstate(std::ios::eofbit);
            return _is;
        default:
            if(c != ' ')
                t += (char)c;
        }
    }
}

void alGetLine(ALLEGRO_FILE* fp, std::string& strbuf)
{
    strbuf.clear();
    bool is_comment_out = false;
    bool ignore_space = true;
    for(;;)
    {
        int c = al_fgetc(fp);
        switch(c)
        {
            case '\r':
            {
                int ctmp = al_fgetc(fp);
                if(ctmp != '\n' && ctmp != EOF)
                    al_fseek(fp, -1, ALLEGRO_SEEK_CUR);
                return;
            }
            case '\n':
                return;
            case EOF:
                return;
            case ';':
                is_comment_out = true;
            case '"':
                ignore_space = false;
            default:
                if(!is_comment_out)
                {
                    if(((c == ' ' && !ignore_space) || (c != ' ')))
                        strbuf += (char)c;
                }
        }
    }
}

void KeyValSplit(const std::string& tmpLine, std::string& tmpKey, std::string& tmpStr)
{
    tmpKey.erase();
    size_t tmplinelen = tmpLine.length();
    size_t keyendpos = tmplinelen - 1;
    bool isFlag = true;  // Is #END or #EDITSTART?
    for(size_t i = 1; i < tmplinelen; i++)
    {
        if(tmpLine.at(i) == '=' || tmpLine.at(i) == ';')
        {
            keyendpos = i - 1;
            isFlag = false;
            break;
        }
    }
    tmpKey = tmpLine.substr(1, keyendpos);
    if(!isFlag)
    {
        size_t val_beginpos = keyendpos + 2;
        bool isquotefound = false;
        size_t endpos = tmplinelen - 1;
        for(size_t i = val_beginpos; i < tmplinelen; i++)
        {
            if(tmpLine.at(i) == '=')
            {
                val_beginpos++;
                continue;
            }
            else
            {
                if(tmpLine.at(i) == ';' || (tmpLine.at(i) == '"' && isquotefound))
                {
                    endpos = i - 1;
                    break;
                }
                if(tmpLine.at(i) == '"' && !isquotefound)
                {
                    isquotefound = true;
                    val_beginpos = i+1;
                }
            }
        }
        tmpStr = tmpLine.substr(val_beginpos, (endpos - val_beginpos + 1));
    }
}
