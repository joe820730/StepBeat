#ifndef KEYMAPCONFIG_HPP_INCLUDED
#define KEYMAPCONFIG_HPP_INCLUDED

#include "MenuInterface.hpp"
#include "ConfigProvider.hpp"

#define KEYCFGMENUX     0.1
#define KEYCFGMENUY     0.2

class KeymapConfig : public MenuInterface
{
public:
    KeymapConfig();
    ~KeymapConfig();
    bool Setup(ConfigProvider* cfg_provider);
    void GetFontHeight() override;
    void MenuCtrl(int mouseX, int mouseY) override{};
    void MenuCtrl(int keyCode) override;
    void CopyConfig();
    void Draw(double deltaTime) override;
private:
    enum
    {
        KEYMAP_UP,
        KEYMAP_DOWN,
        KEYMAP_LEFT,
        KEYMAP_RIGHT,
        KEYMAP_SHOWTIME,
        KEYMAP_SAVE,
        KEYMAP_TOTAL
    };
    typedef enum
    {
        KEYCFG_SELECT,
        KEYCFG_GRABBING
    }KEYMAPCFG_STATE;
    int keyGroupIdx;
    KEYMAPCFG_STATE keymapCfgState;
    W_KEYCFG cpwKeyCfg;
    ConfigProvider* gameCfg;
};

extern KeymapConfig* gKEYMAPCFGMENU;
#endif
