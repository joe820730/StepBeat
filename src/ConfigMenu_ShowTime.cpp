#include <iostream>

#include "Allegro_include.hpp"
#include "ConfigProvider.hpp"
#include "ConfigMenu.hpp"
#include "MenuInterface.hpp"
#include "GamePlayCtrl.hpp"
using namespace std;

void ConfigMenu::SwitchShowTimeConfig()
{
    int32_t* curshowtimecfg = gameCfg->GetShowtime();
    if(!gGAMEPLAYCTRL->enableShowTime)
    {
        isShowTimeCfgVisible = false;
        return;
    }
    /* Check value is valid or not. */
    bool isAllZero = true;
    for(int i = 0; i < SHOWTIME_TOTAL; i++)
    {
        if(cpshowtimeConfig[i] != 0)
        {
            isAllZero = false;
        }
    }
    if(isAllZero)
    {
        for(int i = 0; i < SHOWTIME_TOTAL; i++)
            cpshowtimeConfig[i] = 1;
    }
    /* Check configure update. */
    bool isNeedUpdate = false;
    for(uint8_t i = 0; i < SHOWTIME_TOTAL; i++)
    {
        if(cpshowtimeConfig[i] != curshowtimecfg[i])
        {
            isNeedUpdate = true;
            break;
        }
    }
    if(isNeedUpdate)
        gameCfg->SaveShowtime(cpshowtimeConfig);
    isShowTimeCfgVisible = !isShowTimeCfgVisible;
}

bool ConfigMenu::CheckShowTimeCfgVisible()
{
    return isShowTimeCfgVisible;
}

void ConfigMenu::ShowTimeConfig(int keyCode)
{
    switch(keyCode)
    {
        case ALLEGRO_KEY_LEFT:
        {
            IdxMinus(showtimeIdx, SHOWTIME_TOTAL);
            break;
        }
        case ALLEGRO_KEY_RIGHT:
        {
            IdxAdd(showtimeIdx, SHOWTIME_TOTAL);
            break;
        }
        case ALLEGRO_KEY_UP:
        {
            IdxAdd(cpshowtimeConfig[showtimeIdx], SHOWTIME_LEVELS);
            break;
        }
        case ALLEGRO_KEY_DOWN:
        {
            IdxMinus(cpshowtimeConfig[showtimeIdx], SHOWTIME_LEVELS);
            break;
        }
        case ALLEGRO_KEY_ESCAPE:
        case ALLEGRO_KEY_F3:
        case ALLEGRO_KEY_ENTER:
        {
            SwitchShowTimeConfig();
            break;
        }
        default:
        {
            break;
        }
    }
}

void ConfigMenu::DrawShowTimeConfig()
{
    if(!isShowTimeCfgVisible)
        return;
    wRes->bitmaps[WR_POPUP_BG].DrawScaled(screenWidth*0.32, screenHeight*0.33, screenWidth*0.36, 0);
    al_draw_text(wRes->gameFonts[TITLE_FONT], al_map_rgb(255,255,255), screenWidth*0.5, screenHeight*0.35, ALLEGRO_ALIGN_CENTER, "Showtime");
    for(int i = 0; i < SHOWTIME_TOTAL; i++)
    {
        double xoffset = screenWidth*0.375+screenWidth*0.05*i;
        if(showtimeIdx == i)
        {
            wRes->bmpGroups[WRG_SHOWTIMETEXT].Draw(xoffset, screenHeight*0.55, i);
            al_draw_textf(wRes->gameFonts[TITLE_FONT], al_map_rgb(100,255,255),xoffset+screenWidth*0.012, screenHeight*0.47, ALLEGRO_ALIGN_LEFT, "%d", cpshowtimeConfig[i]);
        }
        else
        {
            wRes->bmpGroups[WRG_SHOWTIMETEXT].DrawTinted(xoffset, screenHeight*0.55, al_map_rgb(150,150,150), i);
            al_draw_textf(wRes->gameFonts[TITLE_FONT], al_map_rgb(0,0,0),xoffset+screenWidth*0.012, screenHeight*0.47, ALLEGRO_ALIGN_LEFT, "%d", cpshowtimeConfig[i]);
        }
    }
}
