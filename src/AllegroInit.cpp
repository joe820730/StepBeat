#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "Allegro_include.hpp"
using namespace std;
bool AllegroInit(void)
{
    if(!al_init()) /* initialize Allegro */
    {
        printf("Allegro initialize failed!\n");
        return false;
    }
    else if(!al_install_keyboard())
    {
        printf("Keyboard install fail!\n");
        return false;
    }
    else if(!al_install_mouse())
    {
        printf("Mouse install fail!\n");
        return false;
    }
    else if(!al_init_native_dialog_addon())
    {
        printf("Native dialog initialize failed!\n");
        return false;
    }
    else if(!al_init_image_addon())
    {
        printf("Image addon init fail!\n");
        return false;
    }
    else if(!al_install_audio())  // install sound driver
    {
        printf("Audio install fail!\n");
        return false;
    }
    else if(!al_init_acodec_addon())
    {
        printf("ACodec addon init fail!\n");
        return false;
    }
    else if(!al_reserve_samples(24))
    {
        printf("Sound samples init fail!\n");
        return false;
    }
    else if(!al_init_font_addon())    // install font addons
    {
        printf("Font addon init fail!\n");
        return false;
    }
    else if(!al_init_ttf_addon())
    {
        printf("TTF addon init fail!\n");
        return false;
    }
    else if(!al_init_primitives_addon())
    {
        printf("Primitives addon init fail!\n");
        return false;
    }
    else if(!al_init_video_addon())
    {
        printf("Video addon init fail!\n");
        return false;
    }
    else
    {
        return true;
    }
}

void AllegroDestroy(void)
{
    al_uninstall_keyboard();
    al_uninstall_mouse();
    al_shutdown_font_addon();
    al_shutdown_image_addon();
    al_shutdown_native_dialog_addon();
    al_shutdown_primitives_addon();
    al_shutdown_ttf_addon();
    al_uninstall_audio();
    al_shutdown_video_addon();
    al_uninstall_system();
}
