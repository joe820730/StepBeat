#include "globalDef.hpp"
#include "NoteInfo.hpp"
#include "ConfigProvider.hpp"
#include "GameResources.hpp"
#include "GamePlayCtrl.hpp"
#include "StateHandler.hpp"
#include <functional>
#include <cmath>
#include <iostream>
using namespace std;

NOTE_ARROW KeyCodeToArrow(int keyCode, const W_KEYCFG* wKeyCfg)
{
    if(keyCode == wKeyCfg->wKeyUp_1 || keyCode == wKeyCfg->wKeyUp_2) return NA_UP;
    else if(keyCode == wKeyCfg->wKeyDown_1 || keyCode == wKeyCfg->wKeyDown_2) return NA_DOWN;
    else if(keyCode == wKeyCfg->wKeyLeft_1 || keyCode == wKeyCfg->wKeyLeft_2) return NA_LEFT;
    else if(keyCode == wKeyCfg->wKeyRight_1 || keyCode == wKeyCfg->wKeyRight_2) return NA_RIGHT;
    else return NA_TOTAL;
}

void GamePlayCtrl::GameStateCtrl(int keyCode)
{
    switch(keyCode)
    {
        case ALLEGRO_KEY_ESCAPE:
        {
            if(escapeTimerStamp == 0)
                escapeTimerStamp = timers.nowTime;
            else
            {
                if((timers.nowTime - escapeTimerStamp) < 500)
                {
                    gSTATEHANDLER->SetState(W_RESULT);
                    escapeTimerStamp = 0;
                }
                else
                    escapeTimerStamp = 0;
            }
            break;
        }
        case ALLEGRO_KEY_TAB:
        {
            if(timers.nowTime > 0)
                isPaused = !isPaused;
            break;
        }
//        case ALLEGRO_KEY_PAD_PLUS:
//        {
//            gGAMEPLAYCTRL->SpeedUp();
//            break;
//        }
//        case ALLEGRO_KEY_PAD_MINUS:
//        {
//            gGAMEPLAYCTRL->SpeedDown();
//            break;
//        }
        default:
        {
            if(!isAutoPlay && !isPaused)
            {
                if(keyCode == keyCfg.wKeyShow_1 ||
                   keyCode == keyCfg.wKeyShow_2 ||
                   keyCode == ALLEGRO_KEY_SPACE)
                {
                    if(enableShowTime)
                        ShowTimeStart(timers.nowTime);
                }
            }
            break;
        }
    }
}

void GamePlayCtrl::KeyDownEvent(int keyCode)
{
    NOTE_ARROW nArrow = KeyCodeToArrow(keyCode, &keyCfg);
    if(nArrow == NA_TOTAL)
        return;
    if(isAutoPlay || isPaused)
        return;
    bool isPlayHitSound = true;
    bool isFound = false;
    NOTES_BARLIST* nowbarPtr = notes_bar;
    if(judgeNote_keep != nullptr)
    {
        isFound = true;
        hitCount++;
        hittedQueue.AttachEvent(judgeNote_keep->noteArrow);
    }
    while(nowbarPtr != nullptr && !isFound)
    {
        if(nowbarPtr->barTime_end < timers.nowTime)
        {
            nowbarPtr = nowbarPtr->nextBar;
            continue;
        }
        NOTES_LINKLIST* nowPtr = nowbarPtr->notes;
        while(nowPtr != nullptr)
        {
            if(nowPtr->active && nowPtr->isInJudgeArea && nowPtr->noteArrow == nArrow)
            {
                if(nowPtr->noteType == NTYPE_KHIT)
                {
                    if(!isHitting)
                        isHitting = true;
                    hitCount++;
                }
                else if(nowPtr->noteType == NTYPE_HOLD)
                {
                    if(!isPressing[nArrow] && !nowPtr->hitted)
                    {
                        isPressing[nArrow] = true;
                        if(!nowPtr->isSingleInLong)
                        {
                            al_set_sample_instance_playing(pressSound, true);
                            isPlayHitSound = false;
                        }
                    }
                }
                double _judgeTime = fabs(timers.nowTime - (nowPtr->noteTime));
                if(!nowPtr->hitted)
                {
                    if(nowPtr->noteType != NTYPE_KHIT)
                        NoteJudge(_judgeTime, nowPtr);
                    hittedQueue.AttachEvent(nowPtr->noteArrow);
                    isFound = true;
                    break;
                }
            }
            if(nowPtr->noteTime - timers.nowTime > JUDGE_BAD)
                break;
            nowPtr = nowPtr->nextNote;
        }
        if(nowbarPtr->barTime > timers.nowTime)
            break;
        nowbarPtr = nowbarPtr->nextBar;
    }
    if(isPlayHitSound)
        al_play_sample(wRes->wSounds[WS_HIT], (double)gameCfg.effectVol/100.0, 0, 1, ALLEGRO_PLAYMODE_ONCE, nullptr);
}

void GamePlayCtrl::KeyUpEvent(int keyCode)
{
    NOTE_ARROW nArrow = KeyCodeToArrow(keyCode, &keyCfg);
    if(nArrow == NA_TOTAL)
        return;
    if(isAutoPlay || isPaused)
        return;
    if(judgeNote_long[nArrow] != nullptr && isPressing[nArrow])
    {
        double _judgeTime = fabs(timers.nowTime - (judgeNote_long[nArrow]->noteTime_end));
        if(!judgeNote_long[nArrow]->active)
            return;
        if(judgeNote_long[nArrow]->noteArrow == nArrow && judgeNote_long[nArrow]->hitted)
        {
            NoteJudge(_judgeTime, judgeNote_long[nArrow]);
            al_stop_sample_instance(pressSound);
            hittedQueue.AttachEvent(judgeNote_long[nArrow]->noteArrow);
            if(judgeNote_long[nArrow]->isSingleInLong)
                al_play_sample(wRes->wSounds[WS_HIT], (double)gameCfg.effectVol/100.0, 0, 1, ALLEGRO_PLAYMODE_ONCE, nullptr);
            isPressing[nArrow] = false;
            judgeNote_long[nArrow]->active = false;
        }
    }
}

void GamePlayCtrl::SpeedUp()
{
    if(noteSpeed <= 2.0)
        noteSpeed += 0.1;
}

void GamePlayCtrl::SpeedDown()
{
    if(noteSpeed > 0.6)
        noteSpeed -= 0.1;
}
