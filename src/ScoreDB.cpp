#include "ScoreDB.hpp"
#include <cstring>

#define DB_VERSION "v001"
#define DB_VERSION_LEN 4
#define DB_HEAD "\x01\x64\x62\x02"
#define DB_HEAD_LEN 4

// db format:
// DB_HEAD | md5sum   | last_modified_date | [ playername  | hiscore | accuracy ]*5 | [ playername_bonus  | hiscore_bonus | accuracy_bonus ]*5
// 4 bytes | 16 bytes | 8 bytes            | [ string      | 4 bytes | 8 bytes  ]*5 | [ string            | 4 bytes       | 8 bytes        ]*5
// u32     | u8 array | s64                | [ max 32bytes | u32     | double   ]*5 | [ max 32bytes       | u32           | double         ]*5

bool write_score_db(const std::string& path, const W_SCOREDB target_db[MAP_TOTAL])
{
    bool write_stat = true;
    FILE* fp = fopen(path.c_str(), "wb");
    if(fp != nullptr)
    {
        fwrite(DB_VERSION, sizeof(uint8_t), DB_VERSION_LEN, fp);
        for(int i = 0; i < MAP_TOTAL; i++)
        {
            fwrite(DB_HEAD, sizeof(uint8_t), DB_HEAD_LEN, fp);
            fwrite((void*)&target_db[i].diff, sizeof(uint8_t), 1, fp);
            fwrite((void*)target_db[i].md5sum, sizeof(uint8_t), 16, fp);
            fwrite((void*)&target_db[i].last_modified_time, sizeof(int64_t), 1, fp);
            for(int j = 0; j < HISCORE_MAX; j++)
            {
                uint32_t playername_len = target_db[i].playername[j].length();
                if(playername_len > PLAYERNAME_MAXLEN)
                    playername_len = PLAYERNAME_MAXLEN;
                fwrite((void*)&playername_len, sizeof(uint8_t), 1, fp);
                fwrite((void*)target_db[i].playername[j].c_str(), sizeof(uint8_t), playername_len, fp);
                fwrite((void*)&target_db[i].hiscore[j], sizeof(int32_t), 1, fp);
                fwrite((void*)&target_db[i].accuracy[j], sizeof(double), 1, fp);

                playername_len = target_db[i].playername_bonus[j].length();
                if(playername_len > PLAYERNAME_MAXLEN)
                    playername_len = PLAYERNAME_MAXLEN;
                fwrite((void*)&playername_len, sizeof(uint8_t), 1, fp);
                fwrite((void*)target_db[i].playername_bonus[j].c_str(), sizeof(uint8_t), playername_len, fp);
                fwrite((void*)&target_db[i].hiscore_bonus[j], sizeof(int32_t), 1, fp);
                fwrite((void*)&target_db[i].accuracy_bonus[j], sizeof(double), 1, fp);
            }
        }
        fclose(fp);
    }
    else
    {
        write_stat = false;
    }
    return write_stat;
}

bool read_score_db(const std::string& path, W_SCOREDB target_db[MAP_TOTAL])
{
    bool read_stat = true;
    FILE* fp = fopen(path.c_str(), "rb");
    uint8_t buf[16] = {0};
    uint8_t db_version[DB_VERSION_LEN];
    char playername_buf[PLAYERNAME_MAXLEN+1] = {0};
    if(fp != nullptr)
    {
        fread((void*)db_version, sizeof(uint8_t), DB_VERSION_LEN, fp);
        for(int i = 0; i < MAP_TOTAL; i++)
        {
            fread(buf, sizeof(uint8_t), DB_HEAD_LEN, fp);
            if(strncmp((char*)buf, DB_HEAD, DB_HEAD_LEN) == 0)
            {
                fread((void*)&target_db[i].diff, sizeof(uint8_t), 1, fp);
                fread((void*)target_db[i].md5sum, sizeof(uint8_t), 16, fp);
                fread((void*)&target_db[i].last_modified_time, sizeof(int64_t), 1, fp);
                for(int j = 0; j < HISCORE_MAX; j++)
                {
                    uint32_t playername_len = 0;
                    fread((void*)&playername_len, sizeof(uint8_t), 1, fp);
                    if(playername_len > PLAYERNAME_MAXLEN)
                    {
                        fprintf(stderr, "File corrupted!\n");
                        read_stat = false;
                        break;
                    }
                    fread((void*)playername_buf, sizeof(char), playername_len, fp);
                    target_db[i].playername[j] = playername_buf;
                    memset(playername_buf, 0, (PLAYERNAME_MAXLEN+1)*sizeof(char));
                    fread((void*)&target_db[i].hiscore[j], sizeof(int32_t), 1, fp);
                    fread((void*)&target_db[i].accuracy[j], sizeof(double), 1, fp);

                    fread((void*)&playername_len, sizeof(uint8_t), 1, fp);
                    if(playername_len > PLAYERNAME_MAXLEN)
                    {
                        fprintf(stderr, "File corrupted!\n");
                        read_stat = false;
                        break;
                    }
                    fread((void*)playername_buf, sizeof(char), playername_len, fp);
                    target_db[i].playername_bonus[j] = playername_buf;
                    memset(playername_buf, 0, (PLAYERNAME_MAXLEN+1)*sizeof(char));
                    fread((void*)&target_db[i].hiscore_bonus[j], sizeof(int32_t), 1, fp);
                    fread((void*)&target_db[i].accuracy_bonus[j], sizeof(double), 1, fp);
                }
            }
            else
            {
                fprintf(stderr, "File corrupted!\n");
                read_stat = false;
                break;
            }
        }
        fclose(fp);
    }
    else
    {
        read_stat = false;
    }
    return read_stat;
}
