#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <thread>
#include <mutex>
#include <chrono>
#include <functional>

#include "globalDef.hpp"
#include "GamePlayCtrl.hpp"
#include "GameResources.hpp"
#include "ConfigProvider.hpp"
#include "ResultScreen.hpp"
#include "MainMenu.hpp"
#include "ConfigMenu.hpp"
#include "KeymapConfig.hpp"
#include "GameEvents.hpp"
#include "StateHandler.hpp"
#include "IWidget.hpp"
#include "debug.hpp"

#define READY_LOGO_TIME (EMPTY_TIME+1000.0)
#define GO_LOGO_TIME (READY_LOGO_TIME+1000.0)

using namespace std;

static void GenFrame(W_STATE wState, const W_GAMECONFIG& wGameCfg, double deltaTime);
void KeyEvent(W_STATE wState, ALLEGRO_EVENT keyEvent);
void GameLoop(ConfigProvider* cfg_provider)
{
    ALLEGRO_EVENT events;
    ALLEGRO_EVENT_QUEUE* frame_queue = al_create_event_queue();
    if(frame_queue == nullptr)
    {
        printf("Can't create event queue at: %s\n", __FILE__);
        return;
    }
    al_register_event_source(frame_queue, al_get_display_event_source(g_display));
    al_register_event_source(frame_queue, gGAMEEVENTS->GetEventSource(W_EVENT_ID_SCANSONG_END));
    al_register_event_source(frame_queue, gGAMEEVENTS->GetEventSource(W_EVENT_ID_LOADMAP_END));
    al_register_event_source(frame_queue, gGAMEEVENTS->GetEventSource(W_EVENT_ID_RESET_FPS));
    al_register_event_source(frame_queue, al_get_keyboard_event_source());

    W_GAMECONFIG wGameCfg = cfg_provider->GetConfig();
    double deltaTimeStart = 0;
    double tempTime = al_get_time()*1000.0;
    double deltaTime = tempTime - deltaTimeStart;
    deltaTimeStart = tempTime;

    ALLEGRO_TIMER* FPStimer = nullptr;
    bool isRefreshFrameByTimer = (wGameCfg.vsyncOption == VSYNC_OFF);
    bool isUnlimitFPSinplaying = (wGameCfg.fps < 60 || wGameCfg.fps > 240);
    bool isFrameNeedRefresh = false;
    if(isRefreshFrameByTimer)
    {
        int fps = (isUnlimitFPSinplaying)?60:wGameCfg.fps;
        FPStimer = al_create_timer(1.0 / fps);
        if(FPStimer == nullptr)
        {
            printf("Can't create FPSTimer! Line: %d\n", __LINE__);
            return;
        }
        al_register_event_source(frame_queue, al_get_timer_event_source(FPStimer));
        al_start_timer(FPStimer);
    }
    do
    {
        bool ev_queue_not_empty = false;
        W_STATE wState = gSTATEHANDLER->GetState();
        if(!g_isRun)
            break;
        if((!isRefreshFrameByTimer) || ((wState == W_PLAYING) && isUnlimitFPSinplaying))
        {
            ev_queue_not_empty = al_get_next_event(frame_queue,&events);
        }
        else
        {
            al_wait_for_event(frame_queue,&events);
            ev_queue_not_empty = true;
        }
        if(ev_queue_not_empty)
        {
            switch(events.type)
            {
                case W_EVENT_ID_SCANSONG_END:
                {
                    gMUSICLIST->CloneBitmap();
                    break;
                }
                case W_EVENT_ID_LOADMAP_END:
                {
                    gSTATEHANDLER->SetState(W_PLAYING);
                    break;
                }
                case W_EVENT_ID_RESET_FPS:
                {
                    W_GAMECONFIG tmp = cfg_provider->GetConfig();
                    if(tmp.vsyncOption == VSYNC_OFF)
                    {
                        isUnlimitFPSinplaying = (tmp.fps < 60 || tmp.fps > 240);
                        al_set_timer_speed(FPStimer, 1.0 / ((isUnlimitFPSinplaying)?60:tmp.fps));
                    }
                    wGameCfg = tmp;
                    break;
                }
                case ALLEGRO_EVENT_DISPLAY_CLOSE:
                {
                    g_isRun = false;
                    gGAMEEVENTS->EmitEvent(W_EVENT_ID_EXIT);
                    break;
                }
                case ALLEGRO_EVENT_TIMER:
                {
                    isFrameNeedRefresh = true;
                    break;
                }
                case ALLEGRO_EVENT_KEY_CHAR:
                case ALLEGRO_EVENT_KEY_DOWN:
                case ALLEGRO_EVENT_KEY_UP:
                {
                    KeyEvent(wState, events);
                    break;
                }
                default:
                {
                    break;
                }
            }
            LOG_ALL("queue is %s\n", (al_is_event_queue_empty(frame_queue)?"empty":"not empty"));
            ev_queue_not_empty = false;
        }
        if(!isRefreshFrameByTimer || isFrameNeedRefresh || ((wState == W_PLAYING) && isUnlimitFPSinplaying))
        {
            tempTime = al_get_time() * 1000.0;
            deltaTime = tempTime - deltaTimeStart;
            deltaTimeStart = tempTime;
            isFrameNeedRefresh = false;
            gGAMEPLAYCTRL->MapHandle(deltaTime);
            GenFrame(wState, wGameCfg, deltaTime);
            al_flip_display(); /* Wait for the beginning of a vertical retrace. */
            al_clear_to_color(al_map_rgb(0,0,0)); /* Clear the complete target bitmap, but confined by the clipping rectangle. */
        }
    }
    while(!al_is_event_queue_empty(frame_queue) || g_isRun);
    if(isRefreshFrameByTimer && (FPStimer != nullptr))
    {
        al_destroy_timer(FPStimer);
    }
    al_destroy_event_queue(frame_queue);
    printf("All cleanup.\n");
}

static void GenFrame(W_STATE wState, const W_GAMECONFIG& wGameCfg, double deltaTime)
{
    double tempTime = al_get_time()*1000.0;
    double frameCostStart = tempTime;
    if(wState != W_PLAYING)
    {
        wRes->bitmaps[WR_BG].DrawScaled(0,0,wGameCfg.screenWidth,wGameCfg.screenHeight);
        al_draw_filled_rectangle(0, 0, wGameCfg.screenWidth, wGameCfg.screenHeight, al_map_rgba_f(0, 0, 0, wGameCfg.bgDim/100.0));
    }
    switch(wState)
    {
        case W_PLAYING:
        {
            gGAMEPLAYCTRL->Draw(deltaTime);
            break;
        }
        case W_LOADMAP:
        {
            wRes->bitmaps[WR_SCORE_BAR].DrawScaled(0,0, wGameCfg.screenWidth, 0);
            wRes->bitmaps[WR_INFO_BAR].DrawScaled(0,wGameCfg.screenHeight-wRes->bitmaps[WR_INFO_BAR].GetScaledHeight(),wGameCfg.screenWidth, 0);
            al_draw_text(wRes->gameFonts[TITLE_FONT], al_map_rgb(255,255,255),
                         wGameCfg.screenWidth*0.5, wGameCfg.screenHeight*0.5,
                         ALLEGRO_ALIGN_CENTER, "Loading...");
            break;
        }
        case W_MENU:
        {
            gMAINMENU->Draw(deltaTime);
            break;
        }
        case W_SETTING:
        {
            gCONFIGMENU->Draw(deltaTime);
            break;
        }
        case W_SELECT_MUSIC:
        {
            gMUSICLIST->Draw(deltaTime);
            gCONFIGMENU->DrawShowTimeConfig();
            break;
        }
        case W_KEYMAPCFG:
        {
            gKEYMAPCFGMENU->Draw(deltaTime);
            break;
        }
        case W_RESULT:
        {
            gRESULTSCREEN->Draw(deltaTime);
            gCONFIGMENU->DrawShowTimeConfig();
            break;
        }
        default:
        {
            break;
        }
    }

    if(wState == W_SELECT_MUSIC || wState == W_RESULT || wState == W_PLAYING || wState == W_LOADMAP)
    {
        wRes->bmpGroups[WRG_AUTOOPT].Draw(wGameCfg.screenWidth*AUTOBTNX, AUTOBTNY, gGAMEPLAYCTRL->isAutoPlay);
        wRes->bmpGroups[WRG_SHOWOPT].Draw(wGameCfg.screenWidth*SHOWTIMEBTNX, SHOWTIMEBTNY, gGAMEPLAYCTRL->enableShowTime);
    }
    double frameCost = al_get_time()*1000.0 - frameCostStart;
    al_draw_textf( wRes->gameFonts[DEBUG_FONT], al_map_rgb(255, 255, 255),
                  wGameCfg.screenWidth-5 ,wGameCfg.screenHeight-al_get_font_line_height(wRes->gameFonts[DEBUG_FONT])*2, ALLEGRO_ALIGN_RIGHT,
                  "%.3lf", frameCost);
    al_draw_textf( wRes->gameFonts[DEBUG_FONT], al_map_rgb(255, 255, 255),
                  wGameCfg.screenWidth-5 ,wGameCfg.screenHeight-al_get_font_line_height(wRes->gameFonts[DEBUG_FONT]), ALLEGRO_ALIGN_RIGHT,
                  "%.3lf", deltaTime);
}

void KeyEvent(W_STATE wState, ALLEGRO_EVENT keyEvent)
{
    switch(keyEvent.type)
    {
        case ALLEGRO_EVENT_KEY_CHAR:
        {
            if(wState == W_RESULT)
                gRESULTSCREEN->InputPlayerName(keyEvent.keyboard.unichar);
            break;
        }
        case ALLEGRO_EVENT_KEY_DOWN:
        {
            if(keyEvent.keyboard.keycode == 157 && wState != W_PLAYING)
                al_show_native_message_box(nullptr, "WARNING", "Please switch IME to EN mode.", "Non-EN IME will affect your game experience.", "OK", 0);
            switch(wState)
            {
                case W_MENU:
                {
                    gMAINMENU->MenuCtrl(keyEvent.keyboard.keycode);
                    break;
                }
                case W_SELECT_MUSIC:
                {
                    if(!gCONFIGMENU->CheckShowTimeCfgVisible())
                        gMUSICLIST->MenuCtrl(keyEvent.keyboard.keycode);
                    else
                        gCONFIGMENU->ShowTimeConfig(keyEvent.keyboard.keycode);
                    break;
                }
                case W_SETTING:
                {
                    gCONFIGMENU->MenuCtrl(keyEvent.keyboard.keycode);
                    break;
                }
                case W_KEYMAPCFG:
                {
                    gKEYMAPCFGMENU->MenuCtrl(keyEvent.keyboard.keycode);
                    if(keyEvent.keyboard.keycode == ALLEGRO_KEY_ESCAPE)
                        wState = W_SETTING;
                    break;
                }
                case W_PLAYING:
                {
                    gGAMEPLAYCTRL->KeyDownEvent(keyEvent.keyboard.keycode);
                    gGAMEPLAYCTRL->GameStateCtrl(keyEvent.keyboard.keycode);
                    break;
                }
                case W_RESULT:
                {
                    if(!gCONFIGMENU->CheckShowTimeCfgVisible())
                        gRESULTSCREEN->ScreenCtrl(keyEvent.keyboard.keycode);
                    else
                        gCONFIGMENU->ShowTimeConfig(keyEvent.keyboard.keycode);
                    break;
                }
                default:
                {
                    break;
                }
            }
            break;
        }
        case ALLEGRO_EVENT_KEY_UP:
        {
            switch(wState)
            {
                case W_PLAYING:
                {
                    gGAMEPLAYCTRL->KeyUpEvent(keyEvent.keyboard.keycode);
                    break;
                }
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}
