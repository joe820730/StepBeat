#ifndef _DEBUG_H_
#define _DEBUG_H_
#ifdef DEBUG
#include <cstdio>
#define LOG(msg, ...)                           \
	fprintf(stderr, "%s:%s() at line: %d: ",__FILE__ , __func__, __LINE__);    \
	fprintf(stderr, msg, ##__VA_ARGS__);		\
	fprintf(stderr, "\n");
#else
#define	LOG(msg, ...)
#endif

#ifdef DEBUG_ALL
#include <cstdio>
#define LOG_ALL(msg, ...)                           \
	fprintf(stderr, "%s:%s() at line: %d: ",__FILE__ , __func__, __LINE__);    \
	fprintf(stderr, msg, ##__VA_ARGS__);		\
	fprintf(stderr, "\n");
#else
#define	LOG_ALL(msg, ...)
#endif

#endif

