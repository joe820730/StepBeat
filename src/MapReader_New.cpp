#include <iostream>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#define DEBUG__
#include "MapReader.hpp"
#include "GamePlayCtrl.hpp"
#include "debug.hpp"

using namespace std;

#define VALIDCHARNEWTOTAL 11
enum
{
    MAPLINE_HIT,
    MAPLINE_HOLD,
    MAPLINE_KHIT,
    MAPLINE_TOTAL
};

bool isLineGroupEnd = false;
const char validChar_New[VALIDCHARNEWTOTAL] = "012345WSAD";

extern void ErrorMsg(char msg[], int atline);

bool FixedArrow_New(NOTE_ARROW& nArrow, char c)
{
    switch(c)
    {
        case 'W':
        {
            nArrow = NA_UP;
            return true;
        }
        case 'S':
        {
            nArrow = NA_DOWN;
            return true;
        }
        case 'A':
        {
            nArrow = NA_LEFT;
            return true;
        }
        case 'D':
        {
            nArrow = NA_RIGHT;
            return true;
        }
        case '1':
        case '2':
        case '4':
        default:
        {
            nArrow = GenArrow();
            return false;
        }
    }
}

bool MapReader::MapLineParser_New(NOTES_BARLIST*& bars, W_SONGLIST *songInfo, W_MAPDIFF targetDiff, std::string& tmpLine)
{
    std::string tmpStr; // For analyzing file.
    std::string tmpKey; // For analyzing value.
    tmpStr.reserve(32);
    tmpKey.reserve(16);
    bool isReadNote = false;
    switch(tmpLine.at(0))
    {
        case '<': // Difficult start
        {
            isReadNote = false;
            LOG("Check map block.\n");
            size_t endpos = 0;
            for(size_t i = 1, tmplen = tmpLine.length(); i < tmplen; i++)
            {
                if(tmpLine.at(i) == '>')
                {
                    endpos = i - 1;
                    break;
                }
            }
            if(endpos == 0)
            {
                ErrorMsg(errormsg, nowLine);
                al_show_native_message_box(nullptr, "ERROR!", errormsg, "Missing '>' character, file format error! Please fix it.", "OK", ALLEGRO_MESSAGEBOX_ERROR);
                return false;
            }
            if(tmpLine.substr(1, endpos) != DiffNameStr[targetDiff])
                isDiffMatch = false;
            else
                isDiffMatch = true;
            break;
        }
        case '#': //BPM or NBM change
        {
            if(isDiffMatch)
            {
                isReadNote = false;
                KeyValSplit(tmpLine, tmpKey, tmpStr);
                if(tmpKey == "BPM")
                {
                    std::string _tmpBPM;
                    _tmpBPM.reserve(8);
                    for(size_t i = 0, tmpstrlen = tmpStr.length(); i < tmpstrlen; i++)
                    {
                        if(tmpStr.at(i) >= '0' && tmpStr.at(i) <= '9')
                            _tmpBPM += tmpStr.at(i);
                        else if(tmpStr.at(i) == '.')
                            _tmpBPM += tmpStr.at(i);
                    }
                    nowBPM = stof(_tmpBPM);
                    LOG("BPM = %lf\n", nowBPM);
                    _tmpBPM.erase();
                }
                else if(tmpKey == "MEASURE")
                {
                    std::string _tmpMeasure;
                    _tmpMeasure.reserve(8);
                    for(size_t i = 0, tmpstrlen = tmpStr.length(); i < tmpstrlen; i++)
                    {
                        switch(tmpStr.at(i))
                        {
                            case '/':
                            {
                                nowBPB = stoi(_tmpMeasure);
                                _tmpMeasure.erase();
                                break;
                            }
                            default:
                            {
                                if(tmpStr.at(i) >= '0' && tmpStr.at(i) <= '9')
                                    _tmpMeasure += tmpStr.at(i);
                                break;
                            }
                        }
                    }
                    if(_tmpMeasure.length() != 0)
                        nowBeatLen = stoi(_tmpMeasure);
                    LOG("MEASURE = %d/%d\n", nowBPB, nowBeatLen);
                }
                else if(tmpKey == "SPEED")
                {
                    std::string _tmpSpeed;
                    _tmpSpeed.reserve(8);
                    for(size_t i = 0, tmpstrlen = tmpStr.length(); i < tmpstrlen; i++)
                    {
                        if(tmpStr.at(i) >= '0' && tmpStr.at(i) <= '9')
                            _tmpSpeed += tmpStr.at(i);
                        else if(tmpStr.at(i) == '.' || tmpStr.at(i) == '-')
                            _tmpSpeed += tmpStr.at(i);
                    }
                    nowSpeed = stof(_tmpSpeed);
                    LOG("BPM = %lf\n", nowSpeed);
                    _tmpSpeed.erase();
                }
                else if(tmpKey == "END")
                {
                    isLineGroupEnd = true;
                    isReadNote = true;
                    isLineGroupEnd = true;
                    isMapEnd = true;
                }
                else if(tmpKey == "EDITSTART")
                {
                    songInfo->beginAt = noteTime - 1000.0;
                    if(songInfo->beginAt < 0)
                        songInfo->beginAt = 0;
                    songInfo->isEditing = true;
                    NOTES_BARLIST** tempPtrPtr = &bars;
                    while(tempPtrPtr != nowbarPtrPtr)
                    {
                        (*tempPtrPtr)->active = false;
                        (*tempPtrPtr)->visible = false;
                        tempPtrPtr = &((*tempPtrPtr)->nextBar);
                    }
                }
                else if((tmpKey == "AUTHOR") || (tmpKey == "LEVEL"))
                {
                    isReadNote = false;
                }
                else
                {
                    ErrorMsg(errormsg, nowLine);
                    al_show_native_message_box(nullptr, "ERROR!", errormsg, "Unknown command! Please check map file.", "OK", ALLEGRO_MESSAGEBOX_ERROR);
                    return false;
                }
            }
            break;
        }
        case ';':
        {
            isReadNote = false;
            break;
        }
        case '-':
        {
            if(isDiffMatch)
            {
                isReadNote = true;
                isLineGroupEnd = true;
            }
            break;
        }
        default:
        {
            if(isDiffMatch)
            {
                isReadNote = true;
                isLineGroupEnd = false;
                isMapEmpty = false;
                break;
            }
            break;
        }
    }
    return isReadNote;
}

bool MapReader::ReadMap_New(NOTES_BARLIST*& bars, W_SONGLIST *songInfo, W_MAPDIFF targetDiff, uint32_t& totalNote, NOTES_BARLIST*& last_bar)
{
    NOTES_LINKLIST *prevPtr = nullptr;
    NOTES_LINKLIST *prevPtr2 = nullptr;
    songInfo->beginAt = 0;
    songInfo->isEditing = false;
    nowBPM = songInfo->songBPM;
    noteTime = songInfo->mapOffset;
    Refresh();
    nowbarPtrPtr = &bars;
    NOTES_BARLIST *prevbarPtr = nullptr;

    ALLEGRO_FILE* mapFile;
    NOTE_ARROW prevLongArrow = NA_TOTAL;
    mapFile = al_fopen(songInfo->mapPath[targetDiff].c_str(), "rb");
    if(!mapFile)
    {
        return false;
    }
    else
    {
        int linecounter = 0;
        bool isReadNote = false;
        std::string tmpLine; // For analyzing file.
        std::string tmpBar[MAPLINE_TOTAL]; //For analysis map notes.
        NOTES_LINKLIST *tmpKhit = nullptr;
        tmpLine.reserve(64);
        for(size_t i = 0; i < MAPLINE_TOTAL; i++)
            tmpBar[i].reserve(64);
        while(!al_feof(mapFile) && !isMapEnd)
        {
            alGetLine(mapFile, tmpLine);
            nowLine++;
            if(!tmpLine.empty())
            {
                isReadNote = MapLineParser_New(bars, songInfo, targetDiff, tmpLine);
                if(isReadNote)
                {
                    if(!isLineGroupEnd)
                    {
                        if(linecounter >= MAPLINE_TOTAL)
                        {
                            ErrorMsg(errormsg, nowLine);
                            al_show_native_message_box(nullptr, "Map file error!", errormsg,
                                                       "Track count > 3, please check map file.", "OK", ALLEGRO_MESSAGEBOX_ERROR);
                            return false;
                        }
                        tmpBar[linecounter] = tmpLine;
                        linecounter++;
                    }
                    else
                    {
                        if(linecounter > 0 && linecounter < MAPLINE_TOTAL)
                        {
                            ErrorMsg(errormsg, nowLine);
                            al_show_native_message_box(nullptr, "Map file error!", errormsg,
                                                       "Track count < 3, please check map file.", "OK", ALLEGRO_MESSAGEBOX_ERROR);
                            return false;
                        }
                        linecounter = 0;
                        nowBarLen = 0;
                        for(int i = 0; i < MAPLINE_TOTAL; i++)
                        {
                            if(tmpBar[i].length() > nowBarLen)
                            {
                                nowBarLen = tmpBar[i].length();
                            }
                        }
                        for(int i = 0; i < MAPLINE_TOTAL; i++)
                        {
                            if(tmpBar[i].length() != nowBarLen)
                            {
                                ErrorMsg(errormsg, nowLine);
                                al_show_native_message_box(nullptr, "Map file error!", errormsg,
                                                           "Track length mismatch! Please check map file.", "OK", ALLEGRO_MESSAGEBOX_ERROR);
                                return false;
                            }
                        }
                        nowInterval = (double)nowBarLen / (double)nowBPB / (BEATLENUSUAL / (double)nowBeatLen);
                        if(prevbarPtr != nullptr)
                        {
                            prevbarPtr->barPosition_end = CalcNotePosition(1, nowSpeed, EMPTY_TIME, noteTime, nowBPM, 0);
                            prevbarPtr->barTime_end = noteTime;
                            prevbarPtr->last_note = prevPtr2;
                        }
                        InsertBar(nowbarPtrPtr, noteTime, nowBPM, nowSpeed);
                        (*nowbarPtrPtr)->prevBar = prevbarPtr;
                        prevbarPtr = (*nowbarPtrPtr);
                        prevPtr2 = nullptr;
                        nowPtrPtr = &((*nowbarPtrPtr)->notes);
                        for(size_t i = 0; i < nowBarLen; i++)
                        {
                            if(tmpBar[MAPLINE_KHIT].at(i) != '0' &&
                               (tmpBar[MAPLINE_HIT].at(i) != '0' || tmpBar[MAPLINE_HOLD].at(i) != '0'))
                            {
                                ErrorMsg(errormsg, nowLine);
                                al_show_native_message_box(nullptr, "Map file error!", errormsg,
                                                           "Keep hit track can't overlapped by other two track.", "OK", ALLEGRO_MESSAGEBOX_ERROR);
                                return false;
                            }
                            /* Keep hit note */
                            if((tmpBar[MAPLINE_KHIT].at(i) == '4' ||
                                tmpBar[MAPLINE_KHIT].at(i) == 'W' ||
                                tmpBar[MAPLINE_KHIT].at(i) == 'A' ||
                                tmpBar[MAPLINE_KHIT].at(i) == 'S' ||
                                tmpBar[MAPLINE_KHIT].at(i) == 'D') &&
                               tmpKhit == nullptr)
                            {
                                isFixedArrow = FixedArrow_New(nArrow, tmpBar[MAPLINE_KHIT].at(i));
                                if(!isFixedArrow)
                                    nArrow = GenArrow();
                                InsertNote(nowPtrPtr, noteTime, noteTime, NTYPE_KHIT, nArrow, nowBPM, nowSpeed, isFixedArrow, noteId, 0, 0);
                                tmpKhit = (*nowPtrPtr);
                                noteId++;
                                (*nowPtrPtr)->prevNote = prevPtr2;
                                prevPtr = (*nowPtrPtr);
                                prevPtr2 = (*nowPtrPtr);
                                nowPtrPtr = &((*nowPtrPtr)->nextNote);
                            }
                            if(tmpBar[MAPLINE_KHIT].at(i) == '5' && tmpKhit != nullptr)
                            {
                                tmpKhit->noteTime_end = noteTime;
                                tmpKhit->notePosition_end = CalcNotePosition(1, tmpKhit->noteSpeed, EMPTY_TIME, tmpKhit->noteTime_end, tmpKhit->noteBpm, 0);

                                int khitDrawCount = 0, hitCountTarget = 0;
                                khitDrawCount = (tmpKhit->noteTime_end - tmpKhit->noteTime)/(MIN_TO_SEC/(nowBPM*4.0)*1000);
                                if(targetDiff == MAP_BASIC)
                                    hitCountTarget = (khitDrawCount+1)/4.0;
                                else if(targetDiff == MAP_NORMAL)
                                    hitCountTarget = (khitDrawCount+1)/2.0;
                                else
                                    hitCountTarget = khitDrawCount+1;
                                if(hitCountTarget == 0)
                                    hitCountTarget = 1;
                                tmpKhit->khitDrawCount = khitDrawCount;
                                tmpKhit->hitCountTarget = hitCountTarget;
                                tmpKhit = nullptr;
                            }
                            /* Long press note */
                            if((tmpBar[MAPLINE_HOLD].at(i) == '2' ||
                                tmpBar[MAPLINE_HOLD].at(i) == 'W' ||
                                tmpBar[MAPLINE_HOLD].at(i) == 'A' ||
                                tmpBar[MAPLINE_HOLD].at(i) == 'S' ||
                                tmpBar[MAPLINE_HOLD].at(i) == 'D') &&
                               tmpLong == nullptr)
                            {
                                isFixedArrow = FixedArrow_New(nArrow, tmpBar[MAPLINE_HOLD].at(i));
                                if(!isFixedArrow)
                                {
                                    while(nArrow == prevLongArrow)
                                        nArrow = GenArrow();
                                }
                                InsertNote(nowPtrPtr, noteTime, noteTime, NTYPE_HOLD, nArrow, nowBPM, nowSpeed, isFixedArrow, noteId, 0, 0);
                                tmpLong = (*nowPtrPtr);
                                noteId+=2;
                                (*nowPtrPtr)->prevNote = prevPtr2;
                                prevPtr = (*nowPtrPtr);
                                prevPtr2 = (*nowPtrPtr);
                                nowPtrPtr = &((*nowPtrPtr)->nextNote);
                            }
                            if(tmpBar[MAPLINE_HOLD].at(i) == '3' && tmpLong != nullptr)
                            {
                                tmpLong->noteTime_end = noteTime;
                                tmpLong->notePosition_end = CalcNotePosition(1, tmpLong->noteSpeed, EMPTY_TIME, tmpLong->noteTime_end, tmpLong->noteBpm, 0);
                                prevLongArrow = tmpLong->noteArrow;
                                if(tmpBar[MAPLINE_HIT].at(i) == '0')
                                    tmpLong = nullptr;
                            }
                            /* Single hit note */
                            if(tmpBar[MAPLINE_HIT].at(i) == '1' ||
                               tmpBar[MAPLINE_HIT].at(i) == 'W' ||
                               tmpBar[MAPLINE_HIT].at(i) == 'A' ||
                               tmpBar[MAPLINE_HIT].at(i) == 'S' ||
                               tmpBar[MAPLINE_HIT].at(i) == 'D')
                            {
                                isFixedArrow = FixedArrow_New(nArrow, tmpBar[MAPLINE_HIT].at(i));
                                if(!isFixedArrow)
                                    nArrow = GenArrow();
                                InsertNote(nowPtrPtr, noteTime, noteTime, NTYPE_HIT, nArrow, nowBPM, nowSpeed, isFixedArrow, noteId, 0, 0);
                                if(tmpLong != nullptr)
                                {
                                    while((*nowPtrPtr)->noteArrow == tmpLong->noteArrow)
                                        (*nowPtrPtr)->noteArrow = GenArrow();
                                    tmpLong->isSingleInLong = true;
                                    (*nowPtrPtr)->isSingleInLong = true;
                                }
                                noteId++;
                                (*nowPtrPtr)->prevNote = prevPtr2;
                                prevPtr = (*nowPtrPtr);
                                prevPtr2 = (*nowPtrPtr);
                                if(tmpBar[MAPLINE_HOLD].at(i) == '3' && tmpLong != nullptr)
                                    tmpLong = nullptr;
                                nowPtrPtr = &((*nowPtrPtr)->nextNote);
                            }
                            noteTime += (MIN_TO_SEC / (nowBPM*nowInterval)) * 1000.0;
                        }
                        nowbarPtrPtr = &((*nowbarPtrPtr)->nextBar);
                        for(int i = 0; i < MAPLINE_TOTAL; i++)
                        {
                            tmpBar[i].clear();
                            tmpBar[i] = "";
                        }
                    }
                }
            }
        }
    }
    if(!isMapEmpty)
    {
        totalNote = prevPtr->noteId;
        if(prevPtr->noteType == NTYPE_HOLD)
        {
            totalNote++;
        }
        prevbarPtr->barTime_end = noteTime;
        last_bar = prevbarPtr;
        prevbarPtr->last_note = prevPtr2;
        return true;
    }
    else
    {
        ErrorMsg(errormsg, nowLine);
        al_show_native_message_box(nullptr, "ERROR!", errormsg, "Unknown error! please contact developer.", "OK", ALLEGRO_MESSAGEBOX_ERROR);
        return false;
    }
}
