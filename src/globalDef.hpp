#ifndef _GLOBALDEF_H_INCLUDED_
#define _GLOBALDEF_H_INCLUDED_
#include "Theme.hpp"
/* Empty time before song start. */
#define EMPTY_TIME -4000.0

extern bool g_isRestart;
extern bool g_isRun;
extern bool g_needRestartDisp;
extern double gScaleRate;

typedef struct
{
    double nowTime;
    double bgmTime;
    double bgmLen;
    double escTimeStamp;
}W_TIMERINFO;

typedef enum mapDifficulties
{
    MAP_BASIC,
    MAP_NORMAL,
    MAP_HARD,
    MAP_SPECIAL,
    MAP_EXPERT,
    MAP_TOTAL // This must put at last for counting.
}W_MAPDIFF;

typedef enum gameState
{
    W_MENU,
    W_SELECT_MUSIC,
    W_SETTING,
    W_KEYMAPCFG,
    W_LOADMAP,
    W_PLAYING,
    W_RESULT,
    W_EMPTY
}W_STATE;

enum
{
    RESULT_PERFECT,
    RESULT_GREAT,
    RESULT_COOL,
    RESULT_BAD,
    RESULT_MISS,
    RESULT_COMBO,
    RESULT_ACCURACY,
    RESULT_SCORE,
    RESULT_TOTAL,
    RESULT_HOLD_MISS  // Hold note need consider head and tail, so write an exception for it.
};

typedef enum
{
    SHOWTIME_SPEEDUP,
    SHOWTIME_SPEEDDOWN,
    SHOWTIME_ROTATE,
    SHOWTIME_HIDDEN,
    SHOWTIME_REVERSE,
    SHOWTIME_TOTAL
}W_SHOWTIME_TYPE;

extern bool AllegroInit(void);

#endif // _GLOBALDEF_H_INCLUDED_
