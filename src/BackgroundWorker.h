#ifndef BACKGROUNDWORKER_H
#define BACKGROUNDWORKER_H
#include <thread>
#include "Allegro_include.hpp"

class BackgroundWorker
{
    public:
        BackgroundWorker();
        virtual ~BackgroundWorker();
        bool Init();
        bool Start();
        void Stop();
    protected:

    private:
        std::thread runner;
        ALLEGRO_EVENT ev;
        ALLEGRO_EVENT_QUEUE* ev_queue;
        bool isAlive;
        void Loop();
};

#endif // BACKGROUNDWORKER_H
