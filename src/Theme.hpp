#ifndef THEME_HPP_INCLUDED
#define THEME_HPP_INCLUDED
#include <string>

#define AUTOBTNX        0.76
#define AUTOBTNY        0
#define SHOWTIMEBTNX    0.88
#define SHOWTIMEBTNY    0

typedef struct
{
    double gaugebaroffsetX;
    double gaugebaroffsetY;
    double judgeAreaOffsetX;
    double judgeAreaOffsetY;
    double comboTextX;
    double comboTextY;
}ThemeSet;

class ThemeManager
{
public:
    ThemeManager();
    ~ThemeManager();
    bool ReadConfig(const std::string& path);
    ThemeSet theme;
    void DefaultTheme();
    const ThemeSet& GetTheme()
    {
        return theme;
    };
private:
};

extern ThemeManager* gTHEMEMGR;

#endif // THEME_HPP_INCLUDED
