#ifndef BGACTRL_HPP_INCLUDED
#define BGACTRL_HPP_INCLUDED

#include "Allegro_include.hpp"

class BGACtrl
{
public:
    BGACtrl();
    ~BGACtrl();
    bool LoadBGA(const char* bgaPath, int screenWidth, int screenHeight);
    void PlayBGA();
    void Pause();
    void Resume();
    void DrawBGA();
    void Destroy();
    bool GetBGAStat();
private:
    ALLEGRO_VIDEO* bga;
    ALLEGRO_BITMAP* bgaFrame;
    bool isBGAPaused;
    bool isBGAPlayed;
    bool isBGALoaded;
    int screenWidth;
    int screenHeight;
};

#endif
