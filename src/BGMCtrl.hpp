#ifndef BGMCTRL_HPP_INCLUDED
#define BGMCTRL_HPP_INCLUDED

#include "Allegro_include.hpp"
class BGMCtrl
{
public:
    BGMCtrl();
    ~BGMCtrl();
    bool LoadBGM(const char* bgmPath, int volume);
    bool PlayBGM();
    bool PauseBGM();
    void Destroy();
    bool GetBGMStat();
    double GetMusicPos();
    void SetMusicPos(int posSeconds);
    double GetMusicLen()
    {
        return (double)al_get_sample_instance_length(musicMixer) / (double)musicSampleRate * 1000;
    }
private:
    ALLEGRO_SAMPLE_INSTANCE *musicMixer;
    ALLEGRO_SAMPLE* music;
    unsigned int musicPosition;
    unsigned int musicSampleRate;
    bool isBGMPaused;
};
#endif // BGMCTRL_HPP_INCLUDED
