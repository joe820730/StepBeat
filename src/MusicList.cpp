#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <fstream>

#include "globalDef.hpp"
#include "NoteInfo.hpp"
#include "MapInfo.hpp"
#include "ConfigProvider.hpp"
#include "MusicList.hpp"
#include "ConfigMenu.hpp"
#include "GamePlayCtrl.hpp"
#include "ResultScreen.hpp"
#include "GameEvents.hpp"
#include "StateHandler.hpp"

#define SONGDIR "./Songs"

using namespace std;

MusicList::MusicList()
{
    xLoc = MAPLISTX;
    yLoc = MAPLISTX;
    menuIdx = 0;
    diffIdx = 0;
    wSongList = nullptr;
    tmpList = nullptr;
    selectedSong = nullptr;
    isSongScanning = false;
    gameCfg = nullptr;
    menuBaseLocY = 0;
}

MusicList::~MusicList()
{
    if(wSongList != nullptr)
    {
        SongListClean(wSongList);
    }
}

void MusicList::GetFontHeight()
{
    fontHeight = al_get_font_line_height(wRes->gameFonts[TITLE_FONT]);
}

bool MusicList::Setup(ConfigProvider* cfg_provider)
{
    if(cfg_provider == nullptr)
        return false;
    gameCfg = cfg_provider;
    SetMenuLoc(gameCfg->GetConfig().screenHeight, gameCfg->GetConfig().screenWidth);
    return true;
}

void MusicList::SetMenuLoc(int scrHeight, int scrWidth)
{
    screenHeight = scrHeight;
    screenWidth = scrWidth;
    menuLocateX = xLoc * screenWidth;
    menuLocateY = yLoc * screenHeight;
    menuBaseLocY = menuLocateY;
}

bool MusicList::ScanSong(bool isRescan)
{
    bool ret = true;
    selectedSong = nullptr;
    if(isSongScanning) return true;
    isSongScanning = true;
    menuIdx = 0;
    diffIdx = 0;
    if(!isRescan)
    {
      if(wSongList != nullptr)
      {
        SongListClean(wSongList);
      }
    }
    else
    {
      if(tmpList != nullptr)
      {
        SongListClean(tmpList);
        tmpList = nullptr;
      }
    }
    int readstat = ReadDir(SONGDIR, &tmpList);
    switch(readstat)
    {
        case SCAN_SUCCESS:
        {
            break;
        }
        case SCAN_NODIR:
        {
            al_make_directory(SONGDIR);
            al_show_native_message_box(nullptr, "Error!",
                                       "Songs directory is missing and re-generated.",
                                       "Please put some map file in it and launch program again.", "OK", 0);
            break;
        }
        case SCAN_PATH_ERROR:
        {
            al_show_native_message_box(nullptr, "Error!", "Error occurred when reading map file.", "Is there any special character in your directories?", "OK", 0);
            ret = false;
            break;
        }
        case SCAN_UNKNOWN:
        {
            al_show_native_message_box(nullptr, "Error!", "Unknown error.", "Maybe you need check directory permission", "OK", 0);
            ret = false;
            break;
        }
    }
    W_SONGLIST *tempPtr = tmpList;
    while(tempPtr != nullptr)
    {
        menuIdxMAX = tempPtr->listIdx + 1;
        tempPtr = tempPtr->nextSong;
    }
    selectedSong = tmpList;
    if(selectedSong != nullptr)
    {
        for(int i = 0; i < MAP_TOTAL; i++)
        {
            if(selectedSong->haveDiff[i])
            {
                diffIdx = i;
                break;
            }
        }
    }
    if(isRescan)
        gGAMEEVENTS->EmitEvent(W_EVENT_ID_SCANSONG_END);
    else
    {
      wSongList = tmpList;
      tmpList = nullptr;
    }
    return ret;
}

void MusicList::CloneBitmap()
{
  if(!isSongScanning) return;
  if(wSongList != nullptr)
  {
    SongListClean(wSongList);
    wSongList = tmpList;
    tmpList = nullptr;
  }
    W_SONGLIST *tempPtr = wSongList;
    while(tempPtr != nullptr)
    {
        ALLEGRO_BITMAP* temp = tempPtr->cover;
        if(temp != nullptr)
        {
            tempPtr->cover = al_clone_bitmap(temp);
            al_destroy_bitmap(temp);
        }
        tempPtr = tempPtr->nextSong;
    }
    isSongScanning = false;
}

void MusicList::MenuCtrl(int mouseX, int mouseY)
{
    if(mouseX >= menuLocateX)
    {
        int tempIDX = (mouseY - menuLocateY)/(fontHeight+screenHeight*0.03);
        if(tempIDX >=0 && tempIDX < menuIdxMAX)
        {
            menuIdx = tempIDX;
            Idx2SongPtr();
            SearchDiff();
        }
    }
    else if(mouseX >= screenWidth*0.25 && mouseX <= screenWidth*0.35)
    {
        int tempIDX = (mouseY - (screenHeight * 0.55))/fontHeight;
        if(tempIDX >=0 && tempIDX < MAP_TOTAL)
        {
            if(selectedSong->haveDiff[tempIDX])
                diffIdx = tempIDX;
        }
    }
}
void MusicList::MenuCtrl(int keyCode)
{
    switch(keyCode)
    {
        case ALLEGRO_KEY_F1:
        {
            gGAMEPLAYCTRL->isAutoPlay = !gGAMEPLAYCTRL->isAutoPlay;
            break;
        }
        case ALLEGRO_KEY_UP:
        {
            IdxMinus(menuIdx, menuIdxMAX);
            Idx2SongPtr();
            SearchDiff();
            break;
        }
        case ALLEGRO_KEY_DOWN:
        {
            IdxAdd(menuIdx, menuIdxMAX);
            Idx2SongPtr();
            SearchDiff();
            break;
        }
        case ALLEGRO_KEY_PGUP:
        {
            IdxMinus(menuIdx, menuIdxMAX, 5);
            Idx2SongPtr();
            SearchDiff();
            break;
        }
        case ALLEGRO_KEY_PGDN:
        {
            IdxAdd(menuIdx, menuIdxMAX, 5);
            Idx2SongPtr();
            SearchDiff();
            break;
        }
        case ALLEGRO_KEY_LEFT:
        {
            if(selectedSong != nullptr)
            {
                for(int tempIdx = diffIdx; tempIdx > 0; tempIdx--)
                {
                    if(selectedSong->haveDiff[tempIdx-1])
                    {
                        diffIdx = tempIdx - 1;;
                        break;
                    }
                }
            }
            break;
        }
        case ALLEGRO_KEY_RIGHT:
        {
            if(selectedSong != nullptr)
            {
                for(int tempIdx = diffIdx; tempIdx < MAP_TOTAL - 1; tempIdx++)
                {
                    if(selectedSong->haveDiff[tempIdx+1])
                    {
                        diffIdx = tempIdx+1;;
                        break;
                    }
                }
            }
            break;
        }
        case ALLEGRO_KEY_F2:
        {
            gGAMEPLAYCTRL->enableShowTime = !gGAMEPLAYCTRL->enableShowTime;
            break;
        }
        case ALLEGRO_KEY_F3:
        {
            gCONFIGMENU->SwitchShowTimeConfig();
            break;
        }
        case ALLEGRO_KEY_F5:
        {
            gGAMEEVENTS->EmitEvent(W_EVENT_ID_SCANSONG_BEGIN);
            break;
        }
        case ALLEGRO_KEY_ESCAPE:
        {
            gSTATEHANDLER->SetState(W_MENU);
            break;
        }
        case ALLEGRO_KEY_ENTER:
        case ALLEGRO_KEY_PAD_ENTER:
        {
            if(!gCONFIGMENU->CheckShowTimeCfgVisible())
            {
                gSTATEHANDLER->SetState(W_LOADMAP);
                gGAMEEVENTS->EmitEvent(W_EVENT_ID_LOADMAP_BEGIN, (intptr_t)selectedSong, (W_MAPDIFF)diffIdx);
                gRESULTSCREEN->AttachCurSongDiff(selectedSong, (W_MAPDIFF)diffIdx);
            }
            else
            {
                gCONFIGMENU->SwitchShowTimeConfig();
            }
            break;
        }
        default:
        {
            break;
        }
    }
}

