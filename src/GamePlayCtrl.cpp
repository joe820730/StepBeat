#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <unistd.h>
#include <thread>

#include "globalDef.hpp"
#include "GamePlayCtrl.hpp"
#include "NoteInfo.hpp"
#include "ConfigProvider.hpp"
#include "BGMCtrl.hpp"
#include "ResultScreen.hpp"
#include "StateHandler.hpp"
#include "debug.hpp"

using namespace std;

GamePlayCtrl::GamePlayCtrl()
{
    timerStart = 0;
    timerNow = 0;
    pausedTime = 0;
    mapReader = new MapReader;
    notes_bar = nullptr;
    Reset();
    isAutoPlay = false;
    isMapPlaying = false;
    enableShowTime = false;
    isPaused = false;
    cur_song = nullptr;
    last_bar = nullptr;
    maphandle_cost_time = 0;
    AutoPlayReset();
}

bool GamePlayCtrl::Setup(ConfigProvider* cfg_provider)
{
    if(cfg_provider == nullptr)
        return false;
    gameCfg = cfg_provider->GetConfig();
    keyCfg = cfg_provider->GetKeymap();
    showtimeConfig = cfg_provider->GetShowtime();
    ResizeObj(gameCfg.screenWidth, gameCfg.screenHeight);
    if(PressSoundPreProc())
        return true;
    else
        return false;
}

void GamePlayCtrl::ResizeObj(int screenW, int screenH)
{
    note_judge_area = screenW * JUDGEAREAX;
    note_y_pos = screenH * JUDGEAREAY;
    noteSize = wRes->arrows[WAB_ARROW][0].GetHeight() * gScaleRate;
    hittedQueue.RecalcLoc();
}

bool GamePlayCtrl::PressSoundPreProc()
{
    pressSound = al_create_sample_instance(wRes->wSounds[WS_PRESS]);
    if(pressSound == nullptr)
    {
        printf("Can't create sample instance!!\n");
        return false;
    }
    if(!al_attach_sample_instance_to_mixer(pressSound, al_get_default_mixer()))
    {
        printf("Can't attach sample instance to mixer!!\n");
        return false;
    }
    if(!al_set_sample_instance_playmode(pressSound, ALLEGRO_PLAYMODE_LOOP))
    {
        printf("Can't set sample instance play mode!!\n");
        return false;
    }
    if(!PressSoundSetVol())
    {
        return false;
    }
    return true;
}

bool GamePlayCtrl::PressSoundSetVol()
{
    if(!al_set_sample_instance_gain(pressSound, (double)gameCfg.effectVol/100.0))
    {
        printf("Can't set sample instance gain!!\n");
        return false;
    }
    else
        return true;
}

bool GamePlayCtrl::StopPressSound()
{
    return al_stop_sample_instance(pressSound);
}

void GamePlayCtrl::Reset()
{
    judgeNote_keep = nullptr;
    for(int i = 0; i < NA_TOTAL; i++)
    {
        judgeNote_long[i] = nullptr;
        isPressing[i] = false;
    }
    for(uint8_t i = 0; i < RESULT_TOTAL; i++)
        scoreRecord[i] = 0;
    nowCombo = 0;
    accuracy = 100.0;
    isHitting = false;
    hitCount = 0;
    hitCountTarget = 0;
    latestJudge = RESULT_MISS+1;
    noteSpeed = 1.0;
    showtimegather = 0;
    showtimegatherMax = 0;
    isShowTime = false;
    showType = SHOWTIME_TOTAL;
    judgeFrameTime = 0;
    comboTextScale = 0;
    showtime_StartId = 0;
    showtime_scoremultiplier = 1;
    showtime_targetSpeed = 1;
    AutoPlayReset();
    hittedQueue.Reset();
    escapeTimerStamp = 0;
    holdFrameIdx = 0;
    holdFrameTime = 0;
}

void GamePlayCtrl::CleanUp()
{
    BarClean(notes_bar);
    delete mapReader;
}

void GamePlayCtrl::LoadMap(W_SONGLIST* selectedSong, W_MAPDIFF selectedDiff)
{
    if(isMapPlaying)
    {
        return;
    }
    if(selectedSong == nullptr)
    {
        gSTATEHANDLER->SetState(W_SELECT_MUSIC);
        return;
    }
    bool isRetry = false;
    if(cur_song != selectedSong || cur_song_diff != selectedDiff || cur_song->isEditing)
    {
        bgmCtrl.Destroy();
        cur_song = selectedSong;
        cur_song_diff = selectedDiff;
        isRetry = false;
    }
    else
    {
        isRetry = true;
    }
    Reset();
    if(isRetry)
    {
        if(!cur_song->haveDiff[cur_song_diff])
        {
            gSTATEHANDLER->SetState(W_SELECT_MUSIC);
            return;
        }
        bgmCtrl.SetMusicPos(0);
        maxAccuracy = maxNotes;
        BarArrowReset(notes_bar, cur_song->beginAt);
        SetShowTimeGatherMax(cur_song_diff);
    }
    else
    {
        maxNotes = 0;
        maxAccuracy = 0;
        BarInit(notes_bar);
        uint32_t totalNote = 0;
        if(mapReader->ReadMap(notes_bar, cur_song, cur_song_diff, totalNote, last_bar))
        {
            SetShowTimeGatherMax(cur_song_diff);
            maxNotes = totalNote;
            maxAccuracy = totalNote;
        }
        else
        {
            al_show_native_message_box(nullptr, "Error!", "File format error!", "File format error." , nullptr, ALLEGRO_MESSAGEBOX_ERROR);
            gSTATEHANDLER->SetState(W_SELECT_MUSIC);
            return;
        }
        if(bgmCtrl.LoadBGM(cur_song->wavPath.c_str(), gameCfg.musicVol))
        {
            timers.bgmLen = bgmCtrl.GetMusicLen()/1000.0;
        }
        else
        {
            al_show_native_message_box(nullptr, "Error!", "Music file error!", "No music file or path error." , nullptr, ALLEGRO_MESSAGEBOX_ERROR);
            gSTATEHANDLER->SetState(W_SELECT_MUSIC);
            return;
        }
    }
    timerStart = al_get_time()*1000 - EMPTY_TIME;
    if(cur_song->isEditing)
        isAutoPlay = true;
    timers.nowTime = EMPTY_TIME;
    pausedTime = 0;
    isPaused = false;
    isMapPlaying = true;
}

void GamePlayCtrl::MapHandle(double deltaTime)
{
    if(!g_isRun)
        return;
    if(gSTATEHANDLER->GetState() != W_PLAYING)
        return;
    timerNow = al_get_time() * 1000;
    double costStart = timerNow;
    if(isPaused)
    {
        if(pausedTime == 0)
        {
            pausedTime = timerNow;
            bgmCtrl.PauseBGM();
        }
    }
    else
    {
        if(pausedTime != 0)
        {
            timerStart = timerStart + (timerNow - pausedTime);
            bgmCtrl.PlayBGM();
            timerStart += (timers.nowTime - bgmCtrl.GetMusicPos());
            pausedTime = 0;
        }
        timers.nowTime = (timerNow - timerStart);
        if(cur_song != nullptr)
        {
            if(cur_song->isEditing)
                timers.nowTime+=cur_song->beginAt;
        }
    }
    if(!isMusicPlaying && timers.nowTime > 0)
    {
        if(cur_song != nullptr)
        {
            if(cur_song->isEditing)
                bgmCtrl.SetMusicPos(cur_song->beginAt/1000.0);
        }
        else
        {
            gSTATEHANDLER->SetState(W_SELECT_MUSIC);
        }
        if(!bgmCtrl.PlayBGM())
        {
            al_show_native_message_box(nullptr, "Error!", "Music file error!", "Music file format error!." , nullptr, ALLEGRO_MESSAGEBOX_ERROR);
            gSTATEHANDLER->SetState(W_SELECT_MUSIC);
        }
        else
        {
            isMusicPlaying = true;
            PressSoundSetVol();
            // adjust offset
            timerStart += (timers.nowTime - bgmCtrl.GetMusicPos());
        }
    }
    timers.bgmTime = bgmCtrl.GetMusicPos();
    AutoPlay(timers.nowTime);
    FindJudgeNote(timers.nowTime);
    MoveNote(timers.nowTime);
    ShowTimeCtrl(deltaTime);

    /* If music is end, return to music list or result screen in future. */
    if(isMapPlaying && !bgmCtrl.GetBGMStat() && timers.nowTime/1000.0 >= timers.bgmLen)
    {
        if(!isAutoPlay)
        {
            for(int i = 0; i < HISCORE_MAX; i++)
            {
                if(enableShowTime)
                {
                    if(scoreRecord[RESULT_SCORE] > cur_song->scoredb[cur_song_diff].hiscore_bonus[i])
                    {
                        gRESULTSCREEN->current_rank = i;
                        gRESULTSCREEN->input_playername = true;
                        gRESULTSCREEN->is_bonus_mode = enableShowTime;
                        break;
                    }
                }
                else
                {
                    if(scoreRecord[RESULT_SCORE] > cur_song->scoredb[cur_song_diff].hiscore[i])
                    {
                        gRESULTSCREEN->current_rank = i;
                        gRESULTSCREEN->input_playername = true;
                        gRESULTSCREEN->is_bonus_mode = enableShowTime;
                        break;
                    }
                }
            }
        }
        gSTATEHANDLER->SetState(W_RESULT);
    }
    maphandle_cost_time = al_get_time()*1000 - costStart;
}

void GamePlayCtrl::StopPlaying()
{
    bgmCtrl.PauseBGM();
    isMapPlaying = false;
    isMusicPlaying = false;
    StopPressSound();
    if(cur_song != nullptr)
    {
        if(cur_song->isEditing)
        {
            isAutoPlay = false;
        }
    }
}
