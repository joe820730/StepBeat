#include <iostream>
#include <sstream>
#include <iomanip>
#include <cmath>
#include <queue>

#include "globalDef.hpp"
#include "NoteInfo.hpp"
#include "GameResources.hpp"
#include "GamePlayCtrl.hpp"

double CalcRotateAngle(double position, double note_judge_area, double rotateDist)
{
    double rotateAngle = ALLEGRO_PI*(position - note_judge_area + rotateDist)/250;
    if(rotateAngle > 0)
        rotateAngle = 0;
    return rotateAngle;
}

ALLEGRO_COLOR CalcAlpha(double position, double note_judge_area, double hiddenDist)
{
    double tempAlpha = 0;
    if(position > (note_judge_area - hiddenDist))
    {
        tempAlpha = (note_judge_area - position)/hiddenDist;
        if(tempAlpha < 0)
            tempAlpha = 0;
    }
    else
        tempAlpha = 1;
    return al_map_rgba_f(tempAlpha, tempAlpha, tempAlpha, tempAlpha);
}

void GamePlayCtrl::DrawLongBg(NOTES_LINKLIST* nowPtr, double _length, bool isReverse)
{
    if( nowPtr->noteSpeed >= 0 || enableShowTime)
    {
        if(!isReverse)
        {
            wRes->arrows[nowPtr->noteArrow][WAB_BG].Draw(nowPtr->notePosition+(noteSize/2.0), note_y_pos, 2);
            wRes->arrows[nowPtr->noteArrow][WAB_BG].DrawScaled(nowPtr->notePosition_end+(noteSize/2.0), note_y_pos, _length, 0, 1);
            wRes->arrows[nowPtr->noteArrow][WAB_BG].Draw(nowPtr->notePosition_end, note_y_pos, 0);
        }
        else
        {
            wRes->bmpGroups[WRG_REVERSEBG].Draw(nowPtr->notePosition+(noteSize/2.0), note_y_pos, 2);
            wRes->bmpGroups[WRG_REVERSEBG].DrawScaled(nowPtr->notePosition_end+(noteSize/2.0), note_y_pos, _length, 0, 1);
            wRes->bmpGroups[WRG_REVERSEBG].Draw(nowPtr->notePosition_end, note_y_pos, 0);
        }
    }
    else
    {
        wRes->arrows[nowPtr->noteArrow][WAB_BG].Draw(nowPtr->notePosition, note_y_pos, 0);
        wRes->arrows[nowPtr->noteArrow][WAB_BG].DrawScaled(nowPtr->notePosition+(noteSize/2.0), note_y_pos, _length, noteSize, 1);
        wRes->arrows[nowPtr->noteArrow][WAB_BG].Draw(nowPtr->notePosition_end+(noteSize/2.0), note_y_pos, 2);
    }
}

void GamePlayCtrl::DrawSingleNote(NOTES_LINKLIST* nowPtr)
{
    if (nowPtr->noteArrow != NA_TOTAL)
    {
        if(!isShowTime || nowPtr->noteId < showtime_StartId || nowPtr->noteId >= showtime_StartId+showtimegatherMax)
        {
            wRes->arrows[nowPtr->noteArrow][WAB_BG].Draw(nowPtr->notePosition, note_y_pos, 0);
            wRes->arrows[nowPtr->noteArrow][WAB_BG].Draw(nowPtr->notePosition+(noteSize/2.0), note_y_pos, 2);
            wRes->arrows[nowPtr->noteArrow][WAB_ARROW].Draw(nowPtr->notePosition, note_y_pos, 0);
        }
        else
        {
            switch(showType)
            {
                case SHOWTIME_HIDDEN:
                {
                    wRes->arrows[nowPtr->noteArrow][WAB_BG].DrawTinted(nowPtr->notePosition, note_y_pos, CalcAlpha(nowPtr->notePosition, note_judge_area-(noteSize/2.0), hiddenDistance),0);
                    wRes->arrows[nowPtr->noteArrow][WAB_BG].DrawTinted(nowPtr->notePosition+(noteSize/2.0), note_y_pos, CalcAlpha(nowPtr->notePosition, note_judge_area-(noteSize/2.0), hiddenDistance), 2);
                    wRes->arrows[nowPtr->noteArrow][WAB_ARROW].DrawTinted(nowPtr->notePosition, note_y_pos, CalcAlpha(nowPtr->notePosition, note_judge_area-(noteSize/2.0), hiddenDistance), 0);
                    break;
                }
                case SHOWTIME_REVERSE:
                {
                    wRes->bmpGroups[WRG_REVERSEBG].Draw(nowPtr->notePosition, note_y_pos, 0);
                    wRes->bmpGroups[WRG_REVERSEBG].Draw(nowPtr->notePosition+(noteSize/2.0), note_y_pos, 2);
                    wRes->arrows[nowPtr->noteArrow][WAB_ARROW].Draw(nowPtr->notePosition, note_y_pos, 1);
                    break;
                }
                default:
                {
                    wRes->arrows[nowPtr->noteArrow][WAB_BG].Draw(nowPtr->notePosition, note_y_pos, 0);
                    wRes->arrows[nowPtr->noteArrow][WAB_BG].Draw(nowPtr->notePosition+(noteSize/2.0), note_y_pos, 2);
                    if(showType == SHOWTIME_ROTATE)
                    {
                        double rotateAngle = CalcRotateAngle(nowPtr->notePosition, note_judge_area, rotateDistance);
                        wRes->arrows[nowPtr->noteArrow][WAB_ARROW].DrawRotate(nowPtr->notePosition, note_y_pos, rotateAngle, 0);
                    }
                    else
                    {
                        wRes->arrows[nowPtr->noteArrow][WAB_ARROW].Draw(nowPtr->notePosition, note_y_pos, 0);
                    }
                    break;
                }
            }
        }
    }
}

void GamePlayCtrl::DrawLongNote(NOTES_LINKLIST* nowPtr)
{
    if(nowPtr->noteArrow != NA_TOTAL)
    {
        double _length = fabs(nowPtr->notePosition_end - nowPtr->notePosition);
        double _arrowPosOffset = _length / (double)nowPtr->khitDrawCount;
        if(!isShowTime || nowPtr->noteId < showtime_StartId || nowPtr->noteId >= showtime_StartId+showtimegatherMax ||
           showType == SHOWTIME_SPEEDDOWN || showType == SHOWTIME_SPEEDUP)
        {
            DrawLongBg(nowPtr, _length);

            wRes->arrows[nowPtr->noteArrow][WAB_ARROW].Draw(nowPtr->notePosition, note_y_pos, 0);
            if(nowPtr->noteType == NTYPE_KHIT)
            {
                if( nowPtr->noteSpeed >= 0 || enableShowTime)
                    for(int i = nowPtr->khitDrawCount-1; i > 0; i--)
                        wRes->arrows[nowPtr->noteArrow][WAB_ARROW].Draw(nowPtr->notePosition_end+((_arrowPosOffset)*i), note_y_pos, 0);
                else
                    for(int i = nowPtr->khitDrawCount-1; i > 0; i--)
                        wRes->arrows[nowPtr->noteArrow][WAB_ARROW].Draw(nowPtr->notePosition_end-((_arrowPosOffset)*i), note_y_pos, 0);
            }
            wRes->arrows[nowPtr->noteArrow][WAB_ARROW].Draw(nowPtr->notePosition_end, note_y_pos, 0);
        }
        else
        {
            if(showType == SHOWTIME_REVERSE)
            {
                DrawLongBg(nowPtr, _length, true);
                wRes->arrows[nowPtr->noteArrow][WAB_ARROW].Draw(nowPtr->notePosition, note_y_pos, 1);
                if(nowPtr->noteType == NTYPE_KHIT)
                {
                    for(int i = nowPtr->khitDrawCount-1; i > 0; i--)
                        wRes->arrows[nowPtr->noteArrow][WAB_ARROW].Draw(nowPtr->notePosition_end+((_arrowPosOffset)*i), note_y_pos, 1);
                }
                wRes->arrows[nowPtr->noteArrow][WAB_ARROW].Draw(nowPtr->notePosition_end, note_y_pos, 1);
            }
            else
            {
                DrawLongBg(nowPtr, _length);

                if(showType == SHOWTIME_HIDDEN)
                {
                    wRes->arrows[nowPtr->noteArrow][WAB_ARROW].DrawTinted(nowPtr->notePosition, note_y_pos, CalcAlpha(nowPtr->notePosition, note_judge_area, hiddenDistance), 0);
                    if(nowPtr->noteType == NTYPE_KHIT)
                    {
                        for(int i = nowPtr->khitDrawCount-1; i > 0; i--)
                        {
                            double tempPosition = nowPtr->notePosition_end+((_arrowPosOffset)*i);
                            wRes->arrows[nowPtr->noteArrow][WAB_ARROW].DrawTinted(tempPosition, note_y_pos, CalcAlpha(tempPosition, note_judge_area, hiddenDistance), 0);
                        }
                    }
                    wRes->arrows[nowPtr->noteArrow][WAB_ARROW].DrawTinted(nowPtr->notePosition_end, note_y_pos, CalcAlpha(nowPtr->notePosition_end, note_judge_area, hiddenDistance), 0);
                }
                else
                {
                    wRes->arrows[nowPtr->noteArrow][WAB_ARROW].DrawRotate(nowPtr->notePosition, note_y_pos,
                                                                          CalcRotateAngle(nowPtr->notePosition, note_judge_area, rotateDistance), 0);
                    if(nowPtr->noteType == NTYPE_KHIT)
                    {
                        for(int i = nowPtr->khitDrawCount-1; i > 0; i--)
                        {
                            double tempPosition = nowPtr->notePosition_end+((_arrowPosOffset)*i);
                            wRes->arrows[nowPtr->noteArrow][WAB_ARROW].DrawRotate(tempPosition, note_y_pos,
                                                                                  CalcRotateAngle(tempPosition, note_judge_area, rotateDistance), 0);
                        }
                    }
                    wRes->arrows[nowPtr->noteArrow][WAB_ARROW].DrawRotate(nowPtr->notePosition_end, note_y_pos,
                                                                          CalcRotateAngle(nowPtr->notePosition_end, note_judge_area, rotateDistance), 0);
                }
            }
        }
        if(nowPtr->noteType == NTYPE_KHIT)
        {
            if(nowPtr->noteSpeed >= 0 || enableShowTime)
            {
                wRes->bitmaps[WR_HITTEXT].Draw(nowPtr->notePosition-noteSize*0.75, gameCfg.screenHeight*HITTEXTY);
                wRes->bitmaps[WR_HITNUMX].Draw(nowPtr->notePosition-noteSize, gameCfg.screenHeight*HITNUMY);
                double numOffset = wRes->bitmaps[WR_HITNUMX].GetWidth();
                DrawBitmapNum(WRG_HITNUM, nowPtr->hitCountTarget, nowPtr->notePosition-noteSize+numOffset, gameCfg.screenHeight*HITNUMY,BITMAPALIGN_LEFT,2,false);
            }
            else
            {
                wRes->bitmaps[WR_HITTEXT].Draw(nowPtr->notePosition_end-noteSize*0.75, gameCfg.screenHeight*HITTEXTY);
                wRes->bitmaps[WR_HITNUMX].Draw(nowPtr->notePosition_end-noteSize, gameCfg.screenHeight*HITNUMY);
                double numOffset = wRes->bitmaps[WR_HITNUMX].GetWidth();
                DrawBitmapNum(WRG_HITNUM, nowPtr->hitCountTarget, nowPtr->notePosition_end-noteSize+numOffset, gameCfg.screenHeight*HITNUMY,BITMAPALIGN_LEFT,2,false);
            }
        }
        else if(nowPtr->noteType == NTYPE_HOLD)
        {
            if(nowPtr->noteSpeed >= 0 || enableShowTime)
                wRes->bitmaps[WR_HOLDTEXT].Draw(nowPtr->notePosition-noteSize, gameCfg.screenHeight*HOLDTEXTY);
            else
                wRes->bitmaps[WR_HOLDTEXT].Draw(nowPtr->notePosition_end-noteSize, gameCfg.screenHeight*HOLDTEXTY);
        }
    }
}

void GamePlayCtrl::DrawNote()
{
    std::queue<NOTES_LINKLIST*> note_queue;
    NOTES_BARLIST* nowbarPtr = last_bar;
    while(nowbarPtr != nullptr)
    {
        if(nowbarPtr->visible)
        {
            NOTES_LINKLIST* nowPtr = nowbarPtr->last_note;
            while(nowPtr != nullptr)
            {
                if(nowPtr->visible && nowPtr->notePosition > -120)
                {
                    switch(nowPtr->noteType)
                    {
                        case NTYPE_HIT:
                        {
                            if(nowPtr->isSingleInLong)
                            {
                                note_queue.push(nowPtr);
                            }
                            else
                            {
                                DrawSingleNote(nowPtr);
                            }
                            break;
                        }
                        case NTYPE_HOLD:
                        case NTYPE_KHIT:
                        {
                            DrawLongNote(nowPtr);
                            break;
                        }
                        default:
                            break;
                    }
                }
                nowPtr = nowPtr->prevNote;
            }
        }
        nowbarPtr = nowbarPtr->prevBar;
    }
    // TODO: std::queue not have 'reserve' method, is this cause a performance problem?
    while (!note_queue.empty())
    {
        DrawSingleNote(note_queue.front());
        note_queue.pop();
    }
}
