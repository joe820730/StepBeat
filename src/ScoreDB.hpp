#ifndef SCOREDB_HPP_INCLUDED
#define SCOREDB_HPP_INCLUDED
#include <string>
#include "globalDef.hpp"
#define HISCORE_MAX 5
#define PLAYERNAME_MAXLEN 32

typedef struct
{
    std::string playername[HISCORE_MAX];
    std::string playername_bonus[HISCORE_MAX];
    uint8_t diff;
    uint8_t md5sum[16];
    int64_t last_modified_time;
    int32_t hiscore[HISCORE_MAX];
    double accuracy[HISCORE_MAX];
    int32_t hiscore_bonus[HISCORE_MAX];
    double accuracy_bonus[HISCORE_MAX];
}W_SCOREDB;

bool write_score_db(const std::string& path, const W_SCOREDB target_db[MAP_TOTAL]);
bool read_score_db(const std::string& path, W_SCOREDB target_db[MAP_TOTAL]);

#endif // SCOREDB_HPP_INCLUDED
