#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>

#include "globalDef.hpp"
#include "ConfigProvider.hpp"

#ifdef DEBUG
#define PRINTCFG(key,val) printf("%s = %d\n", (key), (val));
#define PRINTSEC(key) printf("[%s]\n", (key));
#else
#define PRINTCFG(key,val)
#define PRINTSEC(key)
#endif // DEBUG

using namespace std;
template <class TARGETVAL> inline bool str2int(const char* tmpStr, TARGETVAL& targetValue)
{
    if(tmpStr == nullptr)
        return false;
    else
    {
        targetValue = atoi(tmpStr);
        return true;
    }
}

template <class TARGETVAL> bool AssignValue(ALLEGRO_CONFIG*& gameConfig, const char* section, const char* keyword, TARGETVAL& targetValue)
{
    if(str2int(al_get_config_value(gameConfig, section, keyword), targetValue))
    {
        PRINTCFG(keyword, targetValue);
        return true;
    }
    else
        return false;
}

ConfigProvider::ConfigProvider()
{
    memset(&wGameCfg, 0, sizeof(W_GAMECONFIG));
    memset(&wKeyConfig, 0, sizeof(W_KEYCFG));
    for(int i = 0; i < SHOWTIME_TOTAL; i++)
        showtimeConfig[i] = 0;
    gameConfig = nullptr;
}

ConfigProvider::~ConfigProvider()
{
    al_destroy_config(gameConfig);
}

bool ConfigProvider::LoadFile(const string& cfgFileName)
{
    this->cfgFileName = cfgFileName;
    gameConfig = al_load_config_file(cfgFileName.c_str());
    if(gameConfig == nullptr)
    {
        printf("Configure file missing! Use default config.\n");
        GenDefaultCfg();
        printf("Generating default configure file.\n");
        gameConfig = al_create_config();
        if(gameConfig != nullptr)
        {
            return true;
        }
        else
        {
            printf("Can't create default configure structure! Program will exit!\n");
            return false;
        }
    }
    else
    {
        if(!ReadFile())
        {
            GenDefaultCfg();
            return false;
        }
        else
        {
            return true;
        }
    }
}

bool ConfigProvider::ReadFile()
{
    PRINTSEC(WCFG_KEYMAP_SEC);
    if(!AssignValue(gameConfig, WCFG_KEYMAP_SEC, WCFG_KEYUP1, wKeyConfig.wKeyUp_1)) return false;
    if(!AssignValue(gameConfig, WCFG_KEYMAP_SEC, WCFG_KEYUP2, wKeyConfig.wKeyUp_2)) return false;
    if(!AssignValue(gameConfig, WCFG_KEYMAP_SEC, WCFG_KEYDOWN1, wKeyConfig.wKeyDown_1)) return false;
    if(!AssignValue(gameConfig, WCFG_KEYMAP_SEC, WCFG_KEYDOWN2, wKeyConfig.wKeyDown_2)) return false;
    if(!AssignValue(gameConfig, WCFG_KEYMAP_SEC, WCFG_KEYLEFT1, wKeyConfig.wKeyLeft_1)) return false;
    if(!AssignValue(gameConfig, WCFG_KEYMAP_SEC, WCFG_KEYLEFT2, wKeyConfig.wKeyLeft_2)) return false;
    if(!AssignValue(gameConfig, WCFG_KEYMAP_SEC, WCFG_KEYRIGHT1, wKeyConfig.wKeyRight_1)) return false;
    if(!AssignValue(gameConfig, WCFG_KEYMAP_SEC, WCFG_KEYRIGHT2, wKeyConfig.wKeyRight_2)) return false;
    if(!AssignValue(gameConfig, WCFG_KEYMAP_SEC, WCFG_KEYSHOW1, wKeyConfig.wKeyShow_1)) return false;
    if(!AssignValue(gameConfig, WCFG_KEYMAP_SEC, WCFG_KEYSHOW2, wKeyConfig.wKeyShow_2)) return false;

    PRINTSEC(WCFG_CONFIG_SEC);
    /* Offset */
    if(!AssignValue(gameConfig, WCFG_CONFIG_SEC, WCFG_CONFIG_OFFSET, wGameCfg.offsetMs)) return false;
    /* FPS */
    if(!AssignValue(gameConfig, WCFG_CONFIG_SEC, WCFG_CONFIG_FPS, wGameCfg.fps)) return false;
    /* VSync */
    (wGameCfg.fps == 0)?(wGameCfg.vsyncOption = VSYNC_ON):(wGameCfg.vsyncOption = VSYNC_OFF);

    /* Background Dim */
    if(!AssignValue(gameConfig, WCFG_CONFIG_SEC, WCFG_CONFIG_DIM, wGameCfg.bgDim))
        return false;
    else
    {
        if(wGameCfg.bgDim < 0 || wGameCfg.bgDim > 100)
        {
            wGameCfg.bgDim = 30;
        }
    }
    /* ScreenWidth */
    if(!AssignValue(gameConfig, WCFG_CONFIG_SEC, WCFG_CONFIG_SCR_W, wGameCfg.screenWidth))
        return false;
    else
    {
        if(wGameCfg.screenWidth < 800)
        {
            wGameCfg.screenWidth = 800;
        }
    }
    /* ScreenHeight */
    if(!AssignValue(gameConfig, WCFG_CONFIG_SEC, WCFG_CONFIG_SCR_H, wGameCfg.screenHeight))
        return false;
    else
    {
        if(wGameCfg.screenHeight < 600)
        {
            wGameCfg.screenHeight = 600;
        }
    }

    PRINTSEC(WCFG_VOL_SEC);
    /* MusicVol */
    if(!AssignValue(gameConfig, WCFG_VOL_SEC, WCFG_VOL_MUSIC, wGameCfg.musicVol))
        return false;
    else
    {
        if(wGameCfg.musicVol < 0 || wGameCfg.musicVol > 100)
        {
            wGameCfg.musicVol = 100;
        }
    }
    /* EffectSoundVol */
    if(!AssignValue(gameConfig, WCFG_VOL_SEC, WCFG_VOL_EFFECT, wGameCfg.effectVol))
        return false;
    else
    {
        if(wGameCfg.effectVol < 0 || wGameCfg.effectVol > 100)
        {
            wGameCfg.effectVol = 100;
        }
    }

    PRINTSEC(WCFG_SHOW_SEC);
    /* Showtime speedup */
    if(!AssignValue(gameConfig, WCFG_SHOW_SEC, WCFG_SHOW_SPEEDUP, showtimeConfig[SHOWTIME_SPEEDUP])) return false;
    /* Showtime speeddown */
    if(!AssignValue(gameConfig, WCFG_SHOW_SEC, WCFG_SHOW_SPEEDDOWN, showtimeConfig[SHOWTIME_SPEEDDOWN])) return false;
    /* Showtime rotate */
    if(!AssignValue(gameConfig, WCFG_SHOW_SEC, WCFG_SHOW_ROTATE, showtimeConfig[SHOWTIME_ROTATE])) return false;
    /* Showtime hidden */
    if(!AssignValue(gameConfig, WCFG_SHOW_SEC, WCFG_SHOW_HIDDEN, showtimeConfig[SHOWTIME_HIDDEN])) return false;
    /* Showtime reverse */
    if(!AssignValue(gameConfig, WCFG_SHOW_SEC, WCFG_SHOW_REVERSE, showtimeConfig[SHOWTIME_REVERSE])) return false;
    /* Check value */
    for(int i = 0; i < SHOWTIME_TOTAL; i++)
    {
        if(showtimeConfig[i] < 0 || showtimeConfig[i] > 5)
        {
            showtimeConfig[i] = 1;
        }
    }
    return true;
}

template <class TARGETVAL> void SaveValue(ALLEGRO_CONFIG*& gameConfig, const char* section, const char* keyword, TARGETVAL& targetValue)
{
    char tmpStr[STRBUFLEN];
    snprintf(tmpStr, sizeof(tmpStr), "%d", targetValue);
    al_set_config_value(gameConfig, section, keyword, tmpStr);
}

W_GAMECONFIG ConfigProvider::GetConfig()
{
    return this->wGameCfg;
}

W_KEYCFG ConfigProvider::GetKeymap()
{
    return this->wKeyConfig;
}

int32_t* ConfigProvider::GetShowtime()
{
    return this->showtimeConfig;
}

bool ConfigProvider::SaveFile()
{
    if(gameConfig != nullptr)
    {
        al_add_config_section(gameConfig,WCFG_KEYMAP_SEC);
        SaveValue(gameConfig, WCFG_KEYMAP_SEC, WCFG_KEYUP1, this->wKeyConfig.wKeyUp_1);
        SaveValue(gameConfig, WCFG_KEYMAP_SEC, WCFG_KEYUP2, this->wKeyConfig.wKeyUp_2);
        SaveValue(gameConfig, WCFG_KEYMAP_SEC, WCFG_KEYDOWN1, this->wKeyConfig.wKeyDown_1);
        SaveValue(gameConfig, WCFG_KEYMAP_SEC, WCFG_KEYDOWN2, this->wKeyConfig.wKeyDown_2);
        SaveValue(gameConfig, WCFG_KEYMAP_SEC, WCFG_KEYLEFT1, this->wKeyConfig.wKeyLeft_1);
        SaveValue(gameConfig, WCFG_KEYMAP_SEC, WCFG_KEYLEFT2, this->wKeyConfig.wKeyLeft_2);
        SaveValue(gameConfig, WCFG_KEYMAP_SEC, WCFG_KEYRIGHT1, this->wKeyConfig.wKeyRight_1);
        SaveValue(gameConfig, WCFG_KEYMAP_SEC, WCFG_KEYRIGHT2, this->wKeyConfig.wKeyRight_2);
        SaveValue(gameConfig, WCFG_KEYMAP_SEC, WCFG_KEYSHOW1, this->wKeyConfig.wKeyShow_1);
        SaveValue(gameConfig, WCFG_KEYMAP_SEC, WCFG_KEYSHOW2, this->wKeyConfig.wKeyShow_2);

        al_add_config_section(gameConfig, WCFG_CONFIG_SEC);
        SaveValue(gameConfig, WCFG_CONFIG_SEC, WCFG_CONFIG_OFFSET, this->wGameCfg.offsetMs);
        SaveValue(gameConfig, WCFG_CONFIG_SEC, WCFG_CONFIG_FPS, this->wGameCfg.fps);
        SaveValue(gameConfig, WCFG_CONFIG_SEC, WCFG_CONFIG_DIM, this->wGameCfg.bgDim);
        SaveValue(gameConfig, WCFG_CONFIG_SEC, WCFG_CONFIG_SCR_W, this->wGameCfg.screenWidth);
        SaveValue(gameConfig, WCFG_CONFIG_SEC, WCFG_CONFIG_SCR_H, this->wGameCfg.screenHeight);

        al_add_config_section(gameConfig, WCFG_VOL_SEC);
        SaveValue(gameConfig, WCFG_VOL_SEC, WCFG_VOL_MUSIC, this->wGameCfg.musicVol);
        SaveValue(gameConfig, WCFG_VOL_SEC, WCFG_VOL_EFFECT, this->wGameCfg.effectVol);

        al_add_config_section(gameConfig, WCFG_SHOW_SEC);
        SaveValue(gameConfig, WCFG_SHOW_SEC, WCFG_SHOW_SPEEDUP, this->showtimeConfig[SHOWTIME_SPEEDUP]);
        SaveValue(gameConfig, WCFG_SHOW_SEC, WCFG_SHOW_SPEEDDOWN, this->showtimeConfig[SHOWTIME_SPEEDDOWN]);
        SaveValue(gameConfig, WCFG_SHOW_SEC, WCFG_SHOW_ROTATE, this->showtimeConfig[SHOWTIME_ROTATE]);
        SaveValue(gameConfig, WCFG_SHOW_SEC, WCFG_SHOW_HIDDEN, this->showtimeConfig[SHOWTIME_HIDDEN]);
        SaveValue(gameConfig, WCFG_SHOW_SEC, WCFG_SHOW_REVERSE, this->showtimeConfig[SHOWTIME_REVERSE]);
    }
    return al_save_config_file(this->cfgFileName.c_str(), this->gameConfig);
}

bool ConfigProvider::SaveKeymap(W_KEYCFG& keymap)
{
    this->wKeyConfig = keymap;
    return true;
}

bool ConfigProvider::SaveConfig(W_GAMECONFIG& cfg)
{
    this->wGameCfg = cfg;
    (this->wGameCfg.fps == 0)?(this->wGameCfg.vsyncOption = VSYNC_ON):(this->wGameCfg.vsyncOption = VSYNC_OFF);
    return true;
}

bool ConfigProvider::SaveShowtime(int32_t showtimecfg[])
{
    memcpy(this->showtimeConfig, showtimecfg, sizeof(int32_t)*SHOWTIME_TOTAL);
    return true;
}

void ConfigProvider::ResetVSync(int32_t vsyncOpt)
{
    if(wGameCfg.vsyncOption != vsyncOpt)
    {
        if((vsyncOpt == VSYNC_OFF) && (wGameCfg.fps == 0))
        {
            wGameCfg.fps = 60;
        }
        wGameCfg.vsyncOption = vsyncOpt;
    }
}

void ConfigProvider::GenDefaultCfg()
{
    wGameCfg.fps = 60;
    wGameCfg.offsetMs = 0;
    wGameCfg.bgDim = 50;
    wGameCfg.screenWidth = 800;
    wGameCfg.screenHeight = 600;
    wGameCfg.musicVol = 100;
    wGameCfg.effectVol = 100;
    wGameCfg.vsyncOption = VSYNC_OFF;
    for(uint8_t i = 0; i < SHOWTIME_TOTAL; i++)
        showtimeConfig[i] = 1;
    wKeyConfig.wKeyUp_1 = ALLEGRO_KEY_UP;
    wKeyConfig.wKeyUp_2 = ALLEGRO_KEY_PAD_8;
    wKeyConfig.wKeyDown_1 = ALLEGRO_KEY_DOWN;
    wKeyConfig.wKeyDown_2 = ALLEGRO_KEY_PAD_2;
    wKeyConfig.wKeyLeft_1 = ALLEGRO_KEY_LEFT;
    wKeyConfig.wKeyLeft_2 = ALLEGRO_KEY_PAD_4;
    wKeyConfig.wKeyRight_1 = ALLEGRO_KEY_RIGHT;
    wKeyConfig.wKeyRight_2 = ALLEGRO_KEY_PAD_6;
    wKeyConfig.wKeyShow_1 = ALLEGRO_KEY_RCTRL;
    wKeyConfig.wKeyShow_2 = ALLEGRO_KEY_PAD_5;
}
