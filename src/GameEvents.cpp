#include "GameEvents.hpp"

int GameEvents::EventIDToIdx(int event_id)
{
    switch(event_id)
    {
#define XX(evid) case W_EVENT_ID_##evid: return W_EVENT_##evid;
    W_EVENTS_MAP(XX)
#undef XX
    default:
        return W_EVENTS_MAX;
    }
}

GameEvents::GameEvents()
{
    for(int i = 0; i < W_EVENTS_TOTAL; i++)
    {
        al_init_user_event_source(&eventlist[i].ev_src);
    }
}

GameEvents::~GameEvents()
{
    for(int i = 0; i < W_EVENTS_TOTAL; i++)
    {
        al_destroy_user_event_source(&eventlist[i].ev_src);
    }
}

ALLEGRO_EVENT_SOURCE* GameEvents::GetEventSource(int event_id)
{
    return &eventlist[EventIDToIdx(event_id)].ev_src;
}

bool GameEvents::EmitEvent(int event_id, intptr_t data1, intptr_t data2, intptr_t data3, intptr_t data4)
{
    eventlist[EventIDToIdx(event_id)].ev.type = event_id;
    eventlist[EventIDToIdx(event_id)].ev.user.data1 = data1;
    eventlist[EventIDToIdx(event_id)].ev.user.data2 = data2;
    eventlist[EventIDToIdx(event_id)].ev.user.data3 = data3;
    eventlist[EventIDToIdx(event_id)].ev.user.data4 = data4;
    return al_emit_user_event(&eventlist[EventIDToIdx(event_id)].ev_src, &eventlist[EventIDToIdx(event_id)].ev, nullptr);
}
