#ifndef _NOTEINFO_H_INCLUDED_
#define _NOTEINFO_H_INCLUDED_

#include <stdint.h>
#define RESERVED_SPACE 640
#define RESERVED_BAR_SPACE 96
/* Note type, single hit, hold and keep hitting */
typedef enum __NTYPE_
{
    NTYPE_HIT,
    NTYPE_HOLD,
    NTYPE_KHIT,
    NTYPE_HOLD_START,
    NTYPE_HOLD_KEEP,
    NTYPE_KHIT_START,
    NTYPE_KHIT_KEEP,
    NTYPE_TOTAL
}NOTE_TYPE;

/* Arrow type, now we only have 4 arrows, maybe I will add 8 arrows in future? */
typedef enum __nArrow
{
    NA_UP,
    NA_DOWN,
    NA_LEFT,
    NA_RIGHT,
    NA_TOTAL  //For variable initial.
}NOTE_ARROW;

/* Note link-list, record each notes' status and information. */
typedef struct __nl //note list
{
    NOTE_TYPE noteType;
    NOTE_ARROW noteArrow;
    uint32_t noteId;
    double noteTime;
    double noteTime_end;
    double notePosition;
    double notePosition_end;
    int khitDrawCount;  // Record it will draw faster. (Because drawer don't need calculate it.)
    int hitCountTarget;
    double noteBpm;
    double noteSpeed;
    bool active;   // If the note is out of judge area, set it inactive.
    bool visible;  // If the note id out of screen, don't draw it.
    bool hitted;   // Record if player hit the note.
    bool isInJudgeArea;  // If the note is in judge area, set it to true.
    bool isSingleInLong; // If we have single note in long note, record it.
    bool isFixedArrow;
    __nl* nextNote;
    __nl* prevNote;
}NOTES_LINKLIST;

typedef struct __nb
{
    NOTES_LINKLIST *notes;
    NOTES_LINKLIST *last_note;
    double barTime;
    double barTime_end;
    double barPosition;
    double barPosition_end;
    double barBpm;
    double barSpeed;
    bool visible;
    bool active;
    __nb *nextBar;
    __nb *prevBar;
}NOTES_BARLIST;

void NoteInit(NOTES_LINKLIST*& notes);
void NoteArrowReset(NOTES_LINKLIST*& notes, double beginAt);
void NoteReset(NOTES_LINKLIST*& notes);
void NoteClean(NOTES_LINKLIST*& notes);
void InsertNote(NOTES_LINKLIST**& nowNotes, double noteTime, double noteTime_end, NOTE_TYPE nType, NOTE_ARROW nArrow, double noteBpm, double noteSpeed, bool isFixedArrow, uint32_t noteId, int khitDrawCount = 0, int hitCountTarget = 0);

void BarInit(NOTES_BARLIST*& bars);
void BarArrowReset(NOTES_BARLIST*& bars, double beginAt);
void BarReset(NOTES_BARLIST*& bars);
void BarClean(NOTES_BARLIST*& bars);
void InsertBar(NOTES_BARLIST**& nowbars, double noteTime, double noteBpm, double noteSpeed);


inline NOTE_ARROW GenArrow()
{
    return (NOTE_ARROW)(rand()%NA_TOTAL);
}

#endif // _NOTEINFO_H_INCLUDED_
