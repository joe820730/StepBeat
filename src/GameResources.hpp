#ifndef RESOURCES_H_INCLUDED
#define RESOURCES_H_INCLUDED
#include "NoteInfo.hpp"
#include "globalDef.hpp"
#include "Allegro_include.hpp"
#include "BitmapRes.hpp"
#define SHOWTIMEBARMAX 20
#define HITTEDFRAMEMAX 10
#define HOLDEFFECTFRAME 4

enum
{
    WAB_ARROW,
    WAB_BG,
    WAB_HITTED,
    WAB_TOTAL
};

typedef enum
{
    BITMAPALIGN_LEFT,
    BITMAPALIGN_CENTER,
    BITMAPALIGN_RIGHT
}BITMAPNUM_DRAWALIGN;

enum
{
   TITLE_FONT,
   CONTENT_FONT,
   SUBCONTENT_FONT,
   DEBUG_FONT,
   FONT_TOTAL
};

enum
{
    WR_LOGO,
    WR_BG,
    WR_NOCOVER,
    WR_JUDGEAREA,
    WR_TRACKBG,
    WR_SHOWTIMEBG,
    WR_HITTEXT,
    WR_HITNUMSLASH,
    WR_HITNUMX,
    WR_HOLDTEXT,
    WR_COMBOTEXT,
    WR_TITLE_BAR,
    WR_SCORE_BAR,
    WR_INFO_BAR,
    WR_POPUP_BG,
    WR_TOTAL
};

enum
{
    WRG_REVERSEBG,
    WRG_SHOWOPT,
    WRG_AUTOOPT,
    WRG_HITNUM,
    WRG_COMBONUM,
    WRG_SCORENUM,
    WRG_SHOWTIMEIMG,
    WRG_SHOWTIMETEXT,
    WRG_DIFFBG,
    WRG_SHOWTIMEBAR,
    WRG_HOLDFRAME,
    WRG_JUDGETEXT,
    WRG_READYGO,
    WRG_TOTAL
};

enum
{
    WR9_ITEM,
    WR9_SELECTED,
    WR9_INFO_BLOCK,
    WR9_BUTTON,
    WR9_BUTTON_SELECT,
    WR9_TOTAL
};

enum
{
    WS_HIT,
    WS_PRESS,
    WS_SHOWTIME,
    WS_TOTAL
};

class GameResources
{
public:
    GameResources();
    ~GameResources(){};
    BitmapRes bitmaps[WR_TOTAL];
    GroupedBitmap bmpGroups[WRG_TOTAL];
    NineSliceBitmap nineSlices[WR9_TOTAL];
    GroupedBitmap arrows[NA_TOTAL][WAB_TOTAL];
    ALLEGRO_SAMPLE* wSounds[WS_TOTAL];
    ALLEGRO_FONT* gameFonts[FONT_TOTAL];
    bool LoadRes();
    void Resize(int screenH);
    void DestroyRes();
    bool LoadFont(int screenHeight);
    void DestroyFont();
    bool ReloadFont();
private:
    bool isResLoaded;
};

extern GameResources* wRes;
extern void DrawBitmap(ALLEGRO_BITMAP* alBitmap, double xLoc, double yLoc, double width = 0, double height = 0);
extern void DrawScaledBitmap(ALLEGRO_BITMAP* alBitmap, double xLoc, double yLoc, double scale = 1);
extern void DrawScaledTintedBitmap(ALLEGRO_BITMAP* alBitmap, double xLoc, double yLoc, double scale, ALLEGRO_COLOR tint);
extern void DrawScaledRotatedBitmap(ALLEGRO_BITMAP* alBitmap, double xLoc, double yLoc, double scale, double angle);
extern void DrawScaledPartBitmap(ALLEGRO_BITMAP* alBitmap, double xLoc, double yLoc, double sx, double sy, double sw, double sh, double xscale, double yscale);
extern void DrawBitmapNum(int fontGroupEnum, int targetNum, double xLoc, double yLoc, int numAlign, int numLen, bool isFillwithZero, double scaleRate = 1.0);
extern void DrawLevel(double xLoc, double yLoc, int diff, uint8_t level, bool isSelected);

#endif // RESOURCES_H_INCLUDED
