#include <iostream>
#include <cstdio>
#include <cstdlib>
#include "BitmapRes.hpp"
#include "ConfigProvider.hpp"

using namespace std;
BitmapRes::BitmapRes()
{
    bitmapres = nullptr;
    bitmapw = 0;
    bitmaph = 0;
    scaleRate_ = 0;
    sourcew = 0;
    sourceh = 0;
    centerx = 0;
    centery = 0;
    isLoaded = false;
}

int BitmapRes::LoadRes(const char* resPath)
{
    bitmapres = al_load_bitmap(resPath);
    if(bitmapres == nullptr)
    {
        al_show_native_message_box(nullptr, "ERROR!", "Resources missing! File:", resPath, "OK", 0);
        printf("%s is missing! Please check it.\n", resPath);
        return 1;
    }
    else
    {
        sourcew = al_get_bitmap_width(bitmapres);
        sourceh = al_get_bitmap_height(bitmapres);
        centerx = sourcew / 2.0;
        centery = sourceh / 2.0;
        isLoaded = true;
        return 0;
    }
}

int BitmapRes::LoadRes(ALLEGRO_BITMAP* srcBmp, int sx, int sy, int width, int height)
{
    if(srcBmp == nullptr)
    {
        al_show_native_message_box(nullptr, "ERROR!", "Create sub bitmap failed!", "", "OK", 0);
        return 1;
    }
    else
    {
        bitmapres = al_create_sub_bitmap(srcBmp, sx, sy, width, height);
        sourcew = al_get_bitmap_width(bitmapres);
        sourceh = al_get_bitmap_height(bitmapres);
        centerx = sourcew / 2.0;
        centery = sourceh / 2.0;
        isLoaded = true;
        return 0;
    }
}

void BitmapRes::ResizeRes(int screenH, double scale)
{
    if(bitmapres != nullptr)
    {
        if(scale == 0)
            scale = sourceh / wAvailableOption.resolution[0].screenHeight;
        bitmaph = screenH * scale;
        scaleRate_ = bitmaph / sourceh;
        bitmapw = (double)sourcew * scaleRate_;
    }
}

void BitmapRes::Draw(double xloc, double yloc)
{
    al_draw_scaled_bitmap(bitmapres, 0, 0, sourcew, sourceh, xloc, yloc, bitmapw, bitmaph, 0);
}

void BitmapRes::DrawScaled(double xloc, double yloc, double xsize, double ysize)
{
    double newxsize = bitmapw, newysize = bitmaph;
    if(xsize != 0)
        newxsize = xsize;
    if(ysize != 0)
        newysize = ysize;
    al_draw_scaled_bitmap(bitmapres, 0, 0, sourcew, sourceh, xloc, yloc, newxsize, newysize, 0);
}

void BitmapRes::DrawRotate(double xloc, double yloc, double angle)
{
    al_draw_scaled_rotated_bitmap(bitmapres, centerx, centery, xloc+centerx*scaleRate_, yloc+centery*scaleRate_, scaleRate_, scaleRate_, angle, 0);
}

void BitmapRes::DrawTinted(double xloc, double yloc, ALLEGRO_COLOR tint)
{
    al_draw_tinted_scaled_bitmap(bitmapres, tint, 0, 0, sourcew, sourceh, xloc, yloc, bitmapw, bitmaph, 0);
}

void BitmapRes::DestroyRes()
{
    al_destroy_bitmap(bitmapres);
}

NineSliceBitmap::NineSliceBitmap()
{
    uph = 0;
    lowh = 0;
    leftw = 0;
    rightw = 0;
    centerw = 0;
    centerh = 0;
    for(int i = 0; i < 9; i++)
        slicedBmp[i] = nullptr;
}

void NineSliceBitmap::SetSlicePoint(int x1, int x2, int y1, int y2)
{
    uph = y1;
    lowh = sourceh - y2;
    leftw = x1;
    rightw = sourcew - x2;
    centerw = x2 - x1;
    centerh = y2 - y1;
    slicedBmp[UPLEFT] =         al_create_sub_bitmap(bitmapres, 0,  0, leftw, uph);
    slicedBmp[UPCENTER] =       al_create_sub_bitmap(bitmapres, x1, 0, centerw, uph);
    slicedBmp[UPRIGHT] =        al_create_sub_bitmap(bitmapres, x2, 0, rightw, uph);
    slicedBmp[CENTERLEFT] =     al_create_sub_bitmap(bitmapres, 0,  y1, x1, centerh);
    slicedBmp[CENTER] =         al_create_sub_bitmap(bitmapres, x1, y1, centerw, centerh);
    slicedBmp[CENTERRIGHT] =    al_create_sub_bitmap(bitmapres, x2, y1, rightw, centerh);
    slicedBmp[LOWLEFT] =        al_create_sub_bitmap(bitmapres, 0,  y2, x1, lowh);
    slicedBmp[LOWCENTER] =      al_create_sub_bitmap(bitmapres, x1, y2, centerw, lowh);
    slicedBmp[LOWRIGHT] =       al_create_sub_bitmap(bitmapres, x2, y2, rightw, lowh);
}

void NineSliceBitmap::DrawScaled(double xloc, double yloc, double xsize, double ysize)
{
    double centerxsize = xsize - leftw - rightw;
    double centerysize = ysize - uph - lowh;
    al_draw_bitmap(slicedBmp[UPLEFT], xloc, yloc, 0);
    al_draw_bitmap(slicedBmp[UPRIGHT], xloc+leftw+centerxsize, yloc, 0);
    al_draw_bitmap(slicedBmp[LOWLEFT], xloc, yloc+uph+centerysize, 0);
    al_draw_bitmap(slicedBmp[LOWRIGHT], xloc+leftw+centerxsize, yloc+uph+centerysize, 0);
    al_draw_scaled_bitmap(slicedBmp[UPCENTER], 0, 0, centerw, uph, xloc+leftw, yloc, centerxsize, uph, 0);
    al_draw_scaled_bitmap(slicedBmp[CENTERLEFT], 0, 0, leftw, centerh, xloc, yloc+uph, leftw, centerysize, 0);
    al_draw_scaled_bitmap(slicedBmp[CENTER], 0, 0, centerw, centerh, xloc+leftw, yloc+uph, centerxsize, centerysize, 0);
    al_draw_scaled_bitmap(slicedBmp[CENTERRIGHT], 0, 0, rightw, centerh, xloc+leftw+centerxsize, yloc+uph, rightw, centerysize, 0);
    al_draw_scaled_bitmap(slicedBmp[LOWCENTER], 0, 0, centerw, lowh, xloc+leftw, yloc+uph+centerysize, centerxsize, lowh, 0);
}

void NineSliceBitmap::DestroyRes()
{
    for(int i = 0; i < 9; i++)
        al_destroy_bitmap(slicedBmp[i]);
    al_destroy_bitmap(bitmapres);
}
