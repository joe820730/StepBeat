#ifndef MENUOBJ_HPP_INCLUDED
#define MENUOBJ_HPP_INCLUDED

#include "Allegro_include.hpp"
#include "globalDef.hpp"
#include "MenuInterface.hpp"
#include "ConfigMenu.hpp"

#define MAINMENUX       0.5
#define MAINMENUY       0.5

class MainMenu : public MenuInterface
{
public:
    MainMenu();
    ~MainMenu();
    bool Setup(ConfigProvider* cfg_provider);
    void GetFontHeight() override;
    void MenuCtrl(int mouseX, int mouseY) override;
    void MenuCtrl(int keyCode) override;
    void MenuEnter();
    void Draw(double deltaTime) override;
private:
    enum
    {
        W_MENU_SELECT_MUSIC,
        W_MENU_CONFIG,
        W_MENU_RELOAD,
        W_MENU_EXIT,
        W_MENU_TOTAL
    };
};

extern MainMenu* gMAINMENU;
#endif
