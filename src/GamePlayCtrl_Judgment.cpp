#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <thread>

#include "globalDef.hpp"
#include "GamePlayCtrl.hpp"
#include "NoteInfo.hpp"
#include "ConfigProvider.hpp"

using namespace std;

const int kJudgeToScore[RESULT_MISS+1]
{
    1000,
    700,
    400,
    0,
    0
};

void GamePlayCtrl::AddJudge(uint8_t judgeType, NOTES_LINKLIST* curNote)
{
    double combo_scoremultiplier = 1.0;
    switch(judgeType)
    {
        case RESULT_PERFECT:
        case RESULT_GREAT:
        case RESULT_COOL:
        {
            if(nowCombo <= 100)
                combo_scoremultiplier += (nowCombo/10)*0.1;
            else
                combo_scoremultiplier = 2.0;
            nowCombo++;
            comboTextScale = 0.1;
            if(nowCombo > scoreRecord[RESULT_COMBO])
                scoreRecord[RESULT_COMBO] = nowCombo;
            scoreRecord[RESULT_SCORE] += kJudgeToScore[judgeType]*showtime_scoremultiplier*combo_scoremultiplier;
            scoreRecord[judgeType]++;
            if(judgeType == RESULT_GREAT)
                maxAccuracy -= 0.3;
            else if(judgeType == RESULT_COOL)
                maxAccuracy -= 0.6;
            if(enableShowTime)
            {
                if(!isShowTime && showtimegather < showtimegatherMax)
                {
                    showtimegather++;
                    if(showtimegather == showtimegatherMax)
                        al_play_sample(wRes->wSounds[WS_SHOWTIME], (double)gameCfg.effectVol/100.0, 0, 1, ALLEGRO_PLAYMODE_ONCE, nullptr);
                }
            }
            latestJudge = judgeType;
            break;
        }
        case RESULT_BAD:
        case RESULT_MISS:
        {
            scoreRecord[judgeType]++;
            nowCombo = 0;
            maxAccuracy -= 1;
            if(enableShowTime)
            {
                if(!isShowTime)
                {
                    if(showtimegather > 0)
                        showtimegather--;
                }
            }
            latestJudge = judgeType;
            break;
        }
        case RESULT_HOLD_MISS:  // Hold note need consider head and tail, so write an exception here.
        {
            scoreRecord[RESULT_MISS]+=2;
            if(nowCombo > scoreRecord[RESULT_COMBO])
                scoreRecord[RESULT_COMBO] = nowCombo;
            nowCombo = 0;
            maxAccuracy -= 2;
            if(enableShowTime)
            {
                if(isShowTime)
                {
                    if(showtimegather > 0 && curNote->noteId >= showtime_StartId)
                    {
                        showtimegather-=2;
                        if(showtimegather < 0)
                            showtimegather = 0;
                    }
                }
                else
                {
                    if(showtimegather > 0)
                    {
                        showtimegather-=2;
                        if(showtimegather < 0)
                            showtimegather = 0;
                    }
                }
            }
            latestJudge = RESULT_MISS;
            break;
        }
        default:
            break;
    }
    if(enableShowTime)
    {
        if(isShowTime)
        {
            if(showtimegather > 0 && curNote->noteId >= showtime_StartId)
                showtimegather--;
        }
    }
    if(maxNotes != 0)
    {
        accuracy = 100*(maxAccuracy/(double)maxNotes);
    }
    judgeFrameTime = 0;
}

void GamePlayCtrl::FindJudgeNote(double nowTime)
{
    NOTES_BARLIST* nowbarPtr = notes_bar;
    while(nowbarPtr != nullptr)
    {
        NOTES_LINKLIST* nowPtr = nowbarPtr->notes;
        while(nowPtr != nullptr)
        {
            if(nowPtr->noteTime-nowTime < 1000)
            {
                if(nowPtr->noteType == NTYPE_HIT)
                {
                    if(fabs(nowPtr->noteTime - nowTime) <= JUDGE_BAD)
                    {
                        nowPtr->isInJudgeArea = true;
                    }
                    else if(nowPtr->noteTime < (nowTime + JUDGE_BAD))
                    {
                        if(!nowPtr->hitted)
                        {
                            if(nowPtr->active)
                            {
                                AddJudge(RESULT_MISS, nowPtr);
                                nowPtr->active = false;
                                nowPtr->isInJudgeArea = false;
                            }
                        }
                    }
                }
                else if(nowPtr->noteType == NTYPE_KHIT)
                {
                    if(fabs(nowPtr->noteTime - nowTime) <= JUDGE_BAD && nowPtr->active)
                    {
                        nowPtr->isInJudgeArea = true;
                        judgeNote_keep = nowPtr;
                        hitCountTarget = nowPtr->hitCountTarget;
                        isHitting = true;
                    }
                    else if(nowPtr->noteTime_end >= nowTime && nowPtr->noteTime < (nowTime + JUDGE_BAD) && nowPtr->active)
                    {
                        judgeNote_keep = nowPtr;
                        isHitting = true;
                    }
                    else if(nowPtr->noteTime_end < nowTime)
                    {
                        if(nowPtr->active)
                        {
                            nowPtr->active = false;
                            nowPtr->isInJudgeArea = false;
                            HittingNoteJudge(judgeNote_keep);
                            judgeNote_keep = nullptr;
                        }
                    }
                }
                else if(nowPtr->noteType == NTYPE_HOLD)
                {
                    if(fabs(nowPtr->noteTime - nowTime) <= JUDGE_BAD)
                    {
                        nowPtr->isInJudgeArea = true;
                        judgeNote_long[nowPtr->noteArrow] = nowPtr;
                    }
                    else if((nowPtr->noteTime_end + JUDGE_BAD) >= nowTime && nowPtr->noteTime < (nowTime + JUDGE_BAD))
                    {
                        if(nowPtr->hitted)
                        {
                            judgeNote_long[nowPtr->noteArrow] = nowPtr;
                        }
                        else
                        {
                            if(nowPtr->active)
                            {
                                AddJudge(RESULT_HOLD_MISS, nowPtr);
                                nowPtr->active = false;
                                nowPtr->isInJudgeArea = false;
                                PressingNoteJudge(nowPtr);
                                judgeNote_long[nowPtr->noteArrow] = nullptr;
                            }
                        }
                    }
                    else if(nowPtr->noteTime_end < (nowTime + JUDGE_BAD))
                    {
                        if(nowPtr->active)
                        {
                            nowPtr->active = false;
                            nowPtr->isInJudgeArea = false;
                            PressingNoteJudge(nowPtr);
                            judgeNote_long[nowPtr->noteArrow] = nullptr;
                            if(!nowPtr->hitted)
                            {
                                AddJudge(RESULT_HOLD_MISS, nowPtr);
                            }
                        }
                    }
                }
            }
            else
                break;
            nowPtr = nowPtr->nextNote;
        }
        nowbarPtr = nowbarPtr->nextBar;
    }
}

void GamePlayCtrl::NoteJudge(double judgeTime, NOTES_LINKLIST* nowPtr)
{
    if(judgeTime <= JUDGE_PERFECT)
    {
        AddJudge(RESULT_PERFECT, nowPtr);
    }
    else if(judgeTime <= JUDGE_GREAT)
    {
        AddJudge(RESULT_GREAT, nowPtr);
    }
    else if(judgeTime <= JUDGE_COOL)
    {
        AddJudge(RESULT_COOL, nowPtr);
    }
    else if(judgeTime <= JUDGE_BAD)
    {
        AddJudge(RESULT_BAD, nowPtr);
    }
    else
    {
        for(int i = 0; i < NA_TOTAL; i++)
        {
            if(judgeNote_long[i] != nullptr && judgeTime > JUDGE_BAD)
            {
                AddJudge(RESULT_MISS, judgeNote_long[i]);
            }
        }
    }
    if(nowPtr->noteType == NTYPE_HIT)
    {
        nowPtr->visible = false;
        nowPtr->active = false;
        nowPtr->isInJudgeArea = false;
    }
    nowPtr->hitted = true;
}

void GamePlayCtrl::PressingNoteJudge(NOTES_LINKLIST* holdNotePtr)
{
    if(!holdNotePtr)
        return;
    if(holdNotePtr->noteType != NTYPE_HOLD)
        return;
    if(isPressing[holdNotePtr->noteArrow])
    {
        al_stop_sample_instance(pressSound);
        isPressing[holdNotePtr->noteArrow] = false;
        AddJudge(RESULT_MISS, holdNotePtr);
    }
}

void GamePlayCtrl::HittingNoteJudge(NOTES_LINKLIST* hitNotePtr)
{
    if(!hitNotePtr)
        return;
    if(hitNotePtr->noteType != NTYPE_KHIT)
        return;
    if(isHitting)
    {
        if(hitCount > 0 && hitCount < hitCountTarget)
        {
            AddJudge(RESULT_GREAT, hitNotePtr);
        }
        else if(hitCount >= hitCountTarget)
        {
            AddJudge(RESULT_PERFECT, hitNotePtr);
        }
        else
        {
            AddJudge(RESULT_MISS, hitNotePtr);
        }
        hitCount = 0;
        isHitting = false;
    }
}

void GamePlayCtrl::DrawJudgment(double deltaTime)
{
    if(judgeFrameTime < 200)
    {
        if(latestJudge <= RESULT_MISS)
        {
            double xLoc = note_judge_area + (wRes->arrows[0][WAB_ARROW].GetScaledWidth() - wRes->bmpGroups[WRG_JUDGETEXT].GetScaledWidth())/2.0;
            wRes->bmpGroups[WRG_JUDGETEXT].Draw(xLoc, gameCfg.screenHeight*JUDGETEXTY-(judgeFrameTime*0.1*gScaleRate), latestJudge);
        }
        judgeFrameTime+=deltaTime;
    }
    if(nowCombo >= 2)
    {
        wRes->bitmaps[WR_COMBOTEXT].Draw(gameCfg.screenWidth*gTHEMEMGR->theme.comboTextX, gameCfg.screenHeight*gTHEMEMGR->theme.comboTextY);
        double offset = wRes->bitmaps[WR_COMBOTEXT].GetScaledWidth() + gameCfg.screenWidth*gTHEMEMGR->theme.comboTextX;
        DrawBitmapNum(WRG_COMBONUM, nowCombo, offset,
                      gameCfg.screenHeight*gTHEMEMGR->theme.comboTextY, BITMAPALIGN_LEFT,3,false, (1.0+comboTextScale));
    }
    if(comboTextScale > 0)
    {
        comboTextScale -= (deltaTime/1000.0);
    }
    DrawBitmapNum(WRG_SCORENUM, gGAMEPLAYCTRL->scoreRecord[RESULT_SCORE], gameCfg.screenWidth*0.5, gameCfg.screenHeight*0.01, BITMAPALIGN_CENTER, 8, true, (1.0+comboTextScale));
}

void GamePlayCtrl::ShowStatistic()
{
    double textlocY = gameCfg.screenHeight * 0.12;
    al_draw_textf( wRes->gameFonts[CONTENT_FONT], al_map_rgb(255, 255, 255), 20,textlocY, ALLEGRO_ALIGN_LEFT, "Perfect: \t%d", scoreRecord[RESULT_PERFECT]);
    textlocY += al_get_font_ascent(wRes->gameFonts[CONTENT_FONT]);
    al_draw_textf( wRes->gameFonts[CONTENT_FONT], al_map_rgb(255, 255, 255), 20,textlocY, ALLEGRO_ALIGN_LEFT, "Great: \t\t\t%d", scoreRecord[RESULT_GREAT]);
    textlocY += al_get_font_ascent(wRes->gameFonts[CONTENT_FONT]);
    al_draw_textf( wRes->gameFonts[CONTENT_FONT], al_map_rgb(255, 255, 255), 20,textlocY, ALLEGRO_ALIGN_LEFT, "Cool: \t\t\t\t%d", scoreRecord[RESULT_COOL]);
    textlocY += al_get_font_ascent(wRes->gameFonts[CONTENT_FONT]);
    al_draw_textf( wRes->gameFonts[CONTENT_FONT], al_map_rgb(255, 255, 255), 20,textlocY, ALLEGRO_ALIGN_LEFT, "Bad: \t\t\t\t\t%d", scoreRecord[RESULT_BAD]);
    textlocY += al_get_font_ascent(wRes->gameFonts[CONTENT_FONT]);
    al_draw_textf( wRes->gameFonts[CONTENT_FONT], al_map_rgb(255, 255, 255), 20,textlocY, ALLEGRO_ALIGN_LEFT, "Miss: \t\t\t\t%d", scoreRecord[RESULT_MISS]);
    textlocY += al_get_font_ascent(wRes->gameFonts[CONTENT_FONT]);
    al_draw_textf( wRes->gameFonts[CONTENT_FONT], al_map_rgb(255, 255, 255), 20,textlocY, ALLEGRO_ALIGN_LEFT, "Accuracy: \t%.2lf", accuracy);
}
