#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <thread>

#include "globalDef.hpp"
#include "GamePlayCtrl.hpp"
#include "NoteInfo.hpp"
#include "ConfigProvider.hpp"
#include "debug.hpp"

#define READY_LOGO_TIME (EMPTY_TIME+1000.0)
#define GO_LOGO_TIME (READY_LOGO_TIME+1000.0)

void GamePlayCtrl::Draw(double deltaTime)
{
    if(!isMapPlaying)
        return;
    if(cur_song != nullptr)
    {
        if(cur_song->cover != nullptr)
        {
            DrawBitmap(cur_song->cover, 0, 0, gameCfg.screenWidth, gameCfg.screenHeight);
        }
        else
        {
            wRes->bitmaps[WR_BG].DrawScaled(0, 0, gameCfg.screenWidth,gameCfg.screenHeight);
        }
        al_draw_filled_rectangle(0, 0, gameCfg.screenWidth, gameCfg.screenHeight,
                                 al_map_rgba_f(0, 0, 0, gameCfg.bgDim/100.0));
    }
    wRes->bitmaps[WR_SCORE_BAR].DrawScaled(0,0,gameCfg.screenWidth,0);
    wRes->bitmaps[WR_INFO_BAR].DrawScaled(0,gameCfg.screenHeight-wRes->bitmaps[WR_INFO_BAR].GetScaledHeight(),gameCfg.screenWidth,0);
    if(cur_song != nullptr)
    {
        al_draw_ustr(wRes->gameFonts[TITLE_FONT], al_map_rgb(255, 255, 255),
                     gameCfg.screenWidth*PLAYINGINFOX,
                     gameCfg.screenHeight*PLAYINGINFOY,
                     ALLEGRO_ALIGN_LEFT, cur_song->songTitleustr);
        al_draw_ustr(wRes->gameFonts[CONTENT_FONT], al_map_rgb(255, 255, 255),
                     gameCfg.screenWidth*PLAYINGINFOX,
                     gameCfg.screenHeight*PLAYINGINFOY+al_get_font_line_height(wRes->gameFonts[TITLE_FONT]),
                         ALLEGRO_ALIGN_LEFT, cur_song->songAtristustr);
        /* Draw Title and Artist. */
        if(cur_song_diff != MAP_TOTAL)
        {
            DrawLevel(gameCfg.screenWidth*PLAYINGLEVELX, gameCfg.screenHeight*PLAYINGLEVELY, cur_song_diff, cur_song->mapLevel[cur_song_diff], true);
        }
    }

    /* Draw Ready go */
    if(timers.nowTime < 0)
    {
        double ready_logo_time_tmp = timers.nowTime-READY_LOGO_TIME;
        if(ready_logo_time_tmp < 0)
        {
            double logoscale = 2-(5-(fabs(ready_logo_time_tmp))/200);
            if(logoscale < 1) logoscale = 1;
            double readyxloc = (gameCfg.screenWidth - wRes->bmpGroups[WRG_READYGO].GetScaledWidth()*logoscale)/2;
            double readyyloc = (gameCfg.screenHeight - wRes->bmpGroups[WRG_READYGO].GetScaledHeight()*logoscale)/2;
            wRes->bmpGroups[WRG_READYGO].DrawScaled(readyxloc,
                                                   readyyloc,
                                                   wRes->bmpGroups[WRG_READYGO].GetScaledWidth()*logoscale,
                                                   wRes->bmpGroups[WRG_READYGO].GetScaledHeight()*logoscale,
                                                   0);
        }
        double go_logo_time_tmp = (timers.nowTime-GO_LOGO_TIME);
        if(go_logo_time_tmp > -1000 && go_logo_time_tmp < 0)
        {
            double logoscale = 2-(5-(fabs(go_logo_time_tmp))/200);
            if(logoscale < 1) logoscale = 1;
            double readyxloc = (gameCfg.screenWidth - wRes->bmpGroups[WRG_READYGO].GetScaledWidth()*logoscale)/2;
            double readyyloc = (gameCfg.screenHeight - wRes->bmpGroups[WRG_READYGO].GetScaledHeight()*logoscale)/2;
            wRes->bmpGroups[WRG_READYGO].DrawScaled(readyxloc,
                                                   readyyloc,
                                                   wRes->bmpGroups[WRG_READYGO].GetScaledWidth()*logoscale,
                                                   wRes->bmpGroups[WRG_READYGO].GetScaledHeight()*logoscale,
                                                   1);
        }
    }

    al_draw_textf( wRes->gameFonts[TITLE_FONT], al_map_rgb(255, 255, 255),
                  gameCfg.screenWidth*PLAYINGTIMEX ,gameCfg.screenHeight*PLAYINGTIMEY, ALLEGRO_ALIGN_LEFT,
                  "%02d:%02d / %02d:%02d",
                  (int)((timers.bgmTime/1000)/60),(int)(timers.bgmTime/1000)%60,
                  (int)(timers.bgmLen/60),(int)timers.bgmLen%60);
    al_draw_textf(wRes->gameFonts[DEBUG_FONT], al_map_rgb(255,255,255),
                  gameCfg.screenWidth-5 ,gameCfg.screenHeight-al_get_font_line_height(wRes->gameFonts[DEBUG_FONT])*3, ALLEGRO_ALIGN_RIGHT,
                  "%.4lf", maphandle_cost_time);

    DrawTrackBackground();
    ShowStatistic();
    DrawJudgment(deltaTime);
    DrawNote();
    DrawHittedEffect(deltaTime);

    DrawKeepHitCount();
    if(isPaused)
    {
        wRes->nineSlices[WR9_INFO_BLOCK].DrawScaled(gameCfg.screenWidth*0.4, gameCfg.screenHeight*0.46,
                                                    gameCfg.screenWidth*0.2, gameCfg.screenHeight*0.15);
        al_draw_text(wRes->gameFonts[TITLE_FONT], al_map_rgb(255,255,255),
                     gameCfg.screenWidth*0.5, gameCfg.screenHeight*0.5,
                     ALLEGRO_ALIGN_CENTER, "Paused");
    }
}
