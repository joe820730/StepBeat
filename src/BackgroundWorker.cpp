#include "BackgroundWorker.h"
#include "globalDef.hpp"
#include "GameEvents.hpp"
#include "MusicList.hpp"
#include "GamePlayCtrl.hpp"
#include <iostream>

BackgroundWorker::BackgroundWorker():
    ev_queue(nullptr),
    isAlive(false)
{
    //ctor
}

BackgroundWorker::~BackgroundWorker()
{
    //dtor
}

bool BackgroundWorker::Init()
{
    this->ev_queue = al_create_event_queue();
    if(this->ev_queue == nullptr)
    {
        return false;
    }
    al_register_event_source(this->ev_queue, gGAMEEVENTS->GetEventSource(W_EVENT_ID_EXIT));
    al_register_event_source(this->ev_queue, gGAMEEVENTS->GetEventSource(W_EVENT_ID_SCANSONG_BEGIN));
    al_register_event_source(this->ev_queue, gGAMEEVENTS->GetEventSource(W_EVENT_ID_LOADMAP_BEGIN));
    return true;
}

bool BackgroundWorker::Start()
{
    if(!isAlive)
    {
        isAlive = true;
        this->runner = std::thread(&BackgroundWorker::Loop, this);
    }
    return true;
}

void BackgroundWorker::Loop()
{
    do
    {
        al_wait_for_event(this->ev_queue, &this->ev);
        switch(this->ev.type)
        {
            case W_EVENT_ID_SCANSONG_BEGIN:
            {
                gMUSICLIST->ScanSong(true);
                gGAMEEVENTS->EmitEvent(W_EVENT_ID_SCANSONG_END);
                break;
            }
            case W_EVENT_ID_LOADMAP_BEGIN:
            {
                gGAMEPLAYCTRL->LoadMap((W_SONGLIST*)ev.user.data1, (W_MAPDIFF)ev.user.data2);
                gGAMEEVENTS->EmitEvent(W_EVENT_ID_LOADMAP_END);
                break;
            }
            case W_EVENT_ID_EXIT:
            {
                isAlive = false;
                break;
            }
            default:
            {
                break;
            }
        }
    }
    while(isAlive);
}

void BackgroundWorker::Stop()
{
    if(this->runner.joinable())
    {
        this->runner.join();
    }
    if(this->ev_queue != nullptr)
    {
        al_destroy_event_queue(this->ev_queue);
    }
}
