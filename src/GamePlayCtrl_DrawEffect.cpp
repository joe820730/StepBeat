#include "GamePlayCtrl.hpp"

void GamePlayCtrl::DrawHittedEffect(double deltaTime)
{
    hittedQueue.DrawEffect(note_judge_area, note_y_pos, deltaTime);
    for(int i = 0; i < NA_TOTAL; i++)
    {
        if(isPressing[i])
        {
            double offset = (wRes->bmpGroups[WRG_HOLDFRAME].GetScaledWidth() - wRes->arrows[WAB_BG][0].GetScaledWidth()) / 2.0;
            holdFrameTime += deltaTime;
            if(holdFrameTime > 16)
            {
                if(holdFrameIdx < HOLDEFFECTFRAME-1)
                    holdFrameIdx++;
                else
                    holdFrameIdx = 0;
                holdFrameTime = 0;
            }
            wRes->bmpGroups[WRG_HOLDFRAME].Draw(note_judge_area-offset, note_y_pos-offset, holdFrameIdx);
            break;
        }
    }
}

void GamePlayCtrl::DrawKeepHitCount()
{
    if(judgeNote_keep == nullptr)
        return;
    wRes->bitmaps[WR_HITTEXT].Draw(gameCfg.screenWidth*HITTEXT_CNTX, gameCfg.screenHeight*HITTEXT_CNTY);
    DrawBitmapNum(WRG_HITNUM, hitCount, gameCfg.screenWidth*HITNUM_CNTX, gameCfg.screenHeight*HITNUM_CNTY, BITMAPALIGN_RIGHT, 3, false);
    wRes->bitmaps[WR_HITNUMSLASH].Draw(gameCfg.screenWidth*HITNUM_CNTX, gameCfg.screenHeight*HITNUM_CNTY);
    double numOffset = wRes->bitmaps[WR_HITNUMSLASH].GetScaledWidth();
    DrawBitmapNum(WRG_HITNUM, judgeNote_keep->hitCountTarget, gameCfg.screenWidth*HITNUM_CNTX+numOffset, gameCfg.screenHeight*HITNUM_CNTY, BITMAPALIGN_LEFT, 1, false);
}

void GamePlayCtrl::DrawTrackBackground()
{
    double judgeAreaOffsetX = gTHEMEMGR->theme.judgeAreaOffsetX * gScaleRate;
    double judgeAreaOffsetY = gTHEMEMGR->theme.judgeAreaOffsetY * gScaleRate;
    double rightOffset = wRes->bitmaps[WR_JUDGEAREA].GetScaledWidth();
    wRes->bitmaps[WR_TRACKBG].DrawScaled(0, note_y_pos-judgeAreaOffsetY, note_judge_area-judgeAreaOffsetX, 0);
    wRes->bitmaps[WR_JUDGEAREA].Draw(note_judge_area-judgeAreaOffsetX, note_y_pos-judgeAreaOffsetY);
    wRes->bitmaps[WR_TRACKBG].DrawScaled(note_judge_area+rightOffset-judgeAreaOffsetX, note_y_pos-judgeAreaOffsetY, gameCfg.screenWidth-note_judge_area-rightOffset+judgeAreaOffsetX, 0);
    if(isShowTime)
    {
        wRes->bmpGroups[WRG_SHOWTIMEIMG].Draw(note_judge_area, note_y_pos, showType);
        double textPos = gameCfg.screenHeight * SHOWTIMETEXTY;
        al_draw_text(wRes->gameFonts[CONTENT_FONT], al_map_rgb(255, 255, 255), gameCfg.screenWidth*SHOWTIMETEXTX, textPos, ALLEGRO_ALIGN_LEFT, "ShowTime");
        textPos += al_get_font_ascent(wRes->gameFonts[CONTENT_FONT]);
        al_draw_textf(wRes->gameFonts[CONTENT_FONT], al_map_rgb(255, 255, 255), gameCfg.screenWidth*SHOWTIMETEXTX, textPos, ALLEGRO_ALIGN_LEFT, "x %.2lf", showtime_scoremultiplier);
    }
    if(enableShowTime)
    {
        double barOffsetX = gTHEMEMGR->theme.gaugebaroffsetX * gScaleRate;
        double barOffsetY = gTHEMEMGR->theme.gaugebaroffsetY * gScaleRate;
        if(showtimegather > 0)
        {
            int showtimebarBitmapIdx = ((double)showtimegather/(double)showtimegatherMax)*(SHOWTIMEBARMAX-1);
            wRes->bmpGroups[WRG_SHOWTIMEBAR].Draw(note_judge_area-barOffsetX, note_y_pos-barOffsetY, showtimebarBitmapIdx);
        }
        else
            wRes->bitmaps[WR_SHOWTIMEBG].Draw(note_judge_area-barOffsetX, note_y_pos-barOffsetY);
    }
}
