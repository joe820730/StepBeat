#ifndef MAPREADER_HPP_INCLUDED
#define MAPREADER_HPP_INCLUDED
#include <string>
#include "NoteInfo.hpp"
#include "MapInfo.hpp"

#define ERRORMSGLEN 64

#define BEATLENUSUAL 4.0
#define MIN_TO_SEC 60.0
#define DEFAULT_BAR_LEN 16
class MapReader
{
public:
    MapReader();
    ~MapReader(){};
    bool ReadMap(NOTES_BARLIST*& bars, W_SONGLIST *songInfo, W_MAPDIFF targetDiff, uint32_t& totalNote, NOTES_BARLIST*& last_bar);
private:
    bool ReadMap_Old(NOTES_BARLIST*& bars, W_SONGLIST *songInfo, W_MAPDIFF targetDiff, uint32_t& totalNote, NOTES_BARLIST*& last_bar);
    bool ReadMap_New(NOTES_BARLIST*& bars, W_SONGLIST *songInfo, W_MAPDIFF targetDiff, uint32_t& totalNote, NOTES_BARLIST*& last_bar);
    bool MapLineParser(NOTES_BARLIST*& bars, W_SONGLIST *songInfo, W_MAPDIFF targetDiff, std::string& tmpLine);
    bool MapLineParser_New(NOTES_BARLIST*& bars, W_SONGLIST *songInfo, W_MAPDIFF targetDiff, std::string& tmpLine);
    void Refresh();
    bool isDiffMatch;
    bool isMapEmpty;
    bool isMapEnd;
    double nowBPM;
    double nowSpeed;
    unsigned int nowBPB; //Beats per bar
    unsigned int nowBeatLen; // Default is quarter note because it usual.
    size_t nowBarLen; //Record now bar length
    double nowInterval;
    double noteTime;
    double noteTime_start;
    double noteTime_end;
    NOTES_BARLIST** nowbarPtrPtr;
    NOTES_LINKLIST** nowPtrPtr;
    NOTES_LINKLIST* tmpLong;
    bool isHitWhenPress;
    bool isHitWhenRelease;
    NOTE_TYPE nType, nType_prev;
    NOTE_ARROW nArrow;
    uint32_t noteId;
    int nowLine; // For showing map error.
    bool isFixedArrow;
    char errormsg[ERRORMSGLEN];
};

#endif // MAPREADER_HPP_INCLUDED
