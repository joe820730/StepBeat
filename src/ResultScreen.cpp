#include <cstdio>
#include <iostream>
#include <cstdlib>
#include "ResultScreen.hpp"
#include "MapInfo.hpp"
#include "Allegro_include.hpp"
#include "GamePlayCtrl.hpp"
#include "GameResources.hpp"
#include "ConfigMenu.hpp"
#include "StateHandler.hpp"
#include "GameEvents.hpp"

ResultScreen::ResultScreen()
{
    ctrlIdx = RS_RETURN;
    input_playername = false;
    is_bonus_mode = false;
    cur_song = nullptr;
    cur_song_diff = MAP_TOTAL;
    cursor_pos = 0;
    current_rank = 0;
    resultText = new std::string[RESULT_TOTAL];
    resultText[RESULT_PERFECT] = "Perfect";
    resultText[RESULT_GREAT] = "Great";
    resultText[RESULT_COOL] = "Cool";
    resultText[RESULT_BAD] = "Bad";
    resultText[RESULT_MISS] = "Miss";
    resultText[RESULT_COMBO] = "MaxCombo";
    resultText[RESULT_SCORE] = "Score";
    resultText[RESULT_ACCURACY] = "Accuracy";
}

ResultScreen::~ResultScreen()
{
    delete [] resultText;
}

bool ResultScreen::Setup(ConfigProvider* cfg_provider)
{
    if(cfg_provider != nullptr)
    {
        wGameCfg = cfg_provider->GetConfig();
        return true;
    }
    else
    {
        return false;
    }
}

void ResultScreen::AttachCurSongDiff(W_SONGLIST* selectedSong, W_MAPDIFF selectedDiff)
{
    if(selectedSong != nullptr)
    {
        cur_song = selectedSong;
        cur_song_diff = selectedDiff;
    }
}

void ResultScreen::ScreenCtrl(int keyCode)
{
    if(!input_playername)
    {
        switch(keyCode)
        {
            case ALLEGRO_KEY_F1:
            {
                gGAMEPLAYCTRL->isAutoPlay = !gGAMEPLAYCTRL->isAutoPlay;
                break;
            }
            case ALLEGRO_KEY_F2:
            {
                gGAMEPLAYCTRL->enableShowTime = !gGAMEPLAYCTRL->enableShowTime;
                break;
            }
            case ALLEGRO_KEY_F3:
            {
                gCONFIGMENU->SwitchShowTimeConfig();
                break;
            }
            case ALLEGRO_KEY_LEFT:
            {
                ctrlIdx = RS_AGAIN;
                break;
            }
            case ALLEGRO_KEY_RIGHT:
            {
                ctrlIdx = RS_RETURN;
                break;
            }
            case ALLEGRO_KEY_ENTER:
            case ALLEGRO_KEY_PAD_ENTER:
            {
                switch(ctrlIdx)
                {
                    case RS_RETURN:
                    {
                        gSTATEHANDLER->SetState(W_SELECT_MUSIC);
                        break;
                    }
                    case RS_AGAIN:
                    {
                        gSTATEHANDLER->SetState(W_LOADMAP);
                        gGAMEEVENTS->EmitEvent(W_EVENT_ID_LOADMAP_BEGIN, (intptr_t)cur_song, (W_MAPDIFF)cur_song_diff);
                        break;
                    }
                }
                break;
            }
            case ALLEGRO_KEY_ESCAPE:
            {
                gSTATEHANDLER->SetState(W_SELECT_MUSIC);
                break;
            }
            default:
                break;
        }
    }
    else
    {
        switch(keyCode)
        {
            case ALLEGRO_KEY_PAD_ENTER:
            case ALLEGRO_KEY_ENTER:
            {
                if(cur_song_diff != MAP_TOTAL)
                {
                    if(is_bonus_mode)
                    {
                        for(int j = HISCORE_MAX - 1; j > current_rank; j--)
                        {
                            cur_song->scoredb[cur_song_diff].playername_bonus[j] = cur_song->scoredb[cur_song_diff].playername_bonus[j-1];
                            cur_song->scoredb[cur_song_diff].hiscore_bonus[j] = cur_song->scoredb[cur_song_diff].hiscore_bonus[j-1];
                            cur_song->scoredb[cur_song_diff].accuracy_bonus[j] = cur_song->scoredb[cur_song_diff].accuracy_bonus[j-1];
                        }
                        cur_song->scoredb[cur_song_diff].hiscore_bonus[current_rank] = gGAMEPLAYCTRL->scoreRecord[RESULT_SCORE];
                        cur_song->scoredb[cur_song_diff].accuracy_bonus[current_rank] = gGAMEPLAYCTRL->accuracy;
                        cur_song->scoredb[cur_song_diff].playername_bonus[current_rank] = playername_temp;
                    }
                    else
                    {
                        for(int j = HISCORE_MAX - 1; j > current_rank; j--)
                        {
                            cur_song->scoredb[cur_song_diff].playername[j] = cur_song->scoredb[cur_song_diff].playername[j-1];
                            cur_song->scoredb[cur_song_diff].hiscore[j] = cur_song->scoredb[cur_song_diff].hiscore[j-1];
                            cur_song->scoredb[cur_song_diff].accuracy[j] = cur_song->scoredb[cur_song_diff].accuracy[j-1];
                        }
                        cur_song->scoredb[cur_song_diff].playername[current_rank] = playername_temp;
                        cur_song->scoredb[cur_song_diff].hiscore[current_rank] = gGAMEPLAYCTRL->scoreRecord[RESULT_SCORE];
                        cur_song->scoredb[cur_song_diff].accuracy[current_rank] = gGAMEPLAYCTRL->accuracy;
                    }
                }
                input_playername = false;
                break;
            }
            case ALLEGRO_KEY_LEFT:
            {
                if(cursor_pos > 0)
                    cursor_pos--;
                break;
            }
            case ALLEGRO_KEY_RIGHT:
            {
                if(cursor_pos < playername_temp.length())
                    cursor_pos++;
                break;
            }
            case ALLEGRO_KEY_ESCAPE:
            {
                input_playername = false;
                break;
            }
            default:
            {
                break;
            }
        }
    }
}

void ResultScreen::InputPlayerName(int unichar)
{
    if(!input_playername)
        return;
    if((unichar >= 0x20 && unichar < 127) && playername_temp.length() < 32)
    {
        playername_temp.insert(cursor_pos, 1, (char)unichar);
        cursor_pos++;
    }
    else if(unichar == 0x08 && cursor_pos > 0)
    {
        playername_temp.erase(cursor_pos-1, 1);
        cursor_pos--;
    }
}

void ResultScreen::DrawPlayernameInputbox(double deltaTime)
{
    thread_local static double deltatime_count = 0;
    thread_local bool is_print_cursor = false;
    deltatime_count += deltaTime;
    int font_h = al_get_font_ascent(wRes->gameFonts[TITLE_FONT]);
    double font_w = al_get_text_width(wRes->gameFonts[TITLE_FONT], playername_temp.substr(0, cursor_pos).c_str());
    wRes->nineSlices[WR9_INFO_BLOCK].DrawScaled(wGameCfg.screenWidth*0.25, wGameCfg.screenHeight*0.4, wGameCfg.screenWidth*0.5, wGameCfg.screenHeight*0.28);
    al_draw_text(wRes->gameFonts[TITLE_FONT], al_map_rgb(255,255,255), wGameCfg.screenWidth*0.5, wGameCfg.screenHeight*0.42, ALLEGRO_ALIGN_CENTER, "Player name:");
    al_draw_text(wRes->gameFonts[CONTENT_FONT], al_map_rgb(255,255,255), wGameCfg.screenWidth*0.5, wGameCfg.screenHeight*0.54, ALLEGRO_ALIGN_CENTER, "Press ENTER to save score.");
    al_draw_text(wRes->gameFonts[CONTENT_FONT], al_map_rgb(255,255,255), wGameCfg.screenWidth*0.5, wGameCfg.screenHeight*0.58, ALLEGRO_ALIGN_CENTER, "Press ESC to cancel.");
    al_draw_textf(wRes->gameFonts[TITLE_FONT], al_map_rgb(255,255,255), wGameCfg.screenWidth*0.28, wGameCfg.screenHeight*0.48, ALLEGRO_ALIGN_LEFT, "%s", playername_temp.c_str());
    if(deltatime_count > 500)
    {
        is_print_cursor = !is_print_cursor;
        deltatime_count = 0;
    }
    if(is_print_cursor)
        al_draw_line(wGameCfg.screenWidth*0.28+font_w, wGameCfg.screenHeight*0.485, wGameCfg.screenWidth*0.28+font_w, wGameCfg.screenHeight*0.485+font_h, al_map_rgb(255,255,255), 2);
}

void ResultScreen::Draw(double deltaTime)
{
    wRes->nineSlices[WR9_INFO_BLOCK].DrawScaled(wGameCfg.screenWidth*0.012, wGameCfg.screenHeight*0.12, wGameCfg.screenWidth*0.976, wGameCfg.screenHeight*0.83);
    wRes->bitmaps[WR_TITLE_BAR].DrawScaled(0,0, wGameCfg.screenWidth, 0);
    al_draw_text(wRes->gameFonts[TITLE_FONT],  al_map_rgb(255,255,255), wGameCfg.screenWidth*0.03, wGameCfg.screenHeight*0.01, 0, "Result");
    double yLoc = wGameCfg.screenHeight*RESULT_TITLE_Y;
    if(cur_song != nullptr)
    {
        if(cur_song_diff != MAP_TOTAL)
        {
            DrawLevel(wGameCfg.screenWidth*(RESULT_TITLE_X-0.08), yLoc+(wGameCfg.screenHeight*0.02), cur_song_diff, cur_song->mapLevel[cur_song_diff], true);
        }
        al_draw_ustr(wRes->gameFonts[TITLE_FONT], al_map_rgb(255, 255, 255),
                     wGameCfg.screenWidth*RESULT_TITLE_X, yLoc,
                     ALLEGRO_ALIGN_LEFT, cur_song->songTitleustr);
        yLoc += al_get_font_line_height(wRes->gameFonts[TITLE_FONT]);
        al_draw_ustr(wRes->gameFonts[CONTENT_FONT], al_map_rgb(255, 255, 255),
                     wGameCfg.screenWidth*RESULT_TITLE_X, yLoc,
                     ALLEGRO_ALIGN_LEFT, cur_song->songAtristustr);
    }
    yLoc += al_get_font_line_height(wRes->gameFonts[CONTENT_FONT])+(wGameCfg.screenHeight*0.02);
    al_draw_line(wGameCfg.screenWidth*0.05, yLoc-(wGameCfg.screenHeight*0.01),
                 wGameCfg.screenWidth*0.95, yLoc-(wGameCfg.screenHeight*0.01), al_map_rgb(138, 213, 235), 2);
    yLoc = wGameCfg.screenHeight*RESULT_BOARD_Y;
    for(int i = 0; i < RESULT_TOTAL; i++)
    {
        al_draw_text(wRes->gameFonts[TITLE_FONT], al_map_rgb(255, 255, 255),
                     wGameCfg.screenWidth*RESULT_JUDGE_X, yLoc,
                     ALLEGRO_ALIGN_LEFT, resultText[i].c_str());
        if(i != RESULT_ACCURACY)
            al_draw_textf(wRes->gameFonts[TITLE_FONT],al_map_rgb(255, 255, 255),
                          wGameCfg.screenWidth*RESULT_SCORE_X, yLoc,
                          ALLEGRO_ALIGN_RIGHT, "%d", gGAMEPLAYCTRL->scoreRecord[i]);
        else
            al_draw_textf(wRes->gameFonts[TITLE_FONT], al_map_rgb(255, 255, 255),
                          wGameCfg.screenWidth*RESULT_SCORE_X, yLoc,
                          ALLEGRO_ALIGN_RIGHT, "%.2lf", (double)gGAMEPLAYCTRL->accuracy);
        yLoc += al_get_font_line_height(wRes->gameFonts[TITLE_FONT])*1.1;
        al_draw_line(wGameCfg.screenWidth*RESULT_JUDGE_X, yLoc,
                     wGameCfg.screenWidth*RESULT_SCORE_X, yLoc, al_map_rgb(138, 213, 235), 2);
    }

    double item_height = wGameCfg.screenHeight * 0.08;
    double text_offset = wGameCfg.screenHeight * 0.01;
    double item_width = wGameCfg.screenWidth * 0.2;
    double xpos = 0;
    double ypos = wGameCfg.screenHeight*RESULT_OPTION_Y;
    switch(ctrlIdx)
    {
        case RS_AGAIN:
        {
            xpos = wGameCfg.screenWidth*RESULT_AGAIN_X - (item_width/2.0);
            wRes->nineSlices[WR9_BUTTON_SELECT].DrawScaled(xpos, ypos, item_width, item_height);
            xpos = wGameCfg.screenWidth*RESULT_RETURN_X - (item_width/2.0);
            wRes->nineSlices[WR9_BUTTON].DrawScaled(xpos, ypos, item_width, item_height);
            break;
        }
        case RS_RETURN:
        {
            xpos = wGameCfg.screenWidth*RESULT_AGAIN_X - (item_width/2.0);
            wRes->nineSlices[WR9_BUTTON].DrawScaled(xpos, ypos, item_width, item_height);
            xpos = wGameCfg.screenWidth*RESULT_RETURN_X - (item_width/2.0);
            wRes->nineSlices[WR9_BUTTON_SELECT].DrawScaled(xpos, ypos, item_width, item_height);
            break;
        }
        default:
            break;
    }
    al_draw_text(wRes->gameFonts[TITLE_FONT], al_map_rgb(255, 255, 255),
                 wGameCfg.screenWidth*RESULT_AGAIN_X,
                 wGameCfg.screenHeight*RESULT_OPTION_Y+text_offset,
                 ALLEGRO_ALIGN_CENTER, "Again");
    al_draw_text(wRes->gameFonts[TITLE_FONT], al_map_rgb(255, 255, 255),
                 wGameCfg.screenWidth*RESULT_RETURN_X,
                 wGameCfg.screenHeight*RESULT_OPTION_Y+text_offset,
                 ALLEGRO_ALIGN_CENTER, "Return");
    if(input_playername)
    {
        DrawPlayernameInputbox(deltaTime);
    }
}
