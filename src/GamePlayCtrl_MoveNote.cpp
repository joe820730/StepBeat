#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>

#include "globalDef.hpp"
#include "NoteInfo.hpp"
#include "GamePlayCtrl.hpp"
#include "ConfigProvider.hpp"

void GamePlayCtrl::MoveNote(double nowTime)
{
    NOTES_BARLIST* nowbarPtr = notes_bar;
    while(nowbarPtr != nullptr)
    {
        if(nowbarPtr->visible)
        {
            nowbarPtr->barPosition = CalcNotePosition(noteSpeed, nowbarPtr->barSpeed, nowTime, nowbarPtr->barTime, nowbarPtr->barBpm, note_judge_area);
            nowbarPtr->barPosition_end = CalcNotePosition(noteSpeed, nowbarPtr->barSpeed, nowTime, nowbarPtr->barTime_end, nowbarPtr->barBpm, note_judge_area);
            if(nowbarPtr->barTime - nowTime > 5000)
                break;
            NOTES_LINKLIST* nowPtr = nowbarPtr->notes;
            while(nowPtr != nullptr)
            {
                if(nowPtr->visible)
                {
                    /* Calculating note position. */
                    nowPtr->notePosition = CalcNotePosition(noteSpeed, nowPtr->noteSpeed, nowTime, nowPtr->noteTime, nowPtr->noteBpm, note_judge_area);
                    if(nowPtr->noteType == NTYPE_HOLD || nowPtr->noteType == NTYPE_KHIT)
                        nowPtr->notePosition_end = CalcNotePosition(noteSpeed, nowPtr->noteSpeed, nowTime, nowPtr->noteTime_end, nowPtr->noteBpm, note_judge_area);
                    /* Reduce search length. */
                    if(nowPtr->noteType == NTYPE_HIT)
                    {
                        if(!nowPtr->isSingleInLong && nowTime - nowPtr->noteTime > 2000)
                        {
                            nowPtr->visible = false;
                        }
                    }
                    else
                    {
                        if(nowTime - nowPtr->noteTime_end > 2000)
                        {
                            nowPtr->visible = false;
                        }
                    }
                }
                nowPtr = nowPtr->nextNote;
            }
        }
        nowbarPtr = nowbarPtr->nextBar;
    }
}
