#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include "Allegro_include.hpp"
#include "GamePlayCtrl.hpp"
#include "GameResources.hpp"
#include "ConfigProvider.hpp"

double delayStart = 0;
bool isTimeTagged = false;

void GamePlayCtrl::AutoPlayReset()
{
    delayStart = 0;
    isTimeTagged = false;
}

void GamePlayCtrl::AutoPlay(double nowTime)
{
    if(!isAutoPlay)
        return;
    NOTES_BARLIST* nowbarPtr = notes_bar;
    while(nowbarPtr != nullptr)
    {
        NOTES_LINKLIST* nowPtr = nowbarPtr->notes;
        while(nowPtr != nullptr)
        {
            double _judgeTime = fabs(nowPtr->noteTime - nowTime);
            switch(nowPtr->noteType)
            {
                case NTYPE_HIT:
                {
                    if( _judgeTime <= AUTOPLAY_PERFECT && !nowPtr->hitted && nowPtr->active)
                    {
                        if(nowPtr->noteType == NTYPE_HIT)
                        {
                            NoteJudge(_judgeTime, nowPtr);
                            hittedQueue.AttachEvent(nowPtr->noteArrow);
                            al_play_sample(wRes->wSounds[WS_HIT], (double)gameCfg.effectVol/100.0, 0, 1, ALLEGRO_PLAYMODE_ONCE, nullptr);
                        }
                    }
                    break;
                }
                case NTYPE_HOLD:
                {
                    if( _judgeTime <= AUTOPLAY_PERFECT && !nowPtr->hitted && nowPtr->active)
                    {
                        if(!isPressing[nowPtr->noteArrow])
                        {
                            NoteJudge(_judgeTime, nowPtr);
                            hittedQueue.AttachEvent(nowPtr->noteArrow);
                            if(nowPtr->isSingleInLong)
                                al_play_sample(wRes->wSounds[WS_HIT], (double)gameCfg.effectVol/100.0, 0, 1, ALLEGRO_PLAYMODE_ONCE, nullptr);
                            else
                                al_set_sample_instance_playing(pressSound, true);
                            isPressing[nowPtr->noteArrow] = true;
                        }
                    }
                    else
                    {
                        _judgeTime = fabs(nowPtr->noteTime_end - nowTime);
                        if( _judgeTime <= AUTOPLAY_PERFECT && nowPtr->hitted && nowPtr->active)
                        {
                            NoteJudge(_judgeTime, nowPtr);
                            if(isPressing[nowPtr->noteArrow])
                            {
                                if(nowPtr->isSingleInLong)
                                    al_play_sample(wRes->wSounds[WS_HIT], (double)gameCfg.effectVol/100.0, 0, 1, ALLEGRO_PLAYMODE_ONCE, nullptr);
                                else
                                    al_stop_sample_instance(pressSound);
                                isPressing[nowPtr->noteArrow] = false;
                                hittedQueue.AttachEvent(nowPtr->noteArrow);
                            }
                            nowPtr->active = false;
                            nowPtr->isInJudgeArea = false;
                        }
                    }
                    break;
                }
                case NTYPE_KHIT:
                {
                    if(judgeNote_keep != nullptr)
                    {
                        if(!isTimeTagged)
                        {
                            delayStart = nowTime;
                            isTimeTagged = true;
                        }
                        if(nowTime - delayStart >= 83)
                        {
                            if(!isHitting)
                            {
                                isHitting = true;
                            }
                            hitCount++;
                            hittedQueue.AttachEvent(judgeNote_keep->noteArrow);
                            al_play_sample(wRes->wSounds[WS_HIT], (double)gameCfg.effectVol/100.0, 0, 1, ALLEGRO_PLAYMODE_ONCE, nullptr);
                            isTimeTagged = false;
                        }
                    }
                    break;
                }
                default:
                {
                    break;
                }
            }
            if(enableShowTime)
            {
                ShowTimeStart(nowTime);
            }
            nowPtr = nowPtr->nextNote;
        }
        nowbarPtr = nowbarPtr->nextBar;
    }
}
