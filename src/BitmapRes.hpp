#ifndef BITMAPRES_HPP_INCLUDED
#define BITMAPRES_HPP_INCLUDED

#include "Allegro_include.hpp"

class BitmapRes
{
public:
    BitmapRes();
    ~BitmapRes(){};
    virtual int LoadRes(const char* resPath);
    virtual int LoadRes(ALLEGRO_BITMAP* srcBmp, int sx, int sy, int width, int height);
    double GetWidth()
    {
        return sourcew;
    };
    double GetHeight()
    {
        return sourceh;
    };
    double GetScaledWidth()
    {
        return bitmapw;
    };
    double GetScaledHeight()
    {
        return bitmaph;
    };
    void ResizeRes(int screenH, double scale);
    virtual void Draw(double xloc, double yloc);
    virtual void DrawScaled(double xloc, double yloc, double xsize = 0, double ysize = 0);
    virtual void DrawRotate(double xloc, double yloc, double angle);
    virtual void DrawTinted(double xloc, double yloc, ALLEGRO_COLOR tint);
    virtual void DestroyRes();
protected:
    bool isLoaded;
    ALLEGRO_BITMAP* bitmapres;
    double bitmapw;
    double bitmaph;
    double scaleRate_;
    double sourcew;
    double sourceh;
    double centerx;
    double centery;
};

class NineSliceBitmap : public BitmapRes
{
public:
    NineSliceBitmap();
    ~NineSliceBitmap(){};
    void SetSlicePoint(int x1, int x2, int y1, int y2);
    void Draw(double xloc, double yloc) override{};
    void DrawScaled(double xloc, double yloc, double xsize, double ysize) override;
    void DrawRotate(double xloc, double yloc, double angle) override{};
    void DrawTinted(double xloc, double yloc, ALLEGRO_COLOR tint) override{};
    void DestroyRes() override;
private:
    int uph;
    int lowh;
    int leftw;
    int rightw;
    int centerw;
    int centerh;
    enum
    {
        UPLEFT,
        UPCENTER,
        UPRIGHT,
        CENTERLEFT,
        CENTER,
        CENTERRIGHT,
        LOWLEFT,
        LOWCENTER,
        LOWRIGHT
    };
    ALLEGRO_BITMAP* slicedBmp[9];
};

class GroupedBitmap
{
public:
    GroupedBitmap();
    ~GroupedBitmap(){};
    int LoadRes(const char* resPath, int colcount = 1, int rowcount = 1);
    void ResizeRes(int screenH, double scale);
    double GetWidth();
    double GetHeight();
    double GetScaledWidth();
    double GetScaledHeight();
    void Draw(double xloc, double yloc, int idx = 0);
    void DrawScaled(double xloc, double yloc, double xsize = 0, double ysize = 0, int idx = 0);
    void DrawRotate(double xloc, double yloc, double angle, int idx = 0);
    void DrawTinted(double xloc, double yloc, ALLEGRO_COLOR tint, int idx = 0);
    void DestroyRes();
    BitmapRes* bitmaparray;
private:
    bool isLoaded;
    ALLEGRO_BITMAP* grouped;
    double sourcew;
    double sourceh;
    int arraytotal;
};

class GroupedBitmapNineSlice
{
public:
    GroupedBitmapNineSlice();
    ~GroupedBitmapNineSlice(){};
    int LoadRes(const char* resPath, int colcount = 1, int rowcount = 1);
    void SetSlicePoint(int x1, int x2, int y1, int y2);
    double GetWidth();
    double GetHeight();
    double GetScaledWidth();
    double GetScaledHeight();
    void Draw(double xloc, double yloc, int idx = 0);
    void DrawScaled(double xloc, double yloc, double xsize = 0, double ysize = 0, int idx = 0);
    void DrawRotate(double xloc, double yloc, double angle, int idx = 0);
    void DrawTinted(double xloc, double yloc, ALLEGRO_COLOR tint, int idx = 0);
    void DestroyRes();
    NineSliceBitmap* bitmaparray;
private:
    bool isLoaded;
    ALLEGRO_BITMAP* grouped;
    double sourcew;
    double sourceh;
    int arraytotal;
};


#endif // BITMAPRES_HPP_INCLUDED
