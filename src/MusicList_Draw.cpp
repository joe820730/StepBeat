#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <fstream>
#include <thread>

#include "globalDef.hpp"
#include "NoteInfo.hpp"
#include "MapInfo.hpp"
#include "ConfigProvider.hpp"
#include "MusicList.hpp"
#include "GamePlayCtrl.hpp"

void MusicList::DrawSongTitle(W_SONGLIST *nowPtr, double locX, double locY, ALLEGRO_COLOR color, bool isSelected)
{
    int fh = al_get_font_line_height(wRes->gameFonts[CONTENT_FONT]);
    double barLength = screenWidth * MAPLISTWIDTH;
    double targetHeight = screenHeight * MAPLISTHEIGHT;
    if(isSelected)
    {
        wRes->nineSlices[WR9_SELECTED].DrawScaled(locX, locY, barLength, targetHeight);
    }
    else
    {
        wRes->nineSlices[WR9_ITEM].DrawScaled(locX, locY, barLength, targetHeight);
    }
    al_draw_ustr(wRes->gameFonts[CONTENT_FONT], color, locX+MAPLISTFONTOFFSETX, locY+MAPLISTFONTOFFSETY, ALLEGRO_ALIGN_LEFT, nowPtr->songTitleustr);
    al_draw_ustr(wRes->gameFonts[SUBCONTENT_FONT], color, locX+MAPLISTFONTOFFSETX, locY+MAPLISTFONTOFFSETY+fh, ALLEGRO_ALIGN_LEFT, nowPtr->songAtristustr);
}

void MusicList::DrawHiscore(W_SONGLIST* nowPtr, double locX, double locY, double xsize, double ysize)
{
    double scorelistx = locX + screenWidth*0.05;
    double scorelisty = locY + screenHeight*0.09;
    wRes->nineSlices[WR9_INFO_BLOCK].DrawScaled(locX, locY, xsize, ysize);
    al_draw_textf(wRes->gameFonts[TITLE_FONT], al_map_rgb(138, 213, 235), locX+(xsize/2.0), locY+screenHeight*0.02, ALLEGRO_ALIGN_CENTER, "HISCORE");
    for(int i = 0; i < HISCORE_MAX; i++)
    {
        if(gGAMEPLAYCTRL->enableShowTime)
        {
            al_draw_textf(wRes->gameFonts[CONTENT_FONT], al_map_rgb(255,255,255),
                          scorelistx, scorelisty, ALLEGRO_ALIGN_LEFT, "%d\t%s",
                          i+1, nowPtr->scoredb[diffIdx].playername_bonus[i].c_str());
            al_draw_textf(wRes->gameFonts[CONTENT_FONT], al_map_rgb(255,255,255),
                          screenWidth*0.42, scorelisty, ALLEGRO_ALIGN_RIGHT, "%d\t%0.2lf",
                          nowPtr->scoredb[diffIdx].hiscore_bonus[i], nowPtr->scoredb[diffIdx].accuracy_bonus[i]);
        }
        else
        {
            al_draw_textf(wRes->gameFonts[CONTENT_FONT], al_map_rgb(255,255,255),
                          scorelistx, scorelisty, ALLEGRO_ALIGN_LEFT, "%d\t%s",
                          i+1, nowPtr->scoredb[diffIdx].playername[i].c_str());
            al_draw_textf(wRes->gameFonts[CONTENT_FONT], al_map_rgb(255,255,255),
                          screenWidth*0.42, scorelisty, ALLEGRO_ALIGN_RIGHT, "%d\t%0.2lf",
                          nowPtr->scoredb[diffIdx].hiscore[i], nowPtr->scoredb[diffIdx].accuracy[i]);
        }
        scorelisty += fontHeight;
    }
}

void MusicList::Draw(double deltaTime)
{
    if(!isSongScanning)
    {
        if(deltaTime > 50) //If FPS < 20
        {
            deltaTime = 50;
        }
        double targetHeight = screenHeight * MAPLISTHEIGHT;
        W_SONGLIST *nowPtr = wSongList;
        if(nowPtr == nullptr)
        {
            al_draw_text(wRes->gameFonts[TITLE_FONT], al_map_rgb(255,255,255), screenWidth*0.5, screenHeight*0.5, ALLEGRO_ALIGN_CENTER, "No maps.");
        }
        while(nowPtr != nullptr)
        {
            double listLocY = menuBaseLocY+targetHeight*(nowPtr->listIdx);
            double listLocX = menuLocateX + abs(nowPtr->listIdx-menuIdx)*(screenWidth>>6);
            if(menuIdx == nowPtr->listIdx)
            {
                if(listLocY > screenHeight * yLoc)
                    menuBaseLocY -= fabs(listLocY-menuLocateY)*deltaTime/50;
                else
                    menuBaseLocY += fabs(listLocY-menuLocateY)*deltaTime/50;
                DrawSongTitle(nowPtr, listLocX, listLocY, al_map_rgb(255,255,255), true);
                DrawSelectedInfo(nowPtr);
            }
            else
            {
                if(listLocY > -targetHeight && listLocY < screenHeight)
                    DrawSongTitle(nowPtr, listLocX, listLocY, al_map_rgb(255,255,255), false);
            }
            nowPtr = nowPtr->nextSong;
        }
    }
    else
    {
        al_draw_text(wRes->gameFonts[TITLE_FONT], al_map_rgb(255,255,255), screenWidth*0.5, screenHeight*0.5, ALLEGRO_ALIGN_CENTER, "Song scanning...");
    }
    wRes->bitmaps[WR_TITLE_BAR].DrawScaled(0,0, screenWidth, 0);
    al_draw_text(wRes->gameFonts[TITLE_FONT],  al_map_rgb(255,255,255), screenWidth*0.03, screenHeight*0.01, 0, "Select music");
}

void MusicList::DrawSelectedInfo(W_SONGLIST* nowPtr)
{
    wRes->nineSlices[WR9_INFO_BLOCK].DrawScaled(screenWidth*0.03, screenHeight*0.12, screenWidth*0.44, screenHeight*0.33);
    if(nowPtr->cover != nullptr)
        DrawBitmap(nowPtr->cover, screenWidth * MAPLISTCOVERX, screenHeight * MAPLISTCOVERY, screenHeight * MAPCOVERSIZE, screenHeight * MAPCOVERSIZE);
    else
        wRes->bitmaps[WR_NOCOVER].DrawScaled(screenWidth * MAPLISTCOVERX, screenHeight * MAPLISTCOVERY, screenHeight * MAPCOVERSIZE, screenHeight * MAPCOVERSIZE);

    double songInfoLocX = screenWidth * MAPLISTINFOX;
    double songInfoLocY = screenHeight * MAPLISTINFOY;
    al_draw_textf(wRes->gameFonts[TITLE_FONT], al_map_rgb(138, 213, 235), songInfoLocX, songInfoLocY, ALLEGRO_ALIGN_CENTER, "BPM");
    songInfoLocY += fontHeight*1.1;
    al_draw_textf(wRes->gameFonts[CONTENT_FONT], al_map_rgb(255, 255, 255), songInfoLocX, songInfoLocY, ALLEGRO_ALIGN_CENTER, "%.2lf", nowPtr->songBPM);
    songInfoLocY += fontHeight;
    al_draw_textf(wRes->gameFonts[TITLE_FONT], al_map_rgb(138, 213, 235), songInfoLocX, songInfoLocY, ALLEGRO_ALIGN_CENTER, "Map Author");
    songInfoLocY += fontHeight*1.1;
    al_draw_ustr(wRes->gameFonts[CONTENT_FONT], al_map_rgb(255, 255, 255), songInfoLocX, songInfoLocY, ALLEGRO_ALIGN_CENTER, nowPtr->mapAuthorustr[diffIdx]);

    double diffListLoc = screenHeight * MAPLISTDIFFY;
    for(int i = 0; i < MAP_TOTAL; i++)
    {
        double diffLocX = screenWidth*(MAPLISTDIFFX + 0.08*i);
        if(nowPtr->haveDiff[i])
        {
            DrawLevel(diffLocX, diffListLoc, i, nowPtr->mapLevel[i], (i == diffIdx) );
        }
        else
            wRes->bmpGroups[WRG_DIFFBG].Draw(diffLocX, diffListLoc, MAP_TOTAL);
    }
    DrawHiscore(nowPtr, screenWidth*MAPLIST_SCOREBOARD_X, screenHeight*MAPLIST_SCOREBOARD_Y,
                screenWidth*0.44, screenHeight*0.4);
}
