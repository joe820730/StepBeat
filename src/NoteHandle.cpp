#include <iostream>
#include <cstdio>
#include <cstdlib>

#include "globalDef.hpp"
#include "ConfigProvider.hpp"
#include "GamePlayCtrl.hpp"
#include "NoteInfo.hpp"
#include "debug.hpp"

void InsertNote(NOTES_LINKLIST**& nowNotes, double noteTime, double noteTime_end, NOTE_TYPE nType, NOTE_ARROW _nArrow, double noteBpm, double noteSpeed, bool isFixedArrow, uint32_t noteId, int khitDrawCount, int hitCountTarget)
{
    if((*nowNotes) == nullptr)
    {
        LOG_ALL("New space writing...\n");
        (*nowNotes) = new NOTES_LINKLIST;
        assert(*nowNotes);
        (*nowNotes)->nextNote = nullptr;
    }
    else
    {
        LOG_ALL("Reserved space writing...\n");
    }
    (*nowNotes)->visible = true;
    (*nowNotes)->active = true;
    (*nowNotes)->isInJudgeArea = false;
    (*nowNotes)->isSingleInLong = false;
    (*nowNotes)->hitted = false;
    (*nowNotes)->noteBpm = noteBpm;
    (*nowNotes)->noteSpeed = noteSpeed;
    (*nowNotes)->notePosition = CalcNotePosition(1, noteSpeed, EMPTY_TIME, noteTime, noteBpm, 0);
    (*nowNotes)->noteTime = noteTime;
    (*nowNotes)->noteType = nType;
    (*nowNotes)->noteId = noteId;
    (*nowNotes)->isFixedArrow = isFixedArrow;
    (*nowNotes)->khitDrawCount = khitDrawCount;
    (*nowNotes)->hitCountTarget = hitCountTarget;
    if(nType == NTYPE_HOLD || nType == NTYPE_KHIT)
    {
        (*nowNotes)->notePosition_end = CalcNotePosition(1, noteSpeed, EMPTY_TIME, noteTime_end, noteBpm, 0);
        (*nowNotes)->noteTime_end = noteTime_end;
    }
    if(nType != NTYPE_TOTAL)
        (*nowNotes)->noteArrow = _nArrow;
    else
        (*nowNotes)->noteArrow = NA_TOTAL;
}

void NoteClean(NOTES_LINKLIST*& notes)
{
    NOTES_LINKLIST* head = notes;
    NOTES_LINKLIST* delPtr = nullptr;
    while(head != nullptr)
    {
        LOG_ALL("Deleting...\n");
        delPtr = head;
        head = head->nextNote;
        delete delPtr;
    }
}

void NoteArrowReset(NOTES_LINKLIST*& notes, double beginAt)
{
    NOTES_LINKLIST* head = notes;
    NOTES_LINKLIST* tmpLong = nullptr;
    while(head != nullptr)
    {
        if(head->noteType != NTYPE_TOTAL)
        {
            if(head->noteTime >= beginAt)
            {
                head->visible = true;
                head->active = true;
                head->isInJudgeArea = false;
                head->hitted = false;
            }
            head->notePosition = CalcNotePosition(1, head->noteSpeed, EMPTY_TIME, head->noteTime, head->noteBpm, 0);
            head->notePosition_end = CalcNotePosition(1, head->noteSpeed, EMPTY_TIME, head->noteTime_end, head->noteBpm, 0);
            if(!head->isFixedArrow)
                head->noteArrow = (NOTE_ARROW)(rand()%NA_TOTAL);
            if(head->noteType == NTYPE_HOLD && head->isSingleInLong)
                tmpLong = head;
            if(head->noteType == NTYPE_HIT)
            {
                if(head->isSingleInLong)
                {
                    if(tmpLong != nullptr)
                    {
                        if(!head->isFixedArrow)
                        {
                            while(tmpLong->noteArrow == head->noteArrow)
                                head->noteArrow = (NOTE_ARROW)(rand()%NA_TOTAL);
                        }
                    }
                }
                else
                {
                    if(tmpLong != nullptr)
                        tmpLong = nullptr;
                }
            }
            head = head->nextNote;
        }
        else
            head = head->nextNote; //Skip first empty note
    }
}

void NoteReset(NOTES_LINKLIST*& notes)
{
    NOTES_LINKLIST* head = notes;
    while(head != nullptr)
    {
        head->noteId = 0;
        head->visible = false;
        head->active = false;
        head->hitted = false;
        head->noteTime = EMPTY_TIME;
        head->noteTime_end = EMPTY_TIME;
        head->notePosition = 0;
        head->notePosition_end = 0;
        head->noteType = NTYPE_TOTAL;
        head->noteBpm = 0;
        head->noteSpeed = 0;
        head->khitDrawCount = 0;
        head->hitCountTarget = 0;
        head->noteArrow = NA_TOTAL;
        head->isInJudgeArea = false;
        head->isSingleInLong = false;
        head->isFixedArrow = false;
        head = head->nextNote;
    }
}

void NoteInit(NOTES_LINKLIST*& notes)
{
    if(notes != nullptr)
    {
        NoteReset(notes);
    }
    else
    {
        NOTES_LINKLIST** head = &notes;
        for(int i = 0; i < RESERVED_SPACE; i++)
        {
            (*head) = new NOTES_LINKLIST;
            (*head)->noteId = 0;
            (*head)->visible = false;
            (*head)->active = false;
            (*head)->hitted = false;
            (*head)->noteTime = EMPTY_TIME;
            (*head)->noteTime_end = EMPTY_TIME;
            (*head)->notePosition = 0;
            (*head)->notePosition_end = 0;
            (*head)->noteType = NTYPE_TOTAL;
            (*head)->noteBpm = 0;
            (*head)->noteSpeed = 0;
            (*head)->khitDrawCount = 0;
            (*head)->hitCountTarget = 0;
            (*head)->noteArrow = NA_TOTAL;
            (*head)->isInJudgeArea = false;
            (*head)->isSingleInLong = false;
            (*head)->isFixedArrow = false;
            (*head)->nextNote = nullptr;
            head = &((*head)->nextNote);
        }
    }
}

void BarInit(NOTES_BARLIST*& bars)
{
    if(bars != nullptr)
    {
        BarReset(bars);
    }
    else
    {
        NOTES_BARLIST** head = &bars;
        for(int i = 0; i < RESERVED_BAR_SPACE; i++)
        {
            (*head) = new NOTES_BARLIST;
            (*head)->barTime = EMPTY_TIME;
            (*head)->barTime_end = EMPTY_TIME;
            (*head)->barPosition = 0;
            (*head)->barPosition_end = 0;
            (*head)->barBpm = 0;
            (*head)->barSpeed = 0;
            (*head)->visible = false;
            (*head)->active = false;
            (*head)->notes = nullptr;
            (*head)->last_note = nullptr;
            (*head)->nextBar = nullptr;
            head = &((*head)->nextBar);
        }
    }
}

void BarArrowReset(NOTES_BARLIST*& bars, double beginAt)
{
    NOTES_BARLIST* headbar = bars;
    NOTES_LINKLIST* tmpLong = nullptr;
    NOTE_ARROW prevLongArrow = NA_TOTAL;
    while(headbar != nullptr)
    {
        NOTES_LINKLIST* head = headbar->notes;
        while(head != nullptr)
        {
            if(head->noteType != NTYPE_TOTAL)
            {
                if(head->noteTime >= beginAt)
                {
                    head->visible = true;
                    head->active = true;
                    head->isInJudgeArea = false;
                    head->hitted = false;
                }
                head->notePosition = CalcNotePosition(1, head->noteSpeed, EMPTY_TIME, head->noteTime, head->noteBpm, 0);
                head->notePosition_end = CalcNotePosition(1, head->noteSpeed, EMPTY_TIME, head->noteTime_end, head->noteBpm, 0);
                if(!head->isFixedArrow)
                    head->noteArrow = (NOTE_ARROW)(rand()%NA_TOTAL);
                if(head->noteType == NTYPE_HOLD)
                {
                    while((head->noteArrow == prevLongArrow) && !head->isFixedArrow)
                        head->noteArrow = (NOTE_ARROW)(rand()%NA_TOTAL);
                    if(head->isSingleInLong)
                    {
                        tmpLong = head;
                    }
                    prevLongArrow = head->noteArrow;
                }
                if(head->noteType == NTYPE_HIT)
                {
                    if(head->isSingleInLong)
                    {
                        if(tmpLong != nullptr)
                        {
                            if(!head->isFixedArrow)
                            {
                                while(tmpLong->noteArrow == head->noteArrow)
                                    head->noteArrow = (NOTE_ARROW)(rand()%NA_TOTAL);
                            }
                        }
                    }
                    else
                    {
                        if(tmpLong != nullptr)
                            tmpLong = nullptr;
                    }
                }
            }
            head = head->nextNote;
        }
        headbar->barPosition = CalcNotePosition(1, headbar->barSpeed, EMPTY_TIME, headbar->barTime, headbar->barBpm, 0);
        headbar->barPosition_end = CalcNotePosition(1, headbar->barSpeed, EMPTY_TIME, headbar->barTime_end, headbar->barBpm, 0);
        headbar->active = true;
        headbar->visible = true;
        headbar = headbar->nextBar;
    }
}

void BarReset(NOTES_BARLIST*& bars)
{
    NOTES_BARLIST* head = bars;
    while(head != nullptr)
    {
        NoteReset(head->notes);
        head->barTime = EMPTY_TIME;
        head->barTime_end = EMPTY_TIME;
        head->barPosition = 0;
        head->barPosition_end = 0;
        head->barBpm = 0;
        head->barSpeed = 0;
        head->visible = false;
        head->active = false;
        head->last_note = nullptr;
        head = head->nextBar;
    }
}

void BarClean(NOTES_BARLIST*& bars)
{
    NOTES_BARLIST* head = bars;
    NOTES_BARLIST* delPtr = nullptr;
    while(head != nullptr)
    {
        LOG_ALL("Deleting...\n");
        delPtr = head;
        NoteClean(head->notes);
        head = head->nextBar;
        delete delPtr;
    }
}

void InsertBar(NOTES_BARLIST**& nowbars, double noteTime, double noteBpm, double noteSpeed)
{
    if((*nowbars) == nullptr)
    {
        LOG_ALL("New space writing...\n");
        (*nowbars) = new NOTES_BARLIST;
        assert(*nowbars);
        (*nowbars)->nextBar = nullptr;
        (*nowbars)->notes = nullptr;
    }
    else
    {
        LOG_ALL("Reserved space writing...\n");
    }
    (*nowbars)->barTime = noteTime;
    (*nowbars)->barPosition = CalcNotePosition(1, noteSpeed, EMPTY_TIME, noteTime, noteBpm, 0);
    (*nowbars)->barBpm = noteBpm;
    (*nowbars)->barSpeed = noteSpeed;
    (*nowbars)->active = true;
    (*nowbars)->visible = true;
    (*nowbars)->last_note = nullptr;
}
