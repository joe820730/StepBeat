#include <iostream>
#include <cstdio>
#include <cstdlib>

#include "Allegro_include.hpp"
#include "globalDef.hpp"
#include "GameResources.hpp"
#include "ConfigProvider.hpp"
#include "MenuInterface.hpp"

void MenuInterface::SetMenuLoc(int scrHeight, int scrWidth)
{
    screenHeight = scrHeight;
    screenWidth = scrWidth;
    menuLocateX = xLoc * screenWidth;
    menuLocateY = yLoc * screenHeight;
}

void IdxAdd(int32_t& targetIdx, int32_t targetIdxMAX, int addNum)
{
    if(targetIdx < targetIdxMAX - 1 && targetIdx >= 0)
    {
        if((targetIdx + addNum) < targetIdxMAX - 1)
            targetIdx += addNum;
        else
            targetIdx = targetIdxMAX - 1;
    }
    else
        targetIdx = 0;
}

void IdxMinus(int32_t& targetIdx, int32_t targetIdxMAX, int minusNum)
{
    if(targetIdx > 0 && targetIdx < targetIdxMAX)
    {
        if((targetIdx - minusNum) > 0)
            targetIdx -= minusNum;
        else
            targetIdx = 0;
    }
    else
        targetIdx = targetIdxMAX - 1;
}
