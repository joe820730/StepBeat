#ifndef STEPBEAT_H_INCLUDED
#define STEPBEAT_H_INCLUDED

#define GAME_TITLE "StepBeat"

#define GAME_VERSION     0
#define GAME_SUBVERSION  12
#define GAME_WIP_VER     0
#define GAME_HOTFIX_VER  0
#define GAME_VER_STR     "0.12.0"

#endif // STEPBEAT_H_INCLUDED
