#include <iostream>
#include <sstream>
#include <iomanip>
#include <cmath>
#include "Allegro_include.hpp"
#include "GameResources.hpp"
using namespace std;

void DrawBitmap(ALLEGRO_BITMAP* alBitmap, double xLoc, double yLoc, double width, double height)
{
    if(alBitmap != nullptr)
    {
        double bitmapWidth = al_get_bitmap_width(alBitmap);
        double bitmapHeight = al_get_bitmap_height(alBitmap);
        if(width <= 0 || height <= 0)
        {
            al_draw_bitmap(alBitmap, xLoc, yLoc, 0);
        }
        else
        {
            al_draw_scaled_bitmap(alBitmap, 0, 0, bitmapWidth, bitmapHeight, xLoc, yLoc, width, height, 0);
        }
    }
}

void DrawScaledBitmap(ALLEGRO_BITMAP* alBitmap, double xLoc, double yLoc, double scale)
{
    if(alBitmap != nullptr)
    {
        double bitmapWidth = al_get_bitmap_width(alBitmap);
        double bitmapHeight = al_get_bitmap_height(alBitmap);
        double width = bitmapWidth * scale;
        double height = bitmapHeight * scale;
        if(scale > 0)
        {
            al_draw_scaled_bitmap(alBitmap, 0, 0, bitmapWidth, bitmapHeight, xLoc, yLoc, width, height, 0);
        }
        else
        {
            al_draw_bitmap(alBitmap, xLoc, yLoc, 0);
        }
    }
}

void DrawScaledPartBitmap(ALLEGRO_BITMAP* alBitmap, double xLoc, double yLoc, double sx, double sy, double sw, double sh, double xscale, double yscale)
{
    if(alBitmap != nullptr)
    {
        double width = sw * xscale;
        double height = sh * yscale;
        if(xscale > 0 && yscale > 0)
        {
            al_draw_scaled_bitmap(alBitmap, sx, sy, sw, sh, xLoc, yLoc, width, height, 0);
        }
        else
        {
            al_draw_scaled_bitmap(alBitmap, sx, sy, sw, sh, xLoc, yLoc, sw, sh, 0);
        }
    }
}

void DrawScaledTintedBitmap(ALLEGRO_BITMAP* alBitmap, double xLoc, double yLoc, double scale, ALLEGRO_COLOR tint)
{
    if(alBitmap != nullptr)
    {
        double bitmapWidth = al_get_bitmap_width(alBitmap);
        double bitmapHeight = al_get_bitmap_height(alBitmap);
        double width = bitmapWidth * scale;
        double height = bitmapHeight * scale;
        if(scale > 0)
        {
            al_draw_tinted_scaled_bitmap(alBitmap, tint, 0, 0, bitmapWidth, bitmapHeight, xLoc, yLoc, width, height, 0);
        }
        else
        {
            al_draw_tinted_bitmap(alBitmap, tint, xLoc, yLoc, 0);
        }
    }
}

void DrawScaledRotatedBitmap(ALLEGRO_BITMAP* alBitmap, double xLoc, double yLoc, double scale, double angle)
{
    if(alBitmap != nullptr)
    {
        double bitmapWidth = al_get_bitmap_width(alBitmap);
        double bitmapHeight = al_get_bitmap_height(alBitmap);
        double offsetX = bitmapWidth / 2.0;
        double offsetY = bitmapHeight / 2.0;
        if(scale > 0)
        {
            offsetX *= scale;
            offsetY *= scale;
            al_draw_scaled_rotated_bitmap(alBitmap, bitmapWidth/2.0, bitmapHeight/2.0, xLoc+offsetX, yLoc+offsetY, scale, scale, angle, 0);
        }
        else
        {
            al_draw_rotated_bitmap(alBitmap, offsetX, offsetY, xLoc+offsetX, yLoc+offsetY, angle, 0);
        }
    }
}

void DrawBitmapNum(int fontGroupEnum, int targetNum, double xLoc, double yLoc, int numAlign, int numLen, bool isFillwithZero, double scaleRate)
{
    string numStr;
    stringstream tmpstream;
    tmpstream.str("");
    tmpstream.clear();
    double fontWidth = wRes->bmpGroups[fontGroupEnum].GetScaledWidth() * scaleRate;
    double fontHeight = wRes->bmpGroups[fontGroupEnum].GetScaledHeight() * scaleRate;
    yLoc -= wRes->bmpGroups[fontGroupEnum].GetHeight() * ((scaleRate-1)/2);
    double strBitmapLen = fontWidth*numLen;
    if(fontGroupEnum >= WRG_HITNUM && fontGroupEnum <= WRG_SCORENUM)
    {
        tmpstream << setw(numLen) << setfill((char)('0'-1)) << targetNum;
        tmpstream >> numStr;
        switch(numAlign)
        {
            default:
            case BITMAPALIGN_LEFT:
            {
                xLoc -= strBitmapLen * ((scaleRate-1)/2);
                break;
            }
            case BITMAPALIGN_RIGHT:
            {
                xLoc -= strBitmapLen * ((scaleRate-1)/2);
                xLoc -= strBitmapLen;
                break;
            }
            case BITMAPALIGN_CENTER:
            {
                xLoc -= (strBitmapLen/2);
                break;
            }
        }
        for(size_t i = 0, tmplen = numStr.length(); i < tmplen; i++)
        {
            int tmpNum = numStr.at(i) - '0';
            if(isFillwithZero && tmpNum < 0)
                tmpNum = 0;
            wRes->bmpGroups[fontGroupEnum].DrawScaled(xLoc+fontWidth*i, yLoc, fontWidth, fontHeight, tmpNum);
        }
    }
}

void DrawLevel(double xLoc, double yLoc, int diff, uint8_t level, bool isSelected)
{
    double levelTextOffsetX = wRes->bmpGroups[WRG_DIFFBG].GetScaledWidth()/2.0;
    double levelTextOffsetY = (wRes->bmpGroups[WRG_DIFFBG].GetScaledHeight() - (double)al_get_font_line_height(wRes->gameFonts[CONTENT_FONT]))/2.0;
    if(isSelected)
    {
        wRes->bmpGroups[WRG_DIFFBG].Draw(xLoc, yLoc, diff);
        al_draw_textf(wRes->gameFonts[CONTENT_FONT], al_map_rgb(255, 255, 255),
                      xLoc + levelTextOffsetX, yLoc+levelTextOffsetY, ALLEGRO_ALIGN_CENTER, "%2hu", level);
    }
    else
    {
        wRes->bmpGroups[WRG_DIFFBG].DrawTinted(xLoc, yLoc, al_map_rgb_f(0.5, 0.5, 0.5), diff);
        al_draw_textf(wRes->gameFonts[CONTENT_FONT], al_map_rgb(120, 120, 120),
                      xLoc + levelTextOffsetX, yLoc+levelTextOffsetY, ALLEGRO_ALIGN_CENTER, "%2hu", level);
    }
}
