#ifndef ALLEGRO_INCLUDE_HPP_INCLUDED
#define ALLEGRO_INCLUDE_HPP_INCLUDED

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_native_dialog.h>
#include <allegro5/allegro_video.h>

extern ALLEGRO_DISPLAY* g_display;

#endif // ALLEGRO_INCLUDE_HPP_INCLUDED
