#include <iostream>
#include <cstdio>
#include <cstdlib>

#include "globalDef.hpp"
#include "MapInfo.hpp"
#include <allegro5/allegro_native_dialog.h>
using namespace std;

typedef struct _localSonglistObj
{
    int listIdx;
    W_SONGLIST** nowPtrPtr;
    W_SONGLIST* prevPtr;
}LOCAL_SONGLIST_OBJ;

std::string GetFilenameExt(const std::string& tmpStr)
{
    std::size_t posOfDot = tmpStr.find_last_of(".");
    return tmpStr.substr(posOfDot + 1);
}

std::string GetMapFilePath(const std::string& tmpStr)
{
    std::size_t posOfDot = tmpStr.find_last_of(".");
    return tmpStr.substr(0, posOfDot + 1) + "wdm";
}

int ScanDir(ALLEGRO_FS_ENTRY* songDir, void *extra)
{
    int ret = 0;
    if(!songDir)
    {
        ret = ALLEGRO_FOR_EACH_FS_ENTRY_ERROR;
    }
    if((al_get_fs_entry_mode(songDir) & ALLEGRO_FILEMODE_ISDIR) != ALLEGRO_FILEMODE_ISDIR)
    {
        std::string tmpPathStr = al_get_fs_entry_name(songDir);
        if(extra != nullptr)
        {
            LOCAL_SONGLIST_OBJ* songListObj = (LOCAL_SONGLIST_OBJ*)extra;
            if(GetFilenameExt(tmpPathStr) == "wdi")
            {
                int readerStat = MapInfoReader(tmpPathStr, songListObj->nowPtrPtr);
                if(readerStat == INFO_SUCCESS)
                {
                    (*songListObj->nowPtrPtr)->prevSong = songListObj->prevPtr;
                    (*songListObj->nowPtrPtr)->listIdx = songListObj->listIdx;
                    songListObj->nowPtrPtr = &(*songListObj->nowPtrPtr)->nextSong;
                    songListObj->listIdx++;
                    songListObj->prevPtr = (*songListObj->nowPtrPtr);
                }
                else
                {
                    printf("%s()\n", __func__);
                    switch(readerStat)
                    {
                        case INFO_NOBPM:
                        {
                            al_show_native_message_box(nullptr, "Error occurred when reading map info.",
                                                       "Song BPM not specified: ",
                                                       tmpPathStr.c_str(), "OK", 0);
                            break;
                        }
                        case INFO_NODIFF:
                        {
                            al_show_native_message_box(nullptr, "Error occurred when reading map info.",
                                                       "No valid map and level found: ",
                                                       tmpPathStr.c_str(), "OK", 0);
                            break;
                        }
                        case INFO_NOWAV:
                        {
                            al_show_native_message_box(nullptr, "Error occurred when reading map info.",
                                                       "WAV path is empty: ",
                                                       tmpPathStr.c_str(), "OK", 0);
                            break;
                        }
                        case INFO_NOFILE:
                        {
                            al_show_native_message_box(nullptr, "Error occurred when reading map info.",
                                                       "Can't read file:",
                                                       tmpPathStr.c_str(), "OK", 0);
                            break;
                        }
                    }
                }
            }
            ret = ALLEGRO_FOR_EACH_FS_ENTRY_SKIP;
            /* Here will record all files with filename extension "wdi". */
        }
        else
        {
            ret = ALLEGRO_FOR_EACH_FS_ENTRY_ERROR;
        }
    }
    else if((al_get_fs_entry_mode(songDir) & ALLEGRO_FILEMODE_ISDIR) == ALLEGRO_FILEMODE_ISDIR)
    {
        ret = ALLEGRO_FOR_EACH_FS_ENTRY_OK;
    }
    return ret;
}

int ReadDir(const char* songdir_path, W_SONGLIST** songList)
{
    ALLEGRO_FS_ENTRY* songDir = al_create_fs_entry(songdir_path);
    LOCAL_SONGLIST_OBJ songListObj;
    songListObj.prevPtr = nullptr;
    songListObj.nowPtrPtr = songList;
    songListObj.listIdx = 0;
    int ret = 0;
    if(!al_fs_entry_exists(songDir))
    {
        ret = SCAN_NODIR;
    }
    else
    {
        if(al_for_each_fs_entry(songDir, ScanDir, &songListObj) ==
            ALLEGRO_FOR_EACH_FS_ENTRY_ERROR)
        {
            ret = SCAN_UNKNOWN;
        }
        else
        {
            ret = SCAN_SUCCESS;
        }
    }
    if(songDir != nullptr)
    {
      al_close_directory(songDir);
      al_destroy_fs_entry(songDir);
    }
    return ret;
}

void SongListClean(W_SONGLIST *head)
{
    W_SONGLIST *delPtr = head;
    W_SONGLIST *nextptr = head->nextSong;
    while(delPtr != nullptr)
    {
        write_score_db(delPtr->dbPath, delPtr->scoredb);
        nextptr = delPtr->nextSong;
        for(int i = 0; i < MAP_TOTAL; i++)
        {
            al_ustr_free(delPtr->mapAuthorustr[i]);
        }
        al_ustr_free(delPtr->songAtristustr);
        al_ustr_free(delPtr->songTitleustr);
        if(delPtr->cover != nullptr)
            al_destroy_bitmap(delPtr->cover);
        delete delPtr;
        delPtr = nextptr;
    }
}
