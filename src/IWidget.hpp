#ifndef WIDGET_HPP_INCLUDED
#define WIDGET_HPP_INCLUDED
#include <stdint.h>
#include <list>
#include <string>
#include "Allegro_include.hpp"
#include "BitmapRes.hpp"

typedef struct
{
    double x;
    double y;
}Point;

typedef struct
{
    double width;
    double height;
}Dimension;

class IWidget;
typedef std::list<IWidget*> WidgetList;
typedef void(*WidgetDrawFunc)(IWidget* parent, double deltatime);

class IWidget
{
public:
    IWidget();
    ~IWidget();
    void MouseListener(ALLEGRO_MOUSE_EVENT ev);
    void SetLocation(double x, double y);
    void SetSize(double width, double height);
    void SetVisible(bool visible);
    void SetClickable(bool clickable);
    void AssignChild(IWidget* child);
    void SetCustomDraw(WidgetDrawFunc draw_func);
    virtual void Draw(double deltatime) = 0;
    void DestroyRes();
    Point location;
    Dimension widgetsize;
protected:
    void DrawBitmap(double xloc, double yloc);
    void SetParent(IWidget* parent);
    void CheckEnter(int32_t x, int32_t y);
    void CheckClick(int32_t x, int32_t y);
    bool isClickable;
    bool isEntered;
    bool isClicked;
    bool isVisible;
    IWidget* parent;
    WidgetList childs;
    WidgetDrawFunc customdrawfunc;
};

class Page : public IWidget
{
public:
    Page(){};
    ~Page(){};
    void Draw(double deltatime);
};

class Button : public IWidget
{
public:
    Button();
    ~Button(){};
    int SetBitmap(BitmapRes* base, BitmapRes* highlight, BitmapRes* clicked);
    int SetText(std::string str);
    int SetText(const char* str);
    void Draw(double deltatime);
private:
    BitmapRes* base;
    BitmapRes* highlight;
    BitmapRes* clicked;
};

class List : public IWidget
{
public:
    List(){};
    ~List(){};
    void Draw(double deltatime);
};

#endif // WIDGET_HPP_INCLUDED
