#ifndef _RESULTSCREEN_HPP_INCLUDED_
#define _RESULTSCREEN_HPP_INCLUDED_

#include <cstdio>
#include <iostream>
#include <cstdlib>
#include "Allegro_include.hpp"
#include "ConfigProvider.hpp"
#include "GamePlayCtrl.hpp"
#include "MapInfo.hpp"
#include "GameResources.hpp"

#define RESULT_TEXT_X 0.5
#define RESULT_TEXT_Y 0.1
#define RESULT_TITLE_X 0.2
#define RESULT_TITLE_Y 0.15
#define RESULT_BOARD_Y 0.32
#define RESULT_JUDGE_X 0.25
#define RESULT_SCORE_X 0.75

#define RESULT_OPTION_Y 0.82
#define RESULT_AGAIN_X 0.35
#define RESULT_RETURN_X 0.65
enum
{
    RS_AGAIN,
    RS_RETURN,
    RS_TOTAL
};

class ResultScreen
{
public:
    ResultScreen();
    ~ResultScreen();
    bool Setup(ConfigProvider* cfg_provider);
    void ScreenCtrl(int keyCode);
    void InputPlayerName(int unichar);
    void Draw(double deltaTime);
    void DrawPlayernameInputbox(double deltaTime);
    void AttachCurSongDiff(W_SONGLIST* selectedSong, W_MAPDIFF selectedDiff);
    bool input_playername;
    bool is_bonus_mode;
    int current_rank;
private:
    std::string* resultText;
    std::string playername_temp;
    size_t cursor_pos;
    int cur_input;
    W_GAMECONFIG wGameCfg;
    W_SONGLIST* cur_song;
    W_MAPDIFF cur_song_diff;
    int ctrlIdx;
};

extern ResultScreen* gRESULTSCREEN;
#endif
