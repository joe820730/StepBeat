#include "GamePlayCtrl.hpp"

const double kShowTime_SpeedUp[SHOWTIME_LEVELS-1]
{
    1.2,
    1.4,
    1.6,
    1.8,
    2.0
};
const double kShowTime_SpeedDown[SHOWTIME_LEVELS-1]
{
    0.9,
    0.85,
    0.8,
    0.75,
    0.7
};
const int kShowTime_RotateDistance[SHOWTIME_LEVELS-1]
{
    250,
    200,
    150,
    100,
    50
};
const int kShowTime_HiddenDistance[SHOWTIME_LEVELS-1]
{
    100,
    135,
    170,
    205,
    240
};
const double kShowTime_multiple[SHOWTIME_LEVELS-1]
{
    1.5,
    1.75,
    2.0,
    2.25,
    2.5
};

const uint8_t ktargetShowTimeGather[MAP_TOTAL]
{
    20,
    25,
    30,
    40,
    40
};

void GamePlayCtrl::SetShowTimeGatherMax(W_MAPDIFF selectedDiff)
{
    if(selectedDiff < MAP_TOTAL)
        showtimegatherMax = ktargetShowTimeGather[selectedDiff];
    else
        showtimegatherMax = ktargetShowTimeGather[0];
}

void GamePlayCtrl::ShowTimeCtrl(double deltaTime)
{
    if(!enableShowTime)
        return;
    if(showtimegather == 0)
    {
        showtime_scoremultiplier = 1.0;
        isShowTime = false;
    }
    if(isShowTime)
    {
        if(showType == SHOWTIME_SPEEDUP)
        {
            if(noteSpeed < showtime_targetSpeed)
            {
                noteSpeed += deltaTime/1000.0;
            }
        }
        if(showType == SHOWTIME_SPEEDDOWN)
        {
            if(noteSpeed > showtime_targetSpeed)
            {
                noteSpeed -= deltaTime/2000.0;
            }
        }
    }
    else
    {
        if(showType == SHOWTIME_SPEEDUP)
        {
            if(noteSpeed > 1.0)
            {
                noteSpeed -= deltaTime/1000.0;
            }
        }
        if(showType == SHOWTIME_SPEEDDOWN)
        {
            if(noteSpeed < 1.0)
            {
                noteSpeed += deltaTime/2000.0;
            }
        }
    }
}

int GenShowType(const int32_t* showtimeConfig)
{
    int showType = SHOWTIME_TOTAL;
    double randomRange[SHOWTIME_TOTAL+1] = {0};
    randomRange[0] = 0;
    int totalLevel = 0;
    for(int i = 0; i < SHOWTIME_TOTAL; i++)
    {
        totalLevel += showtimeConfig[i];
        randomRange[i+1] = totalLevel;
    }
    int targetShowtime = rand()%totalLevel;
    for(int i = 0; i < SHOWTIME_TOTAL; i++)
    {
        if(targetShowtime >= randomRange[i] && targetShowtime < randomRange[i+1])
        {
            showType = i;
        }
    }
    return showType;
}

void GamePlayCtrl::ShowTimeStart(double nowTime)
{
    if(showtimegather == showtimegatherMax && !isShowTime)
    {
        showType = (W_SHOWTIME_TYPE)GenShowType(showtimeConfig);
        if(showType != SHOWTIME_SPEEDUP && showType != SHOWTIME_SPEEDDOWN)
        {
            NOTES_BARLIST* nowbarPtr = notes_bar;
            while(nowbarPtr != nullptr)
            {
                if(nowbarPtr->barTime >= nowTime + 1500)
                {
                    showtime_StartId = nowbarPtr->notes->noteId;
                    break;
                }
                nowbarPtr = nowbarPtr->nextBar;
            }
            if(showType == SHOWTIME_ROTATE)
                rotateDistance = kShowTime_RotateDistance[showtimeConfig[SHOWTIME_ROTATE]-1] * gScaleRate;
            else if(showType == SHOWTIME_HIDDEN)
                hiddenDistance = kShowTime_HiddenDistance[showtimeConfig[SHOWTIME_HIDDEN]-1] * gScaleRate;
        }
        else
        {
            if(showType == SHOWTIME_SPEEDUP)
                showtime_targetSpeed = kShowTime_SpeedUp[showtimeConfig[SHOWTIME_SPEEDUP]-1];
            else
                showtime_targetSpeed = kShowTime_SpeedDown[showtimeConfig[SHOWTIME_SPEEDDOWN]-1];
            NOTES_BARLIST* nowbarPtr = notes_bar;
            while(nowbarPtr != nullptr)
            {
                NOTES_LINKLIST* nowPtr = nowbarPtr->notes;
                bool isFind = false;
                while(nowPtr != nullptr)
                {
                    if(nowPtr->noteTime > nowTime)
                    {
                        showtime_StartId = nowPtr->noteId;
                        isFind = true;
                        break;
                    }
                    nowPtr = nowPtr->nextNote;
                }
                nowbarPtr = nowbarPtr->nextBar;
                if(isFind)
                    break;
            }
        }
        showtime_scoremultiplier = kShowTime_multiple[showtimeConfig[showType]-1];
        isShowTime = true;
    }
}
