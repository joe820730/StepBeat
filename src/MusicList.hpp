#ifndef MUSICLIST_HPP_INCLUDED
#define MUSICLIST_HPP_INCLUDED

#include "MenuInterface.hpp"
#include "MapInfo.hpp"

#define MAPLISTX        0.5
#define MAPLISTY        0.4
#define MAPLISTCOVERX   0.07
#define MAPLISTCOVERY   0.17
#define MAPCOVERSIZE    0.22
#define MAPLISTINFOX    0.34
#define MAPLISTINFOY    0.16
#define MAPLISTDIFFX    0.06
#define MAPLISTDIFFY    0.48
#define MAPLISTWIDTH    1
#define MAPLISTHEIGHT   0.12

#define MAPLIST_SCOREBOARD_X 0.03
#define MAPLIST_SCOREBOARD_Y 0.58

#define MAPLISTFONTOFFSETX 15
#define MAPLISTFONTOFFSETY 8

class MusicList : public MenuInterface
{
public:
    MusicList();
    ~MusicList();
    bool Setup(ConfigProvider* cfg_provider);
    void SetMenuLoc(int scrHeight, int scrWidth) override;
    void GetFontHeight() override;
    bool ScanSong(bool isRescan = false);
    void CloneBitmap();
    void MenuCtrl(int mouseX, int mouseY) override;
    void MenuCtrl(int keyCode) override;
    void Draw(double deltaTime) override;
    bool isSongScanning;
    W_SONGLIST* getSelectedSong(W_MAPDIFF& selectedDiff)
    {
        selectedDiff = (W_MAPDIFF)diffIdx;
        return this->selectedSong;
    }
private:
    uint16_t diffIdx;
    ConfigProvider* gameCfg;
    double menuBaseLocY;
    W_SONGLIST* wSongList;
    W_SONGLIST* tmpList;
    W_SONGLIST* selectedSong;
    void DrawSongTitle(W_SONGLIST *nowPtr, double locX, double locY, ALLEGRO_COLOR color, bool isSelected);
    void DrawHiscore(W_SONGLIST *nowPtr, double locX, double locY, double xsize, double ysize);
    void Idx2SongPtr()
    {
        W_SONGLIST *nowPtr = wSongList;
        while(nowPtr != nullptr)
        {
            if(menuIdx == nowPtr->listIdx)
                selectedSong = nowPtr;
            nowPtr = nowPtr->nextSong;
        }
    }
    void SearchDiff()
    {
        if(selectedSong != nullptr)
        {
            for(int idxTemp = 0; idxTemp < MAP_TOTAL; idxTemp++)
            {
                if(selectedSong->haveDiff[idxTemp])
                {
                    diffIdx = idxTemp;
                    break;
                }
            }
        }
    }
    void DrawSelectedInfo(W_SONGLIST* nowPtr);
};

extern MusicList* gMUSICLIST;
#endif // MUSICLIST_HPP_INCLUDED
